package com.rgapps.auth.datahelper;

import com.rgapps.auth.controller.resource.AuthResponse;
import com.rgapps.auth.controller.resource.LoginResource;
import com.rgapps.auth.controller.resource.SignupResource;
import com.rgapps.auth.controller.resource.UserView;
import com.rgapps.auth.model.Role;
import com.rgapps.auth.model.User;
import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.apiexceptions.ValidationErrorResponse;
import com.rgapps.common.exception.modelexceptions.ModelErrorResponse;
import org.assertj.core.api.Assertions;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ObjectValidations {
    public static void validateError(ValidationErrorResponse response,
                                     HttpStatus status,
                                     String error,
                                     String message,
                                     Map<String, List<String>> expectedErrors) {
        assertThat(response).isNotNull();
        Assertions.assertThat(response.getStatus()).isEqualTo(status);
        Assertions.assertThat(response.getError()).isEqualTo(error);
        Assertions.assertThat(response.getMessage()).isEqualTo(message);
        Map<String, List<String>> detail = response.getErrors();
        assertThat(detail).hasSameSizeAs(expectedErrors);
        assertThat(detail.keySet()).hasSameElementsAs(expectedErrors.keySet());
        detail.forEach((field, errors) -> assertThat(errors).hasSameElementsAs(expectedErrors.get(field)));
    }

    public static void validateError(ModelErrorResponse<?> response,
                                     HttpStatus status,
                                     String error,
                                     String message) {
        assertThat(response).isNotNull();
        Assertions.assertThat(response.getStatus()).isEqualTo(status);
        Assertions.assertThat(response.getError()).isEqualTo(error);
        Assertions.assertThat(response.getMessage()).isEqualTo(message);
    }

    public static void validateError(ApiErrorResponse response,
                                     HttpStatus status,
                                     String error,
                                     String message) {
        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(status);
        assertThat(response.getError()).isEqualTo(error);
        assertThat(response.getMessage()).isEqualTo(message);
    }

    public static void validateUser(User user,
                                    String username,
                                    List<Role.RoleType> roles,
                                    LocalDateTime createdIntervalStart,
                                    LocalDateTime createdIntervalEnd) {
        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isEqualTo(username);
        assertThat(user.getPassword()).isNotBlank();
        assertThat(user.getRoles().stream().map(Role::getType)).containsAll(roles);
        assertThat(user.getCreatedDate()).isBetween(createdIntervalStart, createdIntervalEnd);
        assertThat(user.getUpdatedDate()).isEqualTo(user.getCreatedDate());
    }

    public static void validateUserView(UserView user,
                                        String username,
                                        List<Role.RoleType> roles) {
        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isEqualTo(username);
        Assertions.assertThat(user.getRoles().stream().map(Role::getType)).containsAll(roles);
    }

    public static void validateSignUpResource(SignupResource resource,
                                              String username,
                                              String password,
                                              List<String> roles) {
        assertThat(resource).isNotNull();
        assertThat(resource.getUsername()).isEqualTo(username);
        assertThat(resource.getPassword()).isEqualTo(password);
        assertThat(resource.getRoles()).containsAll(roles);
    }

    public static void validateAuthResponse(AuthResponse response) {
        assertThat(response).isNotNull();
        assertThat(response.getToken()).isNotBlank();
    }

    public static void validateLoginResource(LoginResource resource,
                                             String username,
                                             String password) {
        assertThat(resource).isNotNull();
        assertThat(resource.getUsername()).isEqualTo(username);
        assertThat(resource.getPassword()).isEqualTo(password);
    }
}
