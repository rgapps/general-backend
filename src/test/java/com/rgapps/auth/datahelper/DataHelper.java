package com.rgapps.auth.datahelper;

import com.rgapps.auth.controller.resource.LoginResource;
import com.rgapps.auth.controller.resource.SignupResource;
import com.rgapps.common.exception.modelexceptions.ModelErrorResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.junit.jupiter.params.provider.Arguments;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class DataHelper {
    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class ModelErrorResponseSignupResource extends ModelErrorResponse<SignupResource> {
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class ModelErrorResponseLoginResource extends ModelErrorResponse<LoginResource> {
    }

    public static Stream<Arguments> registerUserFailBodyCases() {
        return Stream.of(
                Arguments.of("INVALID JSON}",
                        Map.of(
                                "resource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{}",
                        Map.of(
                                "username", List.of("must not be empty"),
                                "password", List.of("must not be empty"),
                                "roles", List.of("must not be empty")
                        )),
                Arguments.of("{\"username\": \"\", \"password\": \"\", \"roles\": []}",
                        Map.of(
                                "username", List.of("must not be empty", "size must be between 3 and 20"),
                                "password", List.of("must not be empty", "size must be between 6 and 40"),
                                "roles", List.of("must not be empty")
                        )),
                Arguments.of("{\"username\": \"username\", \"password\": \"password\", \"roles\": [\"ANY_ROLE\"]}",
                        Map.of("roles", List.of("must have a valid among: [ROLE_USER, ROLE_MODERATOR, ROLE_ADMIN]"))));
    }

    public static Stream<Arguments> authenticateUserFailBodyCases() {
        return Stream.of(
                Arguments.of("INVALID JSON}",
                        Map.of(
                                "resource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{}",
                        Map.of(
                                "username", List.of("must not be empty"),
                                "password", List.of("must not be empty")
                        )),
                Arguments.of("{\"username\": \"\", \"password\": \"\"}",
                        Map.of(
                                "username", List.of("must not be empty"),
                                "password", List.of("must not be empty")
                        )));
    }

    public static final String DEFAULT_SIGNUP = "{" +
            "\"username\": \"user\", " +
            "\"password\": \"password\"," +
            "\"roles\": [\"ROLE_USER\"]}";

    public static final String SIGNUP_ADMIN = "{" +
            "\"username\": \"admin\", " +
            "\"password\": \"password\"," +
            "\"roles\": [\"ROLE_ADMIN\"]}";

    public static final String DEFAULT_SIGNIN = "{" +
            "\"username\": \"user\", " +
            "\"password\": \"password\"}";

    public static final String SIGNIN_ADMIN = "{" +
            "\"username\": \"admin\", " +
            "\"password\": \"password\"}";

    public static final String SIGNIN_INCORRECT_PASSWORD = "{" +
            "\"username\": \"user\", " +
            "\"password\": \"incorrect\"}";
}
