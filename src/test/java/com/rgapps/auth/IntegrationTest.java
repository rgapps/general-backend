package com.rgapps.auth;

import com.rgapps.auth.model.Role;
import com.rgapps.auth.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class IntegrationTest {
    @Autowired
    protected ReactiveMongoTemplate mongoTemplate;

    @Autowired
    protected WebTestClient webTestClient;

    @BeforeEach
    public void clean() {
        mongoTemplate.findAllAndRemove(new Query(), User.class).collectList().block();
        mongoTemplate.findAllAndRemove(new Query(), Role.class).collectList().block();
    }

    protected WebTestClient.ResponseSpec signup(String body) {
        return webTestClient.post()
                .uri("/api/auth/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(body)
                .exchange()
                .expectStatus().isOk();
    }

    protected WebTestClient.ResponseSpec signin(String body) {
        return webTestClient.post()
                .uri("/api/auth/signin")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(body)
                .exchange()
                .expectStatus().isOk();
    }
}
