package com.rgapps.stocks.controller;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.rgapps.stocks.IntegrationTest;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.SellStockTransaction;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static com.rgapps.stocks.datahelper.DataHelper.BUY_ANOTHER_STOCK_DEF;
import static com.rgapps.stocks.datahelper.DataHelper.BUY_TWO_SHARES;
import static com.rgapps.stocks.datahelper.DataHelper.DEFAULT_BUY;
import static com.rgapps.stocks.datahelper.DataHelper.DEFAULT_SELL;
import static org.assertj.core.api.Assertions.assertThat;

@Disabled("Embedded mongo not working inside docker")
class AccountsClientCallsTest extends IntegrationTest {
    @Test
    public void debitCalledAfterBuyTransaction() {
        BuyStockTransaction response = buyStock(DEFAULT_BUY)
                .expectBody(BuyStockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo("/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/debit"))
                .withRequestBody(WireMock.equalTo(
                        "{\"debitAmount\":11.0,\"currencyCode\":\"AED\",\"dateDebited\":\"2020-12-07\"," +
                                "\"externalId\":\"" + response.getId() + "\"," +
                                "\"debitType\":\"STOCKS_INVESTMENT\"}")));
    }

    @Test
    public void creditCalledAfterSellTransaction() {
        buyStock(DEFAULT_BUY);
        SellStockTransaction response = sellStock(DEFAULT_SELL)
                .expectBody(SellStockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo("/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/credit"))
                .withRequestBody(WireMock.equalTo(
                        "{\"creditAmount\":9.0,\"currencyCode\":\"AED\",\"dateCredited\":\"2020-12-07\"," +
                                "\"externalId\":\"" + response.getId() + "\"," +
                                "\"creditType\":\"STOCKS_REVENUE\"}")));
    }

    @Test
    public void creditCalledAfterDeleteLastBuyTransaction() {
        buyStock(DEFAULT_BUY);

        BuyStockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last")
                .exchange()
                .expectStatus().isOk()
                .expectBody(BuyStockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo("/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/credit"))
                .withRequestBody(WireMock.equalTo(
                        "{\"creditAmount\":11.0,\"currencyCode\":\"AED\",\"dateCredited\":\"2020-12-07\"," +
                                "\"externalId\":\"" + response.getId() + "\"," +
                                "\"creditType\":\"STOCKS_DELETE_TRANSACTION\"}")));
    }

    @Test
    public void creditCalledAfterDeleteLastBuyTransactionByStock() {
        buyStock(BUY_TWO_SHARES);
        sellStock(DEFAULT_SELL);
        buyStock(BUY_TWO_SHARES);
        buyStock(BUY_ANOTHER_STOCK_DEF);
        BuyStockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC")
                .exchange()
                .expectStatus().isOk()
                .expectBody(BuyStockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo("/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/credit"))
                .withRequestBody(WireMock.equalTo(
                        "{\"creditAmount\":21.0,\"currencyCode\":\"AED\",\"dateCredited\":\"2020-12-07\"," +
                                "\"externalId\":\"" + response.getId() + "\"," +
                                "\"creditType\":\"STOCKS_DELETE_TRANSACTION\"}")));
    }

    @Test
    public void debitCalledAfterDeleteLastSellTransaction() {
        buyStock(BUY_TWO_SHARES);
        sellStock(DEFAULT_SELL);
        sellStock(DEFAULT_SELL);
        SellStockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last")
                .exchange()
                .expectStatus().isOk()
                .expectBody(SellStockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo("/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/debit"))
                .withRequestBody(WireMock.equalTo(
                        "{\"debitAmount\":9.0,\"currencyCode\":\"AED\",\"dateDebited\":\"2020-12-07\"," +
                                "\"externalId\":\"" + response.getId() + "\"," +
                                "\"debitType\":\"STOCKS_DELETE_TRANSACTION\"}")));
    }

    @Test
    public void debitCalledAfterDeleteLastSellTransactionByStock() {
        buyStock(BUY_TWO_SHARES);
        sellStock(DEFAULT_SELL);
        sellStock(DEFAULT_SELL);
        buyStock(BUY_ANOTHER_STOCK_DEF);
        SellStockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC")
                .exchange()
                .expectStatus().isOk()
                .expectBody(SellStockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo("/api/accounts/aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa/movements/debit"))
                .withRequestBody(WireMock.equalTo(
                        "{\"debitAmount\":9.0,\"currencyCode\":\"AED\",\"dateDebited\":\"2020-12-07\"," +
                                "\"externalId\":\"" + response.getId() + "\"," +
                                "\"debitType\":\"STOCKS_DELETE_TRANSACTION\"}")));
    }
}
