package com.rgapps.stocks.controller;

import com.rgapps.stocks.IntegrationTest;
import com.rgapps.stocks.controller.resource.StockJournalView;
import com.rgapps.stocks.model.StockJournal;
import com.rgapps.stocks.model.StockTransaction;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

import static com.rgapps.stocks.datahelper.DataHelper.BUY_3_FOR_TWENTY;
import static com.rgapps.stocks.datahelper.DataHelper.BUY_ANOTHER_STOCK_DEF;
import static com.rgapps.stocks.datahelper.DataHelper.BUY_NEXT_DAY;
import static com.rgapps.stocks.datahelper.DataHelper.BUY_TWO_SHARES;
import static com.rgapps.stocks.datahelper.DataHelper.DEFAULT_BUY;
import static com.rgapps.stocks.datahelper.DataHelper.DEFAULT_SELL;
import static com.rgapps.stocks.datahelper.DataHelper.SELL_2_FOR_THIRTY;
import static com.rgapps.stocks.datahelper.DataHelper.SELL_2_WITH_2_BROKERAGE_FEE;
import static com.rgapps.stocks.datahelper.DataHelper.SELL_ANOTHER_STOCK;
import static com.rgapps.stocks.datahelper.ModelData.defaultAccountId;
import static com.rgapps.stocks.datahelper.ObjectValidations.validateStockJournal;
import static com.rgapps.stocks.datahelper.ObjectValidations.validateStockJournalView;
import static org.assertj.core.api.Assertions.assertThat;

@Disabled("Embedded mongo not working inside docker")
class StockJournalTest extends IntegrationTest {
    @Test
    public void stockJournalIsUpdatedAfterBuyTransaction() {
        StockTransaction response = buyStock(DEFAULT_BUY)
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        StockJournal stockJournal = mongoTemplate.findById(StockJournal.calculateId(response), StockJournal.class).block();
        validateStockJournal(stockJournal,
                "ABC",
                1,
                "AED",
                11.0,
                "2020-12-07",
                "1970-01-01",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void stockJournalIsUpdatedAfterSellTransaction() {
        buyStock(BUY_TWO_SHARES);

        StockTransaction response = sellStock(DEFAULT_SELL)
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        StockJournal stockJournal = mongoTemplate.findById(StockJournal.calculateId(response), StockJournal.class).block();
        validateStockJournal(stockJournal,
                "ABC",
                1,
                "AED",
                (2 * 10.0 + 1) / 2,
                "2020-12-07",
                "2020-12-07",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void stockJournalIsUpdatedAfterDeleteBuyTransactionJustOneTransaction() {
        buyStock(DEFAULT_BUY);

        StockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?accountId=" + defaultAccountId)
                .exchange()
                .expectStatus().isOk()
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        StockJournal stockJournal = mongoTemplate.findById(StockJournal.calculateId(response), StockJournal.class).block();
        validateStockJournal(stockJournal,
                "ABC",
                0,
                "ANY",
                0,
                "1970-01-01",
                "1970-01-01",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void stockJournalIsUpdatedAfterDeleteBuyTransaction() {
        buyStock(BUY_TWO_SHARES);
        sellStock(DEFAULT_SELL);
        buyStock(BUY_NEXT_DAY);

        StockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?accountId=" + defaultAccountId)
                .exchange()
                .expectStatus().isOk()
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        StockJournal stockJournal = mongoTemplate.findById(StockJournal.calculateId(response), StockJournal.class).block();
        validateStockJournal(stockJournal,
                "ABC",
                1,
                "AED",
                (2 * 10.0 + 1) / 2,
                "2020-12-07",
                "2020-12-07",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void stockJournalIsUpdatedAfterDeleteLastBuyTransactionByStock() {
        buyStock(BUY_TWO_SHARES);
        sellStock(DEFAULT_SELL);
        buyStock(BUY_TWO_SHARES);
        buyStock(BUY_ANOTHER_STOCK_DEF);
        StockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC&accountId=" + defaultAccountId)
                .exchange()
                .expectStatus().isOk()
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        StockJournal stockJournal = mongoTemplate.findById(StockJournal.calculateId(response), StockJournal.class).block();
        validateStockJournal(stockJournal,
                "ABC",
                1,
                "AED",
                (2 * 10.0 + 1) / 2,
                "2020-12-07",
                "2020-12-07",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void stockJournalIsUpdatedAfterDeleteSellTransaction() {
        buyStock(BUY_TWO_SHARES);
        sellStock(DEFAULT_SELL);
        sellStock(DEFAULT_SELL);
        StockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?accountId=" + defaultAccountId)
                .exchange()
                .expectStatus().isOk()
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        StockJournal stockJournal = mongoTemplate.findById(StockJournal.calculateId(response), StockJournal.class).block();
        validateStockJournal(stockJournal,
                "ABC",
                1,
                "AED",
                (2 * 10.0 + 1) / 2,
                "2020-12-07",
                "2020-12-07",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void stockJournalIsUpdatedAfterDeleteLastSellTransactionByStock() {
        buyStock(BUY_TWO_SHARES);
        sellStock(DEFAULT_SELL);
        sellStock(DEFAULT_SELL);
        buyStock(BUY_ANOTHER_STOCK_DEF);
        StockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC&accountId=" + defaultAccountId)
                .exchange()
                .expectStatus().isOk()
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        StockJournal stockJournal = mongoTemplate.findById(StockJournal.calculateId(response), StockJournal.class).block();
        validateStockJournal(stockJournal,
                "ABC",
                1,
                "AED",
                (2 * 10.0 + 1) / 2,
                "2020-12-07",
                "2020-12-07",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void stockJournalCalculatesGainsOnBuyCorrectly() {
        buyStock(BUY_TWO_SHARES);

        StockTransaction response = buyStock(BUY_3_FOR_TWENTY)
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        StockJournal stockJournal = mongoTemplate.findById(StockJournal.calculateId(response), StockJournal.class).block();
        validateStockJournal(stockJournal,
                "ABC",
                5,
                "AED",
                ((2 * 10.0 + 1) + (3 * 20.0 + 1)) / 5,
                "2020-12-07",
                "1970-01-01",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void stockJournalCalculatesNewValuesOnSellCorrectly() {
        buyStock(BUY_TWO_SHARES);
        buyStock(BUY_3_FOR_TWENTY);

        StockTransaction response = sellStock(DEFAULT_SELL)
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        StockJournal stockJournal = mongoTemplate.findById(StockJournal.calculateId(response), StockJournal.class).block();
        validateStockJournal(stockJournal,
                "ABC",
                4,
                "AED",
                ((2 * 10.0 + 1) + (3 * 20.0 + 1)) / 5,
                "2020-12-07",
                "2020-12-07",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void findStockJournalReturnsEmptyListIfNoData() {
        List<StockJournalView> summaryList = authenticatedWebTestClient.get()
                .uri("/api/stocks/transactions/journal?accountId=" + defaultAccountId)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(StockJournalView.class)
                .returnResult()
                .getResponseBody();

        assertThat(summaryList).isNotNull();
        assertThat(summaryList).hasSize(0);
    }

    @Test
    public void findStockJournalCorrectly() {
        buyStock(BUY_TWO_SHARES);
        sellStock(SELL_2_FOR_THIRTY);
        buyStock(BUY_3_FOR_TWENTY);
        sellStock(SELL_2_WITH_2_BROKERAGE_FEE);
        List<StockJournalView> response = authenticatedWebTestClient.get()
                .uri("/api/stocks/transactions/journal?accountId=" + defaultAccountId)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(StockJournalView.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response).hasSize(1);
        validateStockJournalView(response.get(0),
                "ABC",
                1,
                (3 * 20.0 + 1) / 3,
                "2020-12-07",
                1 * (3 * 20.0 + 1) / 3);
    }

    @Test
    public void findMultipleStocksJournals() {
        buyStock(DEFAULT_BUY);
        buyStock(BUY_ANOTHER_STOCK_DEF);
        List<StockJournalView> response = authenticatedWebTestClient.get()
                .uri("/api/stocks/transactions/journal?accountId=" + defaultAccountId)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(StockJournalView.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response).hasSize(2);
        validateStockJournalView(response.get(0),
                "ABC",
                1,
                10.0 + 1,
                "2020-12-07",
                10.0 + 1);
        validateStockJournalView(response.get(1),
                "DEF",
                1,
                10.0 + 1,
                "2020-12-07",
                10.0 + 1);
    }

    @Test
    public void findStockJournalExcludesMarketValueZero() {
        buyStock(BUY_ANOTHER_STOCK_DEF);
        buyStock(DEFAULT_BUY);
        sellStock(SELL_ANOTHER_STOCK);
        List<StockJournalView> response = authenticatedWebTestClient.get()
                .uri("/api/stocks/transactions/journal?accountId=" + defaultAccountId)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(StockJournalView.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response).hasSize(1);
        validateStockJournalView(response.get(0),
                "ABC",
                1,
                10.0 + 1,
                "2020-12-07",
                10.0 + 1);
    }

    @Test
    public void findStockJournalFailsIfNotAuthenticated() {
        webTestClient.get()
                .uri("/api/stocks/transactions/journal?accountId=" + defaultAccountId)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }
}
