package com.rgapps.stocks.controller;

import com.rgapps.common.exception.apiexceptions.ValidationErrorResponse;
import com.rgapps.stocks.IntegrationTest;
import com.rgapps.stocks.controller.resource.SummaryForMonthView;
import com.rgapps.stocks.datahelper.DataHelper;
import com.rgapps.stocks.datahelper.ModelData;
import com.rgapps.stocks.datahelper.ObjectValidations;
import com.rgapps.stocks.model.MonthlyProfitSummary;
import com.rgapps.stocks.model.StockTransaction;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled("Embedded mongo not working inside docker")
class MonthlyProfitSummaryTest extends IntegrationTest {

    @Test
    public void monthlyProfitSummaryIsNOTUpdatedAfterBuyTransaction() {
        StockTransaction response = buyStock(DataHelper.DEFAULT_BUY)
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        List<MonthlyProfitSummary> monthlyProfitSummary = mongoTemplate.findAll(MonthlyProfitSummary.class).collectList().block();
        assertThat(monthlyProfitSummary).isEmpty();
    }

    @Test
    public void monthlyProfitSummaryIsUpdatedAfterSellTransaction() {
        buyStock(DataHelper.BUY_TWO_SHARES);

        StockTransaction response = sellStock(DataHelper.SELL_FOR_TWENTY)
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        MonthlyProfitSummary monthlyProfitSummary =
                mongoTemplate.findById(MonthlyProfitSummary.calculateId(response), MonthlyProfitSummary.class).block();
        ObjectValidations.validateMonthlyProfitSummary(monthlyProfitSummary,
                "AED",
                (2 * 10.0 + 1) / 2,
                20.0 - 1,
                1,
                (20.0 - 1) - (2 * 10.0 + 1) / 2,
                "2020-12-01",
                "ABC",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void monthlyProfitSummaryIsNOTUpdatedAfterDeleteLastBuyTransaction() {
        buyStock(DataHelper.BUY_TWO_SHARES);
        LocalDateTime start = LocalDateTime.now();
        sellStock(DataHelper.SELL_FOR_TWENTY);
        LocalDateTime end = LocalDateTime.now();
        buyStock(DataHelper.BUY_TWO_SHARES);

        StockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last")
                .exchange()
                .expectStatus().isOk()
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        MonthlyProfitSummary monthlyProfitSummary = mongoTemplate.findById(MonthlyProfitSummary.calculateId(response),
                MonthlyProfitSummary.class).block();
        ObjectValidations.validateMonthlyProfitSummary(monthlyProfitSummary,
                "AED",
                (2 * 10.0 + 1) / 2,
                20.0 - 1,
                1,
                (20.0 - 1) - (2 * 10.0 + 1) / 2,
                "2020-12-01",
                "ABC",
                start,
                end);
    }

    @Test
    public void monthlyProfitSummaryIsNOTUpdatedAfterDeleteLastBuyTransactionByStock() {
        buyStock(DataHelper.BUY_TWO_SHARES);
        LocalDateTime start = LocalDateTime.now();
        sellStock(DataHelper.SELL_FOR_TWENTY);
        LocalDateTime end = LocalDateTime.now();
        buyStock(DataHelper.BUY_TWO_SHARES);
        buyStock(DataHelper.BUY_ANOTHER_STOCK_DEF);

        StockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC")
                .exchange()
                .expectStatus().isOk()
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        MonthlyProfitSummary monthlyProfitSummary = mongoTemplate.findById(MonthlyProfitSummary.calculateId(response),
                MonthlyProfitSummary.class).block();
        ObjectValidations.validateMonthlyProfitSummary(monthlyProfitSummary,
                "AED",
                (2 * 10.0 + 1) / 2,
                20.0 - 1,
                1,
                (20.0 - 1) - (2 * 10.0 + 1) / 2,
                "2020-12-01",
                "ABC",
                start,
                end);
    }

    @Test
    public void monthlyProfitSummaryIsUpdatedAfterDeleteLastSellTransactionBysStock() {
        buyStock(DataHelper.BUY_TWO_SHARES);
        sellStock(DataHelper.SELL_FOR_TWENTY);
        sellStock(DataHelper.SELL_FOR_TWENTY);
        buyStock(DataHelper.BUY_ANOTHER_STOCK_DEF);
        StockTransaction response = authenticatedWebTestClient.delete()
                .uri("/api/stocks/transactions/last?stockCode=ABC")
                .exchange()
                .expectStatus().isOk()
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        MonthlyProfitSummary monthlyProfitSummary = mongoTemplate.findById(MonthlyProfitSummary.calculateId(response),
                MonthlyProfitSummary.class).block();
        ObjectValidations.validateMonthlyProfitSummary(monthlyProfitSummary,
                "AED",
                (2 * 10.0 + 1) / 2,
                20.0 - 1,
                1,
                (20.0 - 1) - (2 * 10.0 + 1) / 2,
                "2020-12-01",
                "ABC",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void monthlyProfitSummaryCalculatesGainCorrectly() {
        buyStock(DataHelper.BUY_TWO_SHARES);
        StockTransaction response = sellStock(DataHelper.SELL_2_FOR_THIRTY)
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        MonthlyProfitSummary monthlyProfitSummary =
                mongoTemplate.findById(MonthlyProfitSummary.calculateId(response), MonthlyProfitSummary.class).block();
        ObjectValidations.validateMonthlyProfitSummary(monthlyProfitSummary,
                "AED",
                (2 * 10.0 + 1) / 2,
                (2 * 30.0 - 1) / 2,
                2,
                (2 * 30.0 - 1) - (2 * 10.0 + 1),
                "2020-12-01",
                "ABC",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void monthlyProfitSummaryCalculatesLostCorrectly() {
        buyStock(DataHelper.BUY_2_FOR_20);
        StockTransaction response = sellStock(DataHelper.SELL_TWO_SHARES)
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        MonthlyProfitSummary monthlyProfitSummary =
                mongoTemplate.findById(MonthlyProfitSummary.calculateId(response), MonthlyProfitSummary.class).block();
        ObjectValidations.validateMonthlyProfitSummary(monthlyProfitSummary,
                "AED",
                (2 * 20.0 + 1) / 2,
                (2 * 10.0 - 1) / 2,
                2,
                2 * 10.0 - 1 - (2 * 20.0 + 1),
                "2020-12-01",
                "ABC",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void monthlyProfitSummaryCalculatesAveragesCorrectly() {
        buyStock(DataHelper.BUY_TWO_SHARES);
        buyStock(DataHelper.BUY_3_FOR_TWENTY);
        sellStock(DataHelper.SELL_2_FOR_THIRTY);
        StockTransaction response = sellStock(DataHelper.SELL_2_WITH_2_BROKERAGE_FEE)
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        MonthlyProfitSummary monthlyProfitSummary =
                mongoTemplate.findById(MonthlyProfitSummary.calculateId(response), MonthlyProfitSummary.class).block();
        ObjectValidations.validateMonthlyProfitSummary(monthlyProfitSummary,
                "AED",
                (2 * 10.0 + 1 + 3 * 20.0 + 1) / 5,
                ((2 * 30.0 - 1) + (2 * 10.0 - 2)) / 4,
                4,
                2 * 30.0 - 1 - 2 * ((2 * 10.0 + 1 + 3 * 20.0 + 1) / 5) + 2 * 10.0 - 2 - 2 * ((2 * 10.0 + 1 + 3 * 20.0 + 1) / 5),
                "2020-12-01",
                "ABC",
                response.getCreatedDate(),
                LocalDateTime.now());
    }

    @Test
    public void monthlyProfitSummaryCalculatesAfterBuyAndSellMultipleTimesCorrectly() {
        buyStock(DataHelper.BUY_TWO_SHARES);
        sellStock(DataHelper.SELL_2_FOR_THIRTY);
        buyStock(DataHelper.BUY_3_FOR_TWENTY);
        StockTransaction lastSellResponse = sellStock(DataHelper.SELL_2_WITH_2_BROKERAGE_FEE)
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();
        StockTransaction lastBuyResponse = buyStock(DataHelper.DEFAULT_BUY)
                .expectBody(StockTransaction.class)
                .returnResult()
                .getResponseBody();

        assertThat(lastSellResponse).isNotNull();
        assertThat(lastBuyResponse).isNotNull();
        String id = MonthlyProfitSummary.calculateId(lastSellResponse.getTransactionDate(), lastSellResponse.getStock(), ModelData.defaultAccountId);
        MonthlyProfitSummary monthlyProfitSummary = mongoTemplate.findById(id, MonthlyProfitSummary.class).block();
        ObjectValidations.validateMonthlyProfitSummary(monthlyProfitSummary,
                "AED",
                (2 * 10.0 + 1 + 2 * ((3 * 20.0 + 1) / 3)) / 4,
                ((2 * 30.0 - 1) + (2 * 10.0 - 2)) / 4,
                4,
                2 * 30.0 - 1 - (2 * 10.0 + 1) + 2 * 10 - 2 - (2 * (3 * 20.0 + 1) / 3),
                "2020-12-01",
                "ABC",
                lastSellResponse.getCreatedDate(),
                lastBuyResponse.getCreatedDate());
    }

    @ParameterizedTest
    @MethodSource("com.rgapps.stocks.datahelper.DataHelper#findMonthlyReturnsNotFoundForInvalidPathParametersCases")
    public void findMonthlyReturnsNotFoundForInvalidPathParameters(String lastPathPart, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = authenticatedWebTestClient.get()
                .uri(String.format("/api/stocks/transactions/profitSummary/%s?accountId=%s", lastPathPart, ModelData.defaultAccountId))
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);
    }

    @Test
    public void findMonthlyReturnsEmptyListIfNoData() {
        buyStock(DataHelper.BUY_TWO_SHARES);
        sellStock(DataHelper.SELL_2_FOR_THIRTY);

        List<SummaryForMonthView> summaryList = authenticatedWebTestClient.get()
                .uri("/api/stocks/transactions/profitSummary/2021/01?accountId=" + ModelData.defaultAccountId)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(SummaryForMonthView.class)
                .returnResult()
                .getResponseBody();

        assertThat(summaryList).isNotNull();
        assertThat(summaryList).hasSize(0);
    }

    @Test
    public void findMonthlyReturnsCorrectly() {
        buyStock(DataHelper.BUY_TWO_SHARES);
        sellStock(DataHelper.SELL_2_FOR_THIRTY);
        buyStock(DataHelper.BUY_3_FOR_TWENTY);
        sellStock(DataHelper.SELL_2_WITH_2_BROKERAGE_FEE);
        List<SummaryForMonthView> summaryList = authenticatedWebTestClient.get()
                .uri("/api/stocks/transactions/profitSummary/2020/12?accountId=" + ModelData.defaultAccountId)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(SummaryForMonthView.class)
                .returnResult()
                .getResponseBody();

        assertThat(summaryList).isNotNull();
        assertThat(summaryList).hasSize(1);
        ObjectValidations.validateSummaryForMonthView(summaryList.get(0),
                ((2 * 10.0 + 1) + 2 * (3 * 20.0 + 1) / 3) / 4,
                ((2 * 30.0 - 1) + (2 * 10.0 - 2)) / 4,
                4,
                "AED",
                ((2 * 30.0 - 1) - (2 * 10.0 + 1)) + ((2 * 10.0 - 2) - 2 * ((3 * 20.0 + 1) / 3)));
    }

    @Test
    public void findMonthlyReturnsExcludesMarketValueZero() {
        buyStock(DataHelper.BUY_TWO_SHARES);
        sellStock(DataHelper.SELL_2_FOR_THIRTY);
        buyStock(DataHelper.BUY_ANOTHER_STOCK_DEF);
        List<SummaryForMonthView> summaryList = authenticatedWebTestClient.get()
                .uri("/api/stocks/transactions/profitSummary/2020/12?accountId=" + ModelData.defaultAccountId)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(SummaryForMonthView.class)
                .returnResult()
                .getResponseBody();

        assertThat(summaryList).isNotNull();
        assertThat(summaryList).hasSize(1);
        ObjectValidations.validateSummaryForMonthView(summaryList.get(0),
                (2 * 10.0 + 1) / 2,
                (2 * 30.0 - 1) / 2,
                2,
                "AED",
                ((2 * 30.0 - 1) - (2 * 10.0 + 1)));
    }

    @Test
    public void findMonthlyFailsIfNotAuthenticated() {
        webTestClient.get()
                .uri("/api/stocks/transactions/profitSummary/2020/12?accountId=" + ModelData.defaultAccountId)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }
}
