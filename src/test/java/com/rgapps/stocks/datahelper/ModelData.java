package com.rgapps.stocks.datahelper;

import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.MonthlyProfitSummary;
import com.rgapps.stocks.model.SellStockTransaction;
import com.rgapps.stocks.model.StockJournal;
import com.rgapps.stocks.model.StockTransaction;
import com.rgapps.stocks.model.values.Currency;
import com.rgapps.stocks.model.values.MoneyAmount;
import com.rgapps.stocks.model.values.Stock;

import java.time.LocalDate;
import java.util.UUID;

public class ModelData {
    public static Stock defaultStock = new Stock("ABC");
    public static UUID defaultAccountId = UUID.fromString("aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa");
    public static int defaultNumberOfShares = 1;
    public static Currency defaultCurrency = new Currency("AED");
    public static MoneyAmount defaultPrice = new MoneyAmount(10, defaultCurrency);
    public static MoneyAmount defaultBrokerageFee = new MoneyAmount(2, defaultCurrency);
    public static LocalDate yesterday = LocalDate.now().minusDays(1);
    public static LocalDate now = LocalDate.now();

    public static StockJournal defaultStockJournal =
            new StockJournal(
                    defaultStock.getCode(),
                    defaultNumberOfShares,
                    defaultPrice,
                    yesterday,
                    now,
                    defaultAccountId);

    public static StockJournal stockJournalWithNullLastSoldDate =
            new StockJournal(
                    defaultStock.getCode(),
                    defaultNumberOfShares,
                    defaultPrice,
                    yesterday,
                    null,
                    defaultAccountId);

    public static BuyStockTransaction buyTransactionWithNullBrokerageFee = new BuyStockTransaction(
            defaultAccountId,
            defaultStock,
            defaultNumberOfShares,
            defaultPrice,
            yesterday,
            null,
            StockTransaction.Status.SUCCESS);

    public static BuyStockTransaction defaultBuyTransaction() {
        return new BuyStockTransaction(
                defaultAccountId,
                defaultStock,
                defaultNumberOfShares,
                defaultPrice,
                yesterday,
                defaultBrokerageFee,
                StockTransaction.Status.SUCCESS);
    }

    public static SellStockTransaction defaultSellTransaction() {
        return new SellStockTransaction(
                defaultAccountId,
                defaultStock,
                defaultNumberOfShares,
                defaultPrice,
                yesterday,
                defaultBrokerageFee,
                StockTransaction.Status.SUCCESS);
    }

    public static GetDividendTransaction defaultDividendTransaction() {
        return new GetDividendTransaction(
                defaultAccountId,
                defaultStock,
                defaultNumberOfShares,
                defaultPrice,
                yesterday,
                defaultBrokerageFee,
                StockTransaction.Status.SUCCESS);
    }

    public static MonthlyProfitSummary defaultMonthlyProfitSummary = new MonthlyProfitSummary(
            yesterday,
            defaultStock,
            defaultPrice,
            defaultPrice,
            defaultNumberOfShares,
            defaultAccountId);
}
