package com.rgapps.stocks.datahelper;

import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.apiexceptions.ValidationErrorResponse;
import com.rgapps.common.exception.modelexceptions.ModelErrorResponse;
import com.rgapps.stocks.controller.resource.BuyStockResource;
import com.rgapps.stocks.controller.resource.GetDividendResource;
import com.rgapps.stocks.controller.resource.SellStockResource;
import com.rgapps.stocks.controller.resource.StockJournalView;
import com.rgapps.stocks.controller.resource.StockTransactionView;
import com.rgapps.stocks.controller.resource.SummaryForMonthView;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.MonthlyProfitSummary;
import com.rgapps.stocks.model.SellStockTransaction;
import com.rgapps.stocks.model.StockJournal;
import com.rgapps.stocks.model.StockTransaction;
import com.rgapps.stocks.model.values.Currency;
import com.rgapps.stocks.model.values.MoneyAmount;
import com.rgapps.stocks.model.values.Stock;
import org.assertj.core.api.Assertions;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class ObjectValidations {
    public static void validateError(ValidationErrorResponse response,
                                     HttpStatus status,
                                     String error,
                                     String message,
                                     Map<String, List<String>> expectedErrors) {
        assertThat(response).isNotNull();
        Assertions.assertThat(response.getStatus()).isEqualTo(status);
        Assertions.assertThat(response.getError()).isEqualTo(error);
        Assertions.assertThat(response.getMessage()).isEqualTo(message);
        Map<String, List<String>> detail = response.getErrors();
        assertThat(detail).hasSameSizeAs(expectedErrors);
        assertThat(detail.keySet()).hasSameElementsAs(expectedErrors.keySet());
        detail.forEach((field, errors) -> assertThat(errors).hasSameElementsAs(expectedErrors.get(field)));
    }

    public static void validateError(ModelErrorResponse<?> response,
                                     HttpStatus status,
                                     String error,
                                     String message) {
        assertThat(response).isNotNull();
        Assertions.assertThat(response.getStatus()).isEqualTo(status);
        Assertions.assertThat(response.getError()).isEqualTo(error);
        Assertions.assertThat(response.getMessage()).isEqualTo(message);
    }

    public static void validateError(ApiErrorResponse response,
                                     HttpStatus status,
                                     String error,
                                     String message) {
        assertThat(response).isNotNull();
        assertThat(response.getStatus()).isEqualTo(status);
        assertThat(response.getError()).isEqualTo(error);
        assertThat(response.getMessage()).isEqualTo(message);
    }

    public static void validateTransaction(BuyStockTransaction transaction,
                                           StockTransaction.Type type,
                                           StockTransaction.Status status,
                                           String stock,
                                           int numberOfShares,
                                           String currency,
                                           double pricePerShare,
                                           double brokerageFee,
                                           String transactionDate,
                                           LocalDateTime createdIntervalStart,
                                           LocalDateTime createdIntervalEnd) {
        assertThat(transaction).isNotNull();
        assertThat(transaction.getId()).isNotNull();
        assertThat(transaction.getType()).isEqualTo(type);
        assertThat(transaction.getStatus()).isEqualTo(status);
        Assertions.assertThat(transaction.getStock().getCode()).isEqualTo(stock);
        assertThat(transaction.getNumberOfShares()).isEqualTo(numberOfShares);
        Assertions.assertThat(transaction.getPricePerShare()).isEqualTo(new MoneyAmount(pricePerShare, new Currency(currency)));
        Assertions.assertThat(transaction.getBrokerageFee()).isEqualTo(new MoneyAmount(brokerageFee, new Currency(currency)));
        assertThat(transaction.getTransactionDate().toString()).isEqualTo(transactionDate);
        assertThat(transaction.getCreatedDate()).isBetween(createdIntervalStart, createdIntervalEnd);
        assertThat(transaction.getUpdatedDate()).isEqualTo(transaction.getCreatedDate());
    }

    public static void validateTransaction(SellStockTransaction transaction,
                                           StockTransaction.Type type,
                                           StockTransaction.Status status,
                                           String stock,
                                           int numberOfShares,
                                           String currency,
                                           double pricePerShare,
                                           double brokerageFee,
                                           String transactionDate,
                                           LocalDateTime createdIntervalStart,
                                           LocalDateTime createdIntervalEnd) {
        assertThat(transaction).isNotNull();
        assertThat(transaction.getId()).isNotNull();
        assertThat(transaction.getType()).isEqualTo(type);
        assertThat(transaction.getStatus()).isEqualTo(status);
        Assertions.assertThat(transaction.getStock().getCode()).isEqualTo(stock);
        assertThat(transaction.getNumberOfShares()).isEqualTo(numberOfShares);
        assertThat(transaction.getPricePerShare()).isEqualTo(new MoneyAmount(pricePerShare, new Currency(currency)));
        assertThat(transaction.getBrokerageFee()).isEqualTo(new MoneyAmount(brokerageFee, new Currency(currency)));
        assertThat(transaction.getTransactionDate().toString()).isEqualTo(transactionDate);
        assertThat(transaction.getCreatedDate()).isBetween(createdIntervalStart, createdIntervalEnd);
        assertThat(transaction.getUpdatedDate()).isEqualTo(transaction.getCreatedDate());
    }

    public static void validateTransaction(GetDividendTransaction transaction,
                                           StockTransaction.Type type,
                                           StockTransaction.Status status,
                                           String stock,
                                           int numberOfShares,
                                           String currency,
                                           double pricePerShare,
                                           double brokerageFee,
                                           String transactionDate,
                                           LocalDateTime createdIntervalStart,
                                           LocalDateTime createdIntervalEnd) {
        assertThat(transaction).isNotNull();
        assertThat(transaction.getId()).isNotNull();
        assertThat(transaction.getType()).isEqualTo(type);
        assertThat(transaction.getStatus()).isEqualTo(status);
        Assertions.assertThat(transaction.getStock().getCode()).isEqualTo(stock);
        assertThat(transaction.getNumberOfShares()).isEqualTo(numberOfShares);
        Assertions.assertThat(transaction.getEarningsPerShare()).isEqualTo(new MoneyAmount(pricePerShare, new Currency(currency)));
        Assertions.assertThat(transaction.getDividendFee()).isEqualTo(new MoneyAmount(brokerageFee, new Currency(currency)));
        assertThat(transaction.getTransactionDate().toString()).isEqualTo(transactionDate);
        assertThat(transaction.getCreatedDate()).isBetween(createdIntervalStart, createdIntervalEnd);
        assertThat(transaction.getUpdatedDate()).isEqualTo(transaction.getCreatedDate());
    }

    public static void validateTransactionUpdated(BuyStockTransaction transaction,
                                                  StockTransaction.Type type,
                                                  StockTransaction.Status status,
                                                  String stock,
                                                  int numberOfShares,
                                                  String currency,
                                                  double pricePerShare,
                                                  double brokerageFee,
                                                  String transactionDate,
                                                  LocalDateTime createdIntervalStart,
                                                  LocalDateTime createdIntervalEnd,
                                                  LocalDateTime updatedIntervalStart,
                                                  LocalDateTime updatedIntervalEnd) {
        assertThat(transaction).isNotNull();
        assertThat(transaction.getId()).isNotNull();
        assertThat(transaction.getType()).isEqualTo(type);
        assertThat(transaction.getStatus()).isEqualTo(status);
        Assertions.assertThat(transaction.getStock().getCode()).isEqualTo(stock);
        assertThat(transaction.getNumberOfShares()).isEqualTo(numberOfShares);
        Assertions.assertThat(transaction.getPricePerShare()).isEqualTo(new MoneyAmount(pricePerShare, new Currency(currency)));
        Assertions.assertThat(transaction.getBrokerageFee()).isEqualTo(new MoneyAmount(brokerageFee, new Currency(currency)));
        assertThat(transaction.getTransactionDate().toString()).isEqualTo(transactionDate);
        assertThat(transaction.getCreatedDate()).isBetween(createdIntervalStart, createdIntervalEnd);
        assertThat(transaction.getUpdatedDate()).isBetween(updatedIntervalStart, updatedIntervalEnd);
    }

    public static void validateTransactionUpdated(SellStockTransaction transaction,
                                                  StockTransaction.Type type,
                                                  StockTransaction.Status status,
                                                  String stock,
                                                  int numberOfShares,
                                                  String currency,
                                                  double pricePerShare,
                                                  double brokerageFee,
                                                  String transactionDate,
                                                  LocalDateTime createdIntervalStart,
                                                  LocalDateTime createdIntervalEnd,
                                                  LocalDateTime updatedIntervalStart,
                                                  LocalDateTime updatedIntervalEnd) {
        assertThat(transaction).isNotNull();
        assertThat(transaction.getId()).isNotNull();
        assertThat(transaction.getType()).isEqualTo(type);
        assertThat(transaction.getStatus()).isEqualTo(status);
        Assertions.assertThat(transaction.getStock().getCode()).isEqualTo(stock);
        assertThat(transaction.getNumberOfShares()).isEqualTo(numberOfShares);
        assertThat(transaction.getPricePerShare()).isEqualTo(new MoneyAmount(pricePerShare, new Currency(currency)));
        assertThat(transaction.getBrokerageFee()).isEqualTo(new MoneyAmount(brokerageFee, new Currency(currency)));
        assertThat(transaction.getTransactionDate().toString()).isEqualTo(transactionDate);
        assertThat(transaction.getCreatedDate()).isBetween(createdIntervalStart, createdIntervalEnd);
        assertThat(transaction.getUpdatedDate()).isBetween(updatedIntervalStart, updatedIntervalEnd);
    }

    public static void validateTransactionUpdated(GetDividendTransaction transaction,
                                                  StockTransaction.Type type,
                                                  StockTransaction.Status status,
                                                  String stock,
                                                  int numberOfShares,
                                                  String currency,
                                                  double pricePerShare,
                                                  double brokerageFee,
                                                  String transactionDate,
                                                  LocalDateTime createdIntervalStart,
                                                  LocalDateTime createdIntervalEnd,
                                                  LocalDateTime updatedIntervalStart,
                                                  LocalDateTime updatedIntervalEnd) {
        assertThat(transaction).isNotNull();
        assertThat(transaction.getId()).isNotNull();
        assertThat(transaction.getType()).isEqualTo(type);
        assertThat(transaction.getStatus()).isEqualTo(status);
        Assertions.assertThat(transaction.getStock().getCode()).isEqualTo(stock);
        assertThat(transaction.getNumberOfShares()).isEqualTo(numberOfShares);
        Assertions.assertThat(transaction.getEarningsPerShare()).isEqualTo(new MoneyAmount(pricePerShare, new Currency(currency)));
        Assertions.assertThat(transaction.getDividendFee()).isEqualTo(new MoneyAmount(brokerageFee, new Currency(currency)));
        assertThat(transaction.getTransactionDate().toString()).isEqualTo(transactionDate);
        assertThat(transaction.getCreatedDate()).isBetween(createdIntervalStart, createdIntervalEnd);
        assertThat(transaction.getUpdatedDate()).isBetween(updatedIntervalStart, updatedIntervalEnd);
    }

    public static void validateBuyResource(BuyStockResource resource,
                                           String stockCode,
                                           int numberOfShares,
                                           String currency,
                                           double buyPrice,
                                           double brokerageFee,
                                           String transactionDate) {
        assertThat(resource).isNotNull();
        assertThat(resource.getStockCode()).isEqualTo(stockCode);
        assertThat(resource.getNumberOfShares()).isEqualTo(numberOfShares);
        assertThat(resource.getCurrencyCode()).isEqualTo(currency);
        assertThat(resource.getBuyPrice()).isEqualTo(buyPrice);
        assertThat(resource.getBrokerageFee()).isEqualTo(brokerageFee);
        assertThat(resource.getDateBought()).isEqualTo(transactionDate);
    }

    public static void validateSellResource(SellStockResource resource,
                                            String stockCode,
                                            int numberOfShares,
                                            String currency,
                                            double sellPrice,
                                            double brokerageFee,
                                            String transactionDate) {
        assertThat(resource).isNotNull();
        assertThat(resource.getStockCode()).isEqualTo(stockCode);
        assertThat(resource.getNumberOfShares()).isEqualTo(numberOfShares);
        assertThat(resource.getCurrencyCode()).isEqualTo(currency);
        assertThat(resource.getSellPrice()).isEqualTo(sellPrice);
        assertThat(resource.getBrokerageFee()).isEqualTo(brokerageFee);
        assertThat(resource.getDateSold()).isEqualTo(transactionDate);
    }

    public static void validateGetDividendResource(GetDividendResource resource,
                                                   String stockCode,
                                                   int numberOfShares,
                                                   String currency,
                                                   double earningsPerShare,
                                                   double dividendFee,
                                                   String transactionDate) {
        assertThat(resource).isNotNull();
        assertThat(resource.getStockCode()).isEqualTo(stockCode);
        assertThat(resource.getNumberOfShares()).isEqualTo(numberOfShares);
        assertThat(resource.getCurrencyCode()).isEqualTo(currency);
        assertThat(resource.getEarningsPerShare()).isEqualTo(earningsPerShare);
        assertThat(resource.getDividendFee()).isEqualTo(dividendFee);
        assertThat(resource.getDateEarned()).isEqualTo(transactionDate);
    }

    public static void validateTransactionView(StockTransactionView transaction,
                                               StockTransaction.Type type,
                                               String date,
                                               String stock,
                                               int numberOfShares,
                                               double pricePerShare,
                                               double brokerageFee,
                                               double total,
                                               double adjustedPrice,
                                               String currencyCode) {
        Assertions.assertThat(transaction.getTransactionType()).isEqualTo(type);
        assertThat(transaction.getDate()).isEqualTo(date);
        assertThat(transaction.getStockCode()).isEqualTo(stock);
        assertThat(transaction.getNumberOfShares()).isEqualTo(numberOfShares);
        assertThat(transaction.getPrice()).isEqualTo(pricePerShare);
        assertThat(transaction.getFee()).isEqualTo(brokerageFee);
        assertThat(transaction.getTotalMovement()).isEqualTo(total);
        assertThat(transaction.getAdjustedPrice()).isEqualTo(adjustedPrice);
        assertThat(transaction.getCurrency()).isEqualTo(currencyCode);
    }

    public static void validateStockJournal(StockJournal stockJournal,
                                            String stockCode,
                                            int numberOfShares,
                                            String currency,
                                            double averagePrice,
                                            String lastBoughtDate,
                                            String lastSoldDate,
                                            LocalDateTime updatedIntervalStart,
                                            LocalDateTime updatedIntervalEnd) {
        assertThat(stockJournal).isNotNull();
        assertThat(stockJournal.getStockCode()).isEqualTo(stockCode);
        assertThat(stockJournal.getNumberOfShares()).isEqualTo(numberOfShares);
        Assertions.assertThat(stockJournal.getAveragePricePerShare()).isEqualTo(new MoneyAmount(averagePrice, new Currency(currency)));
        assertThat(stockJournal.getLastBoughtDate().toString()).isEqualTo(lastBoughtDate);
        assertThat(stockJournal.getLastSoldDate().toString()).isEqualTo(lastSoldDate);
        assertThat(stockJournal.getUpdatedDate()).isBetween(updatedIntervalStart, updatedIntervalEnd);
    }

    public static void validateStockJournalView(StockJournalView stockJournalView,
                                                String stockCode,
                                                int numberOfShares,
                                                double averagePrice,
                                                String lastBoughtDate,
                                                double marketValue) {
        assertThat(stockJournalView.getStockCode()).isEqualTo(stockCode);
        assertThat(stockJournalView.getAveragePrice()).isEqualTo(averagePrice);
        assertThat(stockJournalView.getNumberOfShares()).isEqualTo(numberOfShares);
        assertThat(stockJournalView.getBoughtDate().toString()).isEqualTo(lastBoughtDate);
        assertThat(stockJournalView.getMarketValue()).isEqualTo(marketValue);
    }

    public static void validateMonthlyProfitSummary(MonthlyProfitSummary monthlyProfitSummary,
                                                    String currencyCode,
                                                    double averageBoughtPrice,
                                                    double averageSoldPrice,
                                                    int numberOfShares,
                                                    double totalProfit,
                                                    String month,
                                                    String stock,
                                                    LocalDateTime updatedIntervalStart,
                                                    LocalDateTime updatedIntervalEnd) {
        assertThat(monthlyProfitSummary).isNotNull();
        assertThat(monthlyProfitSummary.getId()).isNotNull();
        assertThat(monthlyProfitSummary.getAverageBoughtPrice()).isEqualTo(new MoneyAmount(averageBoughtPrice, new Currency(currencyCode)));
        assertThat(monthlyProfitSummary.getAverageSoldPrice()).isEqualTo(new MoneyAmount(averageSoldPrice, new Currency(currencyCode)));
        assertThat(monthlyProfitSummary.getNumberOfSharesSold()).isEqualTo(numberOfShares);
        assertThat(monthlyProfitSummary.getCurrency()).isEqualTo(new Currency(currencyCode));
        assertThat(monthlyProfitSummary.getTotalProfit()).isEqualTo(new MoneyAmount(totalProfit, new Currency(currencyCode)));
        assertThat(monthlyProfitSummary.getMonth().toString()).isEqualTo(month);
        assertThat(monthlyProfitSummary.getStock()).isEqualTo(new Stock(stock));
        assertThat(monthlyProfitSummary.getUpdatedDate()).isBetween(updatedIntervalStart, updatedIntervalEnd);
    }

    public static void validateSummaryForMonthView(SummaryForMonthView summary,
                                                   double averageBoughtPrice,
                                                   double averageSoldPrice,
                                                   int numberOfShares,
                                                   String currency,
                                                   double totalProfit) {
        assertThat(summary.getAverageBoughtPrice()).isEqualTo(averageBoughtPrice);
        assertThat(summary.getAverageSoldPrice()).isEqualTo(averageSoldPrice);
        assertThat(summary.getNumberOfShares()).isEqualTo(numberOfShares);
        assertThat(summary.getCurrency()).isEqualTo(currency);
        assertThat(summary.getProfit()).isEqualTo(totalProfit);
    }
}
