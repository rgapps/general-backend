package com.rgapps.stocks.datahelper;

import com.rgapps.common.exception.modelexceptions.ModelErrorResponse;
import com.rgapps.stocks.controller.resource.BuyStockResource;
import com.rgapps.stocks.controller.resource.GetDividendResource;
import com.rgapps.stocks.controller.resource.SellStockResource;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.junit.jupiter.params.provider.Arguments;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class DataHelper {
    public static class StringList extends ArrayList<String> {
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class ModelErrorResponseBuyStockResource extends ModelErrorResponse<BuyStockResource> {
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class ModelErrorResponseSellStockResource extends ModelErrorResponse<SellStockResource> {
    }

    @EqualsAndHashCode(callSuper = true)
    @Data
    public static class ModelErrorResponseGetDividendResource extends ModelErrorResponse<GetDividendResource> {
    }

    public static Stream<Arguments> buyStockValidationsBodyCases() {
        return Stream.of(
                Arguments.of("INVALID JSON",
                        Map.of(
                                "buyResource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{}",
                        Map.of(
                                "stockCode", List.of("must not be empty"),
                                "numberOfShares", List.of("must not be null"),
                                "currencyCode", List.of("must not be empty"),
                                "buyPrice", List.of("must not be null"),
                                "dateBought", List.of("must not be empty"),
                                "brokerageFee", List.of("must not be null")
                        )),
                Arguments.of("{\"stockCode\": 0, \"numberOfShares\": -1, \"currencyCode\":\"\", \"buyPrice\": -1, \"dateBought\": \"\", " +
                                "\"brokerageFee\": -1}}",
                        Map.of(
                                "numberOfShares", List.of("must be greater than or equal to 1"),
                                "currencyCode", List.of("must not be empty"),
                                "buyPrice", List.of("must be greater than or equal to 0"),
                                "dateBought", List.of("must not be empty"),
                                "brokerageFee", List.of("must be greater than or equal to 0")
                        )),
                Arguments.of("{\"stockCode\": \"ABC\", \"numberOfShares\": 1, \"currencyCode\":\"AED\", \"buyPrice\": 10.0, " +
                                "\"dateBought\": \"invalid\", \"brokerageFee\": 1.0}",
                        Map.of(
                                "dateBought", List.of("must be a date in the past or in the present", "must be a valid YYYY-MM-DD date")
                        )),
                Arguments.of("{\"stockCode\": \"ABC\", \"numberOfShares\": 1, \"currencyCode\":\"AED\", \"buyPrice\": 10.0, " +
                                "\"dateBought\": \"3020-12-07\", \"brokerageFee\": 1.0}",
                        Map.of("dateBought", List.of("must be a date in the past or in the present")
                        )));
    }

    public static Stream<Arguments> sellStockValidationsBodyCases() {
        return Stream.of(
                Arguments.of("INVALID JSON",
                        Map.of(
                                "sellResource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{}",
                        Map.of(
                                "stockCode", List.of("must not be empty"),
                                "numberOfShares", List.of("must not be null"),
                                "currencyCode", List.of("must not be empty"),
                                "sellPrice", List.of("must not be null"),
                                "dateSold", List.of("must not be empty"),
                                "brokerageFee", List.of("must not be null")
                        )),
                Arguments.of("{\"stockCode\": 0, \"numberOfShares\": -1, \"currencyCode\":\"\", \"sellPrice\": -1, \"dateSold\": \"\", " +
                                "\"brokerageFee\": -1}}",
                        Map.of(
                                "numberOfShares", List.of("must be greater than or equal to 1"),
                                "currencyCode", List.of("must not be empty"),
                                "sellPrice", List.of("must be greater than or equal to 0"),
                                "dateSold", List.of("must not be empty"),
                                "brokerageFee", List.of("must be greater than or equal to 0")
                        )),
                Arguments.of("{\"stockCode\": \"ABC\", \"numberOfShares\": 1, \"currencyCode\":\"AED\", \"sellPrice\": 10.0, " +
                                "\"dateSold\": \"invalid\", \"brokerageFee\": 1.0}",
                        Map.of(
                                "dateSold", List.of("must be a date in the past or in the present", "must be a valid YYYY-MM-DD date")
                        )),
                Arguments.of("{\"stockCode\": \"ABC\", \"numberOfShares\": 1, \"currencyCode\":\"AED\", \"sellPrice\": 10.0, " +
                                "\"dateSold\": \"3020-12-07\", \"brokerageFee\": 1.0}",
                        Map.of("dateSold", List.of("must be a date in the past or in the present")
                        )));
    }

    public static Stream<Arguments> getDividendValidationsBodyCases() {
        return Stream.of(
                Arguments.of("INVALID JSON",
                        Map.of(
                                "getDividendResource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{}",
                        Map.of(
                                "stockCode", List.of("must not be empty"),
                                "numberOfShares", List.of("must not be null"),
                                "currencyCode", List.of("must not be empty"),
                                "earningsPerShare", List.of("must not be null"),
                                "dateEarned", List.of("must not be empty"),
                                "dividendFee", List.of("must not be null")
                        )),
                Arguments.of("{\"stockCode\": 0, \"numberOfShares\": -1, \"currencyCode\":\"\", \"earningsPerShare\": -1, \"dateEarned\": \"\", " +
                                "\"dividendFee\": -1}}",
                        Map.of(
                                "numberOfShares", List.of("must be greater than or equal to 1"),
                                "currencyCode", List.of("must not be empty"),
                                "earningsPerShare", List.of("must be greater than or equal to 0"),
                                "dateEarned", List.of("must not be empty"),
                                "dividendFee", List.of("must be greater than or equal to 0")
                        )),
                Arguments.of("{\"stockCode\": \"ABC\", \"numberOfShares\": 1, \"currencyCode\":\"AED\", \"earningsPerShare\": 10.0, " +
                                "\"dateEarned\": \"invalid\", \"dividendFee\": 1.0}",
                        Map.of(
                                "dateEarned", List.of("must be a date in the past or in the present", "must be a valid YYYY-MM-DD date")
                        )),
                Arguments.of("{\"stockCode\": \"ABC\", \"numberOfShares\": 1, \"currencyCode\":\"AED\", \"earningsPerShare\": 10.0, " +
                                "\"dateEarned\": \"3020-12-07\", \"dividendFee\": 1.0}",
                        Map.of("dateEarned", List.of("must be a date in the past or in the present")
                        )));
    }

    public static Stream<Arguments> findMonthlyReturnsNotFoundForInvalidPathParametersCases() {
        return Stream.of(
                Arguments.of("year/month", Map.of("year", List.of("Type mismatch."))),
                Arguments.of("2021/month", Map.of("month", List.of("Type mismatch."))),
                Arguments.of("-2021/01", Map.of("year", List.of("must be greater than or equal to 0"))),
                Arguments.of("2021/-1", Map.of("month", List.of("must be greater than or equal to 0"))),
                Arguments.of("2021/13", Map.of("month", List.of("must be less than or equal to 12"))));
    }

    public static final String DEFAULT_BUY = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"AED\", " +
            "\"buyPrice\": 10.0, " +
            "\"dateBought\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String BUY_10000_STOCKS = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 10000, " +
            "\"currencyCode\":\"AED\", " +
            "\"buyPrice\": 10.0, " +
            "\"dateBought\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String BUY_ANOTHER_CURRENCY_USD = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"USD\", " +
            "\"buyPrice\": 10.0, " +
            "\"dateBought\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String BUY_NEXT_DAY = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"AED\", " +
            "\"buyPrice\": 10.0, " +
            "\"dateBought\": \"2020-12-08\", " +
            "\"brokerageFee\": 1.0}";

    public static final String BUY_BEFORE = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"AED\", " +
            "\"buyPrice\": 10.0, " +
            "\"dateBought\": \"2020-12-06\", " +
            "\"brokerageFee\": 1.0}";

    public static final String BUY_TWO_SHARES = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 2, " +
            "\"currencyCode\":\"AED\", " +
            "\"buyPrice\": 10.0, " +
            "\"dateBought\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String BUY_2_FOR_20 = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 2, " +
            "\"currencyCode\":\"AED\", " +
            "\"buyPrice\": 20.0, " +
            "\"dateBought\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String BUY_3_FOR_TWENTY = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 3, " +
            "\"currencyCode\":\"AED\", " +
            "\"buyPrice\": 20.0, " +
            "\"dateBought\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String BUY_ANOTHER_STOCK_DEF = "{" +
            "\"stockCode\": \"DEF\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"AED\", " +
            "\"buyPrice\": 10.0, " +
            "\"dateBought\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String BUY_ANOTHER_STOCK_GHI = "{" +
            "\"stockCode\": \"GHI\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"AED\", " +
            "\"buyPrice\": 10.0, " +
            "\"dateBought\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String DEFAULT_SELL = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"AED\", " +
            "\"sellPrice\": 10.0, " +
            "\"dateSold\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String SELL_NEXT_DAY = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"AED\", " +
            "\"sellPrice\": 10.0, " +
            "\"dateSold\": \"2020-12-08\", " +
            "\"brokerageFee\": 1.0}";

    public static final String SELL_ANOTHER_CURRENCY = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"USD\", " +
            "\"sellPrice\": 10.0, " +
            "\"dateSold\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String SELL_BEFORE = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"AED\", " +
            "\"sellPrice\": 10.0, " +
            "\"dateSold\": \"2020-12-06\", " +
            "\"brokerageFee\": 1.0}";

    public static final String SELL_TWO_SHARES = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 2, " +
            "\"currencyCode\":\"AED\", " +
            "\"sellPrice\": 10.0, " +
            "\"dateSold\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String SELL_FOR_TWENTY = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"AED\", " +
            "\"sellPrice\": 20.0, " +
            "\"dateSold\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String SELL_2_FOR_THIRTY = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 2, " +
            "\"currencyCode\":\"AED\", " +
            "\"sellPrice\": 30.0, " +
            "\"dateSold\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String SELL_2_WITH_2_BROKERAGE_FEE = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 2, " +
            "\"currencyCode\":\"AED\", " +
            "\"sellPrice\": 10.0, " +
            "\"dateSold\": \"2020-12-07\", " +
            "\"brokerageFee\": 2.0}";

    public static final String SELL_ANOTHER_STOCK = "{" +
            "\"stockCode\": \"DEF\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"AED\", " +
            "\"sellPrice\": 10.0, " +
            "\"dateSold\": \"2020-12-07\", " +
            "\"brokerageFee\": 1.0}";

    public static final String DEFAULT_GET_DIVIDEND = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"AED\", " +
            "\"earningsPerShare\": 10.0, " +
            "\"dateEarned\": \"2020-12-07\", " +
            "\"dividendFee\": 1.0}";

    public static final String GET_DIVIDEND_ANOTHER_CURRENCY = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 1, " +
            "\"currencyCode\":\"USD\", " +
            "\"earningsPerShare\": 10.0, " +
            "\"dateEarned\": \"2020-12-07\", " +
            "\"dividendFee\": 1.0}";

    public static final String GET_DIVIDEND_TWO_SHARES = "{" +
            "\"stockCode\": \"ABC\", " +
            "\"numberOfShares\": 2, " +
            "\"currencyCode\":\"AED\", " +
            "\"earningsPerShare\": 10.0, " +
            "\"dateEarned\": \"2020-12-07\", " +
            "\"dividendFee\": 1.0}";
}
