package com.rgapps.stocks.changelog;

import com.rgapps.accounts.model.Account;
import com.rgapps.stocks.IntegrationTest;
import com.rgapps.stocks.datahelper.ModelData;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.MonthlyProfitSummary;
import com.rgapps.stocks.model.SellStockTransaction;
import com.rgapps.stocks.model.StockJournal;
import com.rgapps.stocks.model.StockTransaction;
import com.rgapps.stocks.model.values.Currency;
import com.rgapps.stocks.model.values.MoneyAmount;
import com.rgapps.stocks.model.values.Stock;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.time.LocalDate;
import java.util.UUID;

import static com.rgapps.stocks.datahelper.ModelData.defaultBuyTransaction;
import static com.rgapps.stocks.datahelper.ModelData.defaultDividendTransaction;
import static com.rgapps.stocks.datahelper.ModelData.defaultMonthlyProfitSummary;
import static com.rgapps.stocks.datahelper.ModelData.defaultSellTransaction;
import static com.rgapps.stocks.datahelper.ModelData.defaultStock;
import static com.rgapps.stocks.datahelper.ModelData.defaultStockJournal;
import static org.assertj.core.api.Assertions.assertThat;

@Disabled("Embedded mongo not working inside docker")
class MigrationTests extends IntegrationTest {
    @Autowired
    private MongoTemplate mongoTemplate;

    Migration migration = new Migration();

    @Test
    public void emptyRenameCollections() {
        migration.renameCollections(() -> mongoTemplate);

        StockJournal journal = mongoTemplate.findById(ModelData.defaultStock.getCode(), StockJournal.class);
        assertThat(journal).isNull();

        MonthlyProfitSummary profitSummary = mongoTemplate.findById(ModelData.defaultStock.getCode(), MonthlyProfitSummary.class);
        assertThat(profitSummary).isNull();
    }

    @Test
    @Disabled("Legacy update. Model has changed")
    public void populatedRenameCollections() {
        mongoTemplate.dropCollection(StockJournal.class);
        mongoTemplate.dropCollection(MonthlyProfitSummary.class);
        StockJournal journal = mongoTemplate.save(defaultStockJournal, "stock-summary");
        MonthlyProfitSummary summary = mongoTemplate.save(ModelData.defaultMonthlyProfitSummary, "monthly-gains");

        migration.renameCollections(() -> mongoTemplate);

        StockJournal updatedJournal = mongoTemplate.findById(journal.getStockCode(), StockJournal.class);
        assertThat(updatedJournal).isNotNull();
        assertThat(updatedJournal).isEqualTo(defaultStockJournal);

        MonthlyProfitSummary updatedSummary = mongoTemplate.findById(summary.getId(), MonthlyProfitSummary.class);
        assertThat(updatedSummary).isNotNull();
        assertThat(updatedSummary).isEqualTo(ModelData.defaultMonthlyProfitSummary);
    }

    @Test
    @Disabled("Legacy update. Model has changed")
    public void addBrokerageAndLastSoldDateToExistent() {
        BuyStockTransaction buyTransaction = mongoTemplate.save(ModelData.buyTransactionWithNullBrokerageFee);
        SellStockTransaction sellTransaction = mongoTemplate.save(ModelData.defaultSellTransaction());
        StockJournal journal = mongoTemplate.save(ModelData.stockJournalWithNullLastSoldDate);

        migration.addBrokerageFeeAndLastSoldDate(() -> mongoTemplate);

        BuyStockTransaction buyTransactionUpdated = mongoTemplate.findById(buyTransaction.getId(), BuyStockTransaction.class);
        assertThat(buyTransactionUpdated).isNotNull();
        Assertions.assertThat(buyTransactionUpdated.getBrokerageFee()).isEqualTo(MoneyAmount.zeroAmount);

        SellStockTransaction sellTransactionUpdated = mongoTemplate.findById(sellTransaction.getId(), SellStockTransaction.class);
        assertThat(sellTransactionUpdated).isNotNull();
        Assertions.assertThat(sellTransactionUpdated.getBrokerageFee()).isEqualTo(new MoneyAmount(2, new Currency("AED")));

        StockJournal journalUpdated = mongoTemplate.findById(journal.getStockCode(), StockJournal.class);
        assertThat(journalUpdated).isNotNull();
        assertThat(journalUpdated.getLastSoldDate()).isEqualTo(LocalDate.EPOCH);
    }

    @Test
    public void updateClassesDueToRefactor() {
        BuyStockTransaction buyTransaction = mongoTemplate.save(ModelData.buyTransactionWithNullBrokerageFee);
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_class").is("com.wilsonr990.page.stocks.model.BuyStockTransaction")),
                new Update().set("_class", "com.wilsonr990.stocks.model.BuyStockTransaction"), StockTransaction.class);
        SellStockTransaction sellTransaction = mongoTemplate.save(ModelData.defaultSellTransaction());
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_class").is("com.wilsonr990.page.stocks.model.SellStockTransaction")),
                new Update().set("_class", "com.wilsonr990.stocks.model.BuyStockTransaction"), StockTransaction.class);
        StockJournal journal = mongoTemplate.save(ModelData.stockJournalWithNullLastSoldDate);
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_class").is("com.wilsonr990.page.stocks.model.StockJournal")),
                new Update().set("_class", "com.wilsonr990.stocks.model.StockJournal"), StockJournal.class);
        MonthlyProfitSummary monthlyProfitSummary = mongoTemplate.save(ModelData.defaultMonthlyProfitSummary);
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_class").is("com.wilsonr990.page.stocks.model.MonthlyProfitSummary")),
                new Update().set("_class", "com.wilsonr990.stocks.model.MonthlyProfitSummary"), MonthlyProfitSummary.class);
        Currency currency = mongoTemplate.save(ModelData.defaultCurrency);
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_class").is("com.wilsonr990.page.stocks.model.Currency")),
                new Update().set("_class", "com.wilsonr990.stocks.model.Currency"), Currency.class);
        Stock stock = mongoTemplate.save(ModelData.defaultStock);
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_class").is("com.wilsonr990.page.stocks.model.Stock")),
                new Update().set("_class", "com.wilsonr990.stocks.model.Stock"), Stock.class);

        migration.updateClassesDueToRefactor(() -> mongoTemplate);

        mongoTemplate.findById(buyTransaction.getId(), StockTransaction.class);
        mongoTemplate.findById(sellTransaction.getId(), StockTransaction.class);
        mongoTemplate.findById(journal.getStockCode(), StockJournal.class);
        mongoTemplate.findById(monthlyProfitSummary.getId(), MonthlyProfitSummary.class);
        mongoTemplate.findById(currency.getCode(), Currency.class);
        mongoTemplate.findById(stock.getCode(), Stock.class);
    }

    @Test
    public void removeFailedTransactions() {
        BuyStockTransaction buyTransactionFailed = mongoTemplate.save(ModelData.defaultBuyTransaction());
        mongoTemplate.updateMulti(new Query(), new Update().set("status", "FAILED"), "stock-transaction");
        SellStockTransaction sellTransactionFailed = mongoTemplate.save(ModelData.defaultSellTransaction());
        mongoTemplate.updateMulti(new Query(), new Update().set("status", "FAILED"), "stock-transaction");
        BuyStockTransaction buyTransaction = mongoTemplate.save(ModelData.defaultBuyTransaction());
        SellStockTransaction sellTransaction = mongoTemplate.save(ModelData.defaultSellTransaction());

        migration.removeFailedTransactionsFix(() -> mongoTemplate);

        assertThat(mongoTemplate.count(new Query(), "stock-transaction")).isEqualTo(2);
        assertThat(mongoTemplate.findAll(StockTransaction.class)).contains(buyTransaction, sellTransaction);
        assertThat(mongoTemplate.findAll(StockTransaction.class)).doesNotContain(buyTransactionFailed, sellTransactionFailed);
    }

    @Test
    public void removeCurrenciesCollection() {
        mongoTemplate.save(ModelData.defaultCurrency, "currencies");

        migration.removeCurrenciesCollection(() -> mongoTemplate);

        assertThat(mongoTemplate.collectionExists("currencies")).isFalse();
    }

    @Test
    public void recalculateJourneyAndSummaryIds() {
        Account account = new Account("DEFAULT", new com.rgapps.accounts.model.values.Currency("AED"), UUID.randomUUID());
        account.setId(ModelData.defaultAccountId);
        mongoTemplate.save(account);

        defaultStockJournal.setId(defaultStockJournal.getStockCode());
        mongoTemplate.save(defaultStockJournal);
        mongoTemplate.updateMulti(new Query(), new Update().set("accountId", null), StockJournal.class);
        mongoTemplate.updateMulti(new Query(), new Update().set("stockCode", null), StockJournal.class);

        ModelData.defaultMonthlyProfitSummary.setId(
                ModelData.defaultMonthlyProfitSummary.getMonth() + ModelData.defaultMonthlyProfitSummary.getStock().getCode());
        mongoTemplate.save(ModelData.defaultMonthlyProfitSummary);
        mongoTemplate.updateMulti(new Query(), new Update().set("accountId", null), MonthlyProfitSummary.class);

        migration.recalculateJourneyAndSummaryIds(() -> mongoTemplate);

        defaultStockJournal.setId(StockJournal.calculateId(defaultStockJournal.getStockCode(), account.getId()));
        assertThat(mongoTemplate.findAll(StockJournal.class)).hasSize(1);
        assertThat(mongoTemplate.findOne(new Query(), StockJournal.class)).isEqualTo(defaultStockJournal);
        ModelData.defaultMonthlyProfitSummary.setId(MonthlyProfitSummary.calculateId(ModelData.defaultMonthlyProfitSummary.getMonth(),
                ModelData.defaultMonthlyProfitSummary.getStock(),
                account.getId()));
        assertThat(mongoTemplate.findAll(MonthlyProfitSummary.class)).hasSize(1);
        assertThat(mongoTemplate.findOne(new Query(), MonthlyProfitSummary.class)).isEqualTo(ModelData.defaultMonthlyProfitSummary);
    }

    @Test
    public void changeModuleNames() {
        Stock stock = mongoTemplate.save(defaultStock);
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("code").is(stock.getCode())),
                new Update().set("_class", "com.wilsonr990.page.stocks.model.StockJournal"), "account");
        StockJournal stockJournal = mongoTemplate.save(defaultStockJournal);
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(stockJournal.getId())),
                new Update().set("_class", "com.wilsonr990.page.stocks.model.StockJournal"), "account");
        MonthlyProfitSummary profitSummary = mongoTemplate.save(defaultMonthlyProfitSummary);
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(profitSummary.getId())),
                new Update().set("_class", "com.wilsonr990.page.stocks.model.MonthlyProfitSummary"), "account-movement");
        BuyStockTransaction buyTransaction = mongoTemplate.save(defaultBuyTransaction());
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(buyTransaction.getId())),
                new Update().set("_class", "com.wilsonr990.page.stocks.model.BuyStockTransaction"), "account-movement");
        SellStockTransaction sellTransaction = mongoTemplate.save(defaultSellTransaction());
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(buyTransaction.getId())),
                new Update().set("_class", "com.wilsonr990.page.stocks.model.SellStockTransaction"), "account-movement");
        GetDividendTransaction dividendTransaction = mongoTemplate.save(defaultDividendTransaction());
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(buyTransaction.getId())),
                new Update().set("_class", "com.wilsonr990.page.stocks.model.DividendStockTransaction"), "account-movement");

        migration.changeModuleNamesStocks(() -> mongoTemplate);

        Stock updatedStock =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(stock.getCode())), Stock.class);
        StockJournal updatedJournal =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(stockJournal.getId())), StockJournal.class);
        MonthlyProfitSummary udpatedSummary =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(profitSummary.getId())), MonthlyProfitSummary.class);
        BuyStockTransaction udpatedBuy =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(buyTransaction.getId())), BuyStockTransaction.class);
        SellStockTransaction udpatedSell =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(sellTransaction.getId())), SellStockTransaction.class);
        GetDividendTransaction udpatedDividend =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(dividendTransaction.getId())), GetDividendTransaction.class);
        assertThat(updatedStock).isEqualTo(stock);
        assertThat(updatedJournal).isEqualTo(stockJournal);
        assertThat(udpatedSummary).isEqualTo(profitSummary);
        assertThat(udpatedBuy).isEqualTo(buyTransaction);
        assertThat(udpatedSell).isEqualTo(sellTransaction);
        assertThat(udpatedDividend).isEqualTo(dividendTransaction);
    }
}
