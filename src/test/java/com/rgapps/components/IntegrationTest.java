package com.rgapps.components;

import com.rgapps.components.model.BusinessCardComponent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class IntegrationTest {
  @Autowired
  protected ReactiveMongoTemplate mongoTemplate;

  @Autowired
  protected WebTestClient webTestClient;

  @BeforeEach
  public void clean() {
    mongoTemplate.findAllAndRemove(new Query(), BusinessCardComponent.class).collectList().block();
  }

  protected WebTestClient.ResponseSpec createComponent(String body) {
    return webTestClient.post()
          .uri("/api/components")
          .contentType(MediaType.APPLICATION_JSON)
          .bodyValue(body)
          .exchange()
          .expectStatus().isOk();
  }
}
