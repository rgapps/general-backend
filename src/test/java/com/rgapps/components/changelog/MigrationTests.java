package com.rgapps.components.changelog;

import com.rgapps.components.IntegrationTest;
import com.rgapps.components.datahelper.ModelData;
import com.rgapps.components.model.BusinessCardComponent;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled("Embedded mongo not working inside docker")
public class MigrationTests extends IntegrationTest {
    @Autowired
    private MongoTemplate mongoTemplate;

    Migration migration = new Migration();

    @Test
    public void addStatusAndIdToComponents() {
        BusinessCardComponent businessCard = mongoTemplate.save(ModelData.defaultBusinessCardComponent);
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(businessCard.getId())),
                new Update().unset("status"), "component");

        migration.addStatusAndIdToComponents(() -> mongoTemplate);

        BusinessCardComponent updatedBusinessCard =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(businessCard.getId())), BusinessCardComponent.class);

        assertThat(updatedBusinessCard).isEqualTo(businessCard);
    }

    @Test
    public void changeModuleNames() {
        BusinessCardComponent component = mongoTemplate.save(ModelData.defaultBusinessCardComponent);
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(component.getId())),
                new Update().set("_class", "com.wilsonr990.page.components.model.UseBusinessCardComponent"), "user");

        migration.changeModuleNamesComponents(() -> mongoTemplate);

        BusinessCardComponent updatedComponent =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(component.getId())), BusinessCardComponent.class);
        assertThat(updatedComponent).isEqualTo(component);
    }
}
