package com.rgapps.components.datahelper;

import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.apiexceptions.ValidationErrorResponse;
import com.rgapps.common.exception.modelexceptions.ModelErrorResponse;
import com.rgapps.components.controller.resource.ComponentView;
import com.rgapps.components.model.BusinessCardComponent;
import com.rgapps.components.model.Component;
import org.assertj.core.api.Assertions;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class ObjectValidations {
  public static void validateError(ValidationErrorResponse response, HttpStatus status, String error, String message,
                                   Map<String, List<String>> expectedErrors) {
    assertThat(response).isNotNull();
    Assertions.assertThat(response.getStatus()).isEqualTo(status);
    Assertions.assertThat(response.getError()).isEqualTo(error);
    Assertions.assertThat(response.getMessage()).isEqualTo(message);
    Map<String, List<String>> detail = response.getErrors();
    assertThat(detail).hasSameSizeAs(expectedErrors);
    assertThat(detail.keySet()).hasSameElementsAs(expectedErrors.keySet());
    detail.forEach((field, errors) -> assertThat(errors).hasSameElementsAs(expectedErrors.get(field)));
  }

  public static void validateError(ModelErrorResponse<?> response, HttpStatus status, String error, String message) {
    assertThat(response).isNotNull();
    Assertions.assertThat(response.getStatus()).isEqualTo(status);
    Assertions.assertThat(response.getError()).isEqualTo(error);
    Assertions.assertThat(response.getMessage()).isEqualTo(message);
  }

  public static void validateError(ApiErrorResponse response, HttpStatus status, String error, String message) {
    assertThat(response).isNotNull();
    assertThat(response.getStatus()).isEqualTo(status);
    assertThat(response.getError()).isEqualTo(error);
    assertThat(response.getMessage()).isEqualTo(message);
  }

  public static void validateBusinessCardComponent(BusinessCardComponent component, String urlImageLogo,
                                                   String name, String profession, String componentName,
                                                   LocalDateTime createdIntervalStart, LocalDateTime createdIntervalEnd) {
    assertThat(component).isNotNull();
    assertThat(component.getId()).isNotNull();
    assertThat(component.getUrlImageLogo()).isEqualTo(urlImageLogo);
    assertThat(component.getName()).isEqualTo(name);
    assertThat(component.getProfession()).isEqualTo(profession);
    assertThat(component.getComponentName()).isEqualTo(componentName);
    assertThat(component.getCreatedDate()).isBetween(createdIntervalStart, createdIntervalEnd);
  }

  public static void validateComponentView(ComponentView component, Component.Type type,
                                           String componentName, UUID componentId, Component.Status componentStatus) {
    assertThat(component).isNotNull();
    Assertions.assertThat(component.getComponentType()).isEqualTo(type);
    assertThat(component.getComponentName()).isEqualTo(componentName);
    assertThat(component.getComponentId()).isEqualTo(componentId);
    Assertions.assertThat(component.getComponentStatus()).isEqualTo(componentStatus);
  }
}

