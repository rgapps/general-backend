package com.rgapps.components.datahelper;

import org.junit.jupiter.params.provider.Arguments;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class DataHelper {
    public static Stream<Arguments> createComponentFailBodyCases() {
        return Stream.of(
                Arguments.of("INVALID JSON}",
                        Map.of(
                                "resource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{}",
                        Map.of(
                                "resource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{\"type\": \"nonExistent\"}",
                        Map.of(
                                "resource", List.of("Failed to read HTTP message")
                        )),
                Arguments.of("{\"type\": \"BUSINESS_CARD\"}",
                        Map.of(
                                "urlImageLogo", List.of("must not be empty"),
                                "name", List.of("must not be empty"),
                                "profession", List.of("must not be empty"),
                                "componentName", List.of("must not be empty")
                        )),
                Arguments.of("{\"type\": \"BUSINESS_CARD\", \"urlImageLogo\": \"\", \"name\": \"\", \"profession\": \"\", " +
                                "\"componentName\": \"\"}",
                        Map.of(
                                "urlImageLogo", List.of("must not be empty"),
                                "name", List.of("must not be empty"),
                                "profession", List.of("must not be empty"),
                                "componentName", List.of("must not be empty")
                        )));
    }

    public static final String DEFAULT_BUSINESS_CARD = "{" +
            "\"type\": \"BUSINESS_CARD\", " +
            "\"urlImageLogo\": \"someUrl.jpg\", " +
            "\"name\": \"someName\", " +
            "\"profession\": \"someProfession\"," +
            "\"componentName\": \"someComponentName\"}";

    public static final String ANOTHER_BUSINESS_CARD = "{" +
            "\"type\": \"BUSINESS_CARD\", " +
            "\"urlImageLogo\": \"anotherUrl.jpg\", " +
            "\"name\": \"anotherName\", " +
            "\"profession\": \"anotherProfession\"," +
            "\"componentName\": \"anotherComponentName\"}";
}
