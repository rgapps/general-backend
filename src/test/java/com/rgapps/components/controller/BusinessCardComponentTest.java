package com.rgapps.components.controller;

import com.rgapps.common.exception.apiexceptions.ValidationErrorResponse;
import com.rgapps.components.IntegrationTest;
import com.rgapps.components.datahelper.DataHelper;
import com.rgapps.components.datahelper.ObjectValidations;
import com.rgapps.components.model.BusinessCardComponent;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static com.rgapps.components.datahelper.ObjectValidations.validateBusinessCardComponent;
import static org.assertj.core.api.Assertions.assertThat;

@Disabled("Embedded mongo not working inside docker")
class BusinessCardComponentTest extends IntegrationTest {

    @ParameterizedTest
    @MethodSource("com.rgapps.components.datahelper.DataHelper#createComponentFailBodyCases")
    public void createComponentFailWithValidationErrors(String resource, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = webTestClient.post()
                .uri("/api/components")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);

        assertThat(mongoTemplate.findAll(BusinessCardComponent.class).collectList().block()).hasSize(0);
    }

    @Test
    public void createBusinessCardComponentCorrectly() {
        LocalDateTime start = LocalDateTime.now();
        BusinessCardComponent response = createComponent(DataHelper.DEFAULT_BUSINESS_CARD)
                .expectBody(BusinessCardComponent.class)
                .returnResult()
                .getResponseBody();

        validateBusinessCardComponent(response, "someUrl.jpg", "someName", "someProfession", "someComponentName", start, LocalDateTime.now());

        assertThat(mongoTemplate.findAll(BusinessCardComponent.class).collectList().block()).hasSize(1);
        assertThat(response).isNotNull();
        BusinessCardComponent registry = mongoTemplate.findById(response.getId(), BusinessCardComponent.class).block();
        assertThat(registry).isEqualTo(response);
    }
}
