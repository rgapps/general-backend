package com.rgapps.components.controller;

import com.rgapps.components.IntegrationTest;
import com.rgapps.components.controller.resource.ComponentView;
import com.rgapps.components.datahelper.DataHelper;
import com.rgapps.components.datahelper.ObjectValidations;
import com.rgapps.components.model.Component;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled("Embedded mongo not working inside docker")
class ComponentTest extends IntegrationTest {

    @Test
    public void getComponentsReturnsAnEmptyListWhenNoComponents() {
        List<ComponentView> response = webTestClient.get()
                .uri("/api/components")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(ComponentView.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response).hasSize(0);
    }

    @Test
    public void getComponentsReturnsCorrectlyTheComponents() {
        Component firstComponent = createComponent(DataHelper.DEFAULT_BUSINESS_CARD)
                .expectBody(Component.class)
                .returnResult()
                .getResponseBody();

        Component secondComponent = createComponent(DataHelper.ANOTHER_BUSINESS_CARD)
                .expectBody(Component.class)
                .returnResult()
                .getResponseBody();

        List<ComponentView> response = webTestClient.get()
                .uri("/api/components")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(ComponentView.class)
                .returnResult()
                .getResponseBody();

        assertThat(response).isNotNull();
        assertThat(response).hasSize(2);

        ObjectValidations.validateComponentView(response.get(0), Component.Type.BUSINESS_CARD, "anotherComponentName", secondComponent.getId(),
                secondComponent.getStatus());
        ObjectValidations.validateComponentView(response.get(1), Component.Type.BUSINESS_CARD, "someComponentName", firstComponent.getId(),
                firstComponent.getStatus());

        List<ComponentView> delete = webTestClient.delete()
                .uri("/api/components?id=" + firstComponent.getId().toString())
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(ComponentView.class)
                .returnResult()
                .getResponseBody();

        assertThat(delete).isNotNull();
        assertThat(delete).hasSize(1);

        List<ComponentView> responseAfterDelete = webTestClient.get()
                .uri("/api/components")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(ComponentView.class)
                .returnResult()
                .getResponseBody();

        assertThat(responseAfterDelete).isNotNull();
        assertThat(responseAfterDelete).hasSize(1);

        ObjectValidations.validateComponentView(responseAfterDelete.get(0), Component.Type.BUSINESS_CARD, "anotherComponentName",
                secondComponent.getId(), secondComponent.getStatus());
    }
}
