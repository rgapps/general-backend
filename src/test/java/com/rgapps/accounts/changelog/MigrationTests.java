package com.rgapps.accounts.changelog;

import com.rgapps.accounts.IntegrationTest;
import com.rgapps.accounts.datahelper.ModelData;
import com.rgapps.accounts.model.Account;
import com.rgapps.accounts.model.AccountSummary;
import com.rgapps.accounts.model.CreditAccountMovement;
import com.rgapps.accounts.model.DebitAccountMovement;
import com.rgapps.accounts.model.DepositAccountMovement;
import com.rgapps.accounts.model.WithdrawAccountMovement;
import com.rgapps.accounts.model.values.Currency;
import com.rgapps.auth.model.Role;
import com.rgapps.auth.model.User;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.SellStockTransaction;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Disabled("Embedded mongo not working inside docker")
class MigrationTests extends IntegrationTest {
    @Autowired
    private MongoTemplate mongoTemplate;

    Migration migration = new Migration();

    @Test
    public void createDefaultAccount() {
        migration.createDefaultAccount(() -> mongoTemplate);

        List<Account> accounts = mongoTemplate.find(new Query(), Account.class);
        assertThat(accounts).hasSize(1);
        assertThat(accounts.get(0).getAccountName()).isEqualTo("DEFAULT");
        Assertions.assertThat(accounts.get(0).getCurrency().getCode()).isEqualTo("PHP");
    }

    @Test
    public void migrateExistentStockTransactionsIntoDefaultAccount() {
        mongoTemplate.save(new Account("DEFAULT", new Currency("AED"), UUID.randomUUID()));
        BuyStockTransaction buy1 = mongoTemplate.save(com.rgapps.stocks.datahelper.ModelData.defaultBuyTransaction());
        BuyStockTransaction buy2 = mongoTemplate.save(com.rgapps.stocks.datahelper.ModelData.defaultBuyTransaction());
        SellStockTransaction sell1 = mongoTemplate.save(com.rgapps.stocks.datahelper.ModelData.defaultSellTransaction());
        SellStockTransaction sell2 = mongoTemplate.save(com.rgapps.stocks.datahelper.ModelData.defaultSellTransaction());

        migration.migrateExistentStockTransactionsIntoDefaultAccount(() -> mongoTemplate);

        List<CreditAccountMovement> creditMovements =
                mongoTemplate.find(new Query(new Criteria("type").is("CREDIT")), CreditAccountMovement.class);
        List<DebitAccountMovement> debitMovements =
                mongoTemplate.find(new Query(new Criteria("type").is("DEBIT")), DebitAccountMovement.class);
        assertThat(debitMovements).hasSize(2);
        assertThat(debitMovements.get(0).getExternalId()).isEqualTo(buy1.getId());
        assertThat(debitMovements.get(1).getExternalId()).isEqualTo(buy2.getId());
        assertThat(creditMovements).hasSize(2);
        assertThat(creditMovements.get(0).getExternalId()).isEqualTo(sell1.getId());
        assertThat(creditMovements.get(1).getExternalId()).isEqualTo(sell2.getId());
        List<AccountSummary> accountSummary = mongoTemplate.find(new Query(), AccountSummary.class);
        assertThat(accountSummary).hasSize(1);
        assertThat(accountSummary.get(0).getCurrentAmount().getValue()).isEqualTo(-8);
    }

    @Test
    public void separateDepositAndWithDrawTransactions() {
        mongoTemplate.save(new Account("DEFAULT", new Currency("AED"), UUID.randomUUID()));
        DepositAccountMovement deposit = mongoTemplate.save(ModelData.defaultDepositAccountMovement());
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(deposit.getId())),
                new Update().set("type", "CREDIT").set("creditType", "DEPOSIT"), "account-movement");
        DebitAccountMovement debit = mongoTemplate.save(ModelData.defaultDebitAccountMovement());
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(debit.getId())),
                new Update().set("_class", "com.wilsonr990.page.accounts.model.DefaultDebitAccountMovement"), "account-movement");
        CreditAccountMovement credit = mongoTemplate.save(ModelData.defaultCreditAccountMovement());
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(credit.getId())),
                new Update().set("_class", "com.wilsonr990.page.accounts.model.DefaultCreditAccountMovement"), "account-movement");
        WithdrawAccountMovement withdraw = mongoTemplate.save(ModelData.defaultWithdrawAccountMovement());
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(withdraw.getId())),
                new Update().set("type", "DEBIT").set("debitType", "WITHDRAW"), "account-movement");

        migration.separateDepositAndWithDrawTransactions(() -> mongoTemplate);

        DepositAccountMovement udpatedDeposit =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(deposit.getId())), DepositAccountMovement.class);
        DebitAccountMovement udpatedDebit =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(debit.getId())), DebitAccountMovement.class);
        CreditAccountMovement udpatedCredit =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(credit.getId())), CreditAccountMovement.class);
        WithdrawAccountMovement udpatedWithdraw =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(withdraw.getId())), WithdrawAccountMovement.class);
        assertThat(udpatedDeposit).isEqualTo(deposit);
        assertThat(udpatedDebit).isEqualTo(debit);
        assertThat(udpatedCredit).isEqualTo(credit);
        assertThat(udpatedWithdraw).isEqualTo(withdraw);
    }

    @Test
    public void assignDefaultAccountToNewUser() {
        Account defaultAccount = mongoTemplate.save(new Account("DEFAULT", new Currency("AED"), UUID.randomUUID()));

        migration.assignDefaultAccountToNewUser(() -> mongoTemplate);

        User user = mongoTemplate.findOne(new Query().addCriteria(new Criteria("username").is("Joshel")), User.class);
        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isEqualTo("Joshel");
        assertThat(user.getRoles()).hasSize(1);
        assertThat(user.getRoles().stream().map(Role::getType)).contains(Role.RoleType.ROLE_USER);

        Account updatedAccount = mongoTemplate.findOne(new Query(new Criteria("_id").is(defaultAccount.getId())), Account.class);
        assertThat(updatedAccount).isNotNull();
        assertThat(updatedAccount.getUserId()).isEqualTo(user.getId());
    }

    @Test
    public void changeModuleNames() {
        Account account = mongoTemplate.save(new Account("DEFAULT", new Currency("AED"), UUID.randomUUID()));
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(account.getId())),
                new Update().set("_class", "com.wilsonr990.page.accounts.model.Account"), "account");
        DepositAccountMovement deposit = mongoTemplate.save(ModelData.defaultDepositAccountMovement());
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(deposit.getId())),
                new Update().set("_class", "com.wilsonr990.page.accounts.model.DepositAccountMovement"), "account-movement");
        DebitAccountMovement debit = mongoTemplate.save(ModelData.defaultDebitAccountMovement());
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(debit.getId())),
                new Update().set("_class", "com.wilsonr990.page.accounts.model.DebitAccountMovement"), "account-movement");
        CreditAccountMovement credit = mongoTemplate.save(ModelData.defaultCreditAccountMovement());
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(credit.getId())),
                new Update().set("_class", "com.wilsonr990.page.accounts.model.CreditAccountMovement"), "account-movement");
        WithdrawAccountMovement withdraw = mongoTemplate.save(ModelData.defaultWithdrawAccountMovement());
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(withdraw.getId())),
                new Update().set("_class", "com.wilsonr990.page.accounts.model.WithdrawAccountMovement"), "account-movement");
        AccountSummary summary = mongoTemplate.save(AccountSummary.empty(account));
        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("_id").is(summary.getAccountId())),
                new Update().set("_class", "com.wilsonr990.page.accounts.model.AccountSummary"), "account-summary");

        migration.changeModuleNamesAccounts(() -> mongoTemplate);

        Account updatedAccount =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(account.getId())), Account.class);
        DepositAccountMovement udpatedDeposit =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(deposit.getId())), DepositAccountMovement.class);
        DebitAccountMovement udpatedDebit =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(debit.getId())), DebitAccountMovement.class);
        CreditAccountMovement udpatedCredit =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(credit.getId())), CreditAccountMovement.class);
        WithdrawAccountMovement udpatedWithdraw =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(withdraw.getId())), WithdrawAccountMovement.class);
        AccountSummary updatedSummary =
                mongoTemplate.findOne(new Query(new Criteria("_id").is(summary.getAccountId())), AccountSummary.class);
        assertThat(updatedAccount).isEqualTo(account);
        assertThat(udpatedDeposit).isEqualTo(deposit);
        assertThat(udpatedDebit).isEqualTo(debit);
        assertThat(udpatedCredit).isEqualTo(credit);
        assertThat(udpatedWithdraw).isEqualTo(withdraw);
        assertThat(updatedSummary).isEqualTo(summary);
    }
}
