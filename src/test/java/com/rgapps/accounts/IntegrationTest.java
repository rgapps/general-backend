package com.rgapps.accounts;

import com.rgapps.accounts.datahelper.ModelData;
import com.rgapps.accounts.model.Account;
import com.rgapps.accounts.model.AccountMovement;
import com.rgapps.accounts.model.AccountSummary;
import com.rgapps.accounts.model.CreditAccountMovement;
import com.rgapps.accounts.model.DepositAccountMovement;
import com.rgapps.accounts.model.WithdrawAccountMovement;
import com.rgapps.commons.AuthenticatedUsers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import javax.annotation.PostConstruct;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class IntegrationTest {
    @Autowired
    protected ReactiveMongoTemplate mongoTemplate;

    @Autowired
    protected AuthenticatedUsers authenticatedUsers;

    @Autowired
    protected WebTestClient authenticatedWebTestClient;

    @Autowired
    protected WebTestClient webTestClient;

    @PostConstruct
    public void init() {
        authenticatedWebTestClient = authenticatedWebTestClient
                .mutate()
                .defaultHeader(HttpHeaders.AUTHORIZATION,
                        String.format("Bearer %s", authenticatedUsers.userNormalToken()))
                .build();
    }

    @BeforeEach
    public void clean() {
        mongoTemplate.findAllAndRemove(new Query(), Account.class).collectList().block();
        mongoTemplate.findAllAndRemove(new Query(), DepositAccountMovement.class).collectList().block();
        mongoTemplate.findAllAndRemove(new Query(), CreditAccountMovement.class).collectList().block();
        mongoTemplate.findAllAndRemove(new Query(), WithdrawAccountMovement.class).collectList().block();
        mongoTemplate.findAllAndRemove(new Query(), AccountMovement.class).collectList().block();
        mongoTemplate.findAllAndRemove(new Query(), AccountSummary.class).collectList().block();
    }

    protected WebTestClient.ResponseSpec createAccount(String body) {
        return authenticatedWebTestClient.put()
                .uri("/api/accounts")
                .contentType(APPLICATION_JSON)
                .bodyValue(body)
                .exchange()
                .expectStatus().isOk();
    }

    protected WebTestClient.ResponseSpec deposit(String body) {
        return authenticatedWebTestClient.post()
                .uri("/api/accounts/" + ModelData.defaultAccount.getId() + "/movements/deposit")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(body)
                .exchange()
                .expectStatus().isOk();
    }

    protected WebTestClient.ResponseSpec credit(String body) {
        return authenticatedWebTestClient.post()
                .uri("/api/accounts/" + ModelData.defaultAccount.getId() + "/movements/credit")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(body)
                .exchange()
                .expectStatus().isOk();
    }

    protected WebTestClient.ResponseSpec withdraw(String body) {
        return authenticatedWebTestClient.post()
                .uri("/api/accounts/" + ModelData.defaultAccount.getId() + "/movements/withdraw")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(body)
                .exchange()
                .expectStatus().isOk();
    }

    protected WebTestClient.ResponseSpec debit(String body) {
        return authenticatedWebTestClient.post()
                .uri("/api/accounts/" + ModelData.defaultAccount.getId() + "/movements/debit")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(body)
                .exchange()
                .expectStatus().isOk();
    }
}
