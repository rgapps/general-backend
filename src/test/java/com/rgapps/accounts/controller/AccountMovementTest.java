package com.rgapps.accounts.controller;

import com.rgapps.accounts.IntegrationTest;
import com.rgapps.accounts.controller.resource.AccountMovementView;
import com.rgapps.accounts.controller.resource.CreditMovementResource;
import com.rgapps.accounts.controller.resource.DebitMovementResource;
import com.rgapps.accounts.controller.resource.DepositMovementResource;
import com.rgapps.accounts.controller.resource.WithdrawMovementResource;
import com.rgapps.accounts.datahelper.DataHelper;
import com.rgapps.accounts.datahelper.ModelData;
import com.rgapps.accounts.model.Account;
import com.rgapps.accounts.model.AccountMovement;
import com.rgapps.accounts.model.CreditAccountMovement;
import com.rgapps.accounts.model.DebitAccountMovement;
import com.rgapps.accounts.model.DepositAccountMovement;
import com.rgapps.accounts.model.WithdrawAccountMovement;
import com.rgapps.auth.datahelper.ObjectValidations;
import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.apiexceptions.ValidationErrorResponse;
import com.rgapps.common.exception.modelexceptions.ModelErrorResponse;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import static com.rgapps.accounts.datahelper.ObjectValidations.validateCreditMovementResource;
import static com.rgapps.accounts.datahelper.ObjectValidations.validateDebitMovementResource;
import static com.rgapps.commons.AuthenticatedUsers.normalUserId;
import static com.rgapps.commons.AuthenticatedUsers.normalUserId2;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Disabled("Embedded mongo not working inside docker")
class AccountMovementTest extends IntegrationTest {
    @ParameterizedTest
    @MethodSource("com.rgapps.accounts.datahelper.DataHelper#depositFailBodyCases")
    public void depositFailWithValidationErrors(String resource, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/deposit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void depositCorrectly() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();

        LocalDateTime start = LocalDateTime.now();
        DepositAccountMovement response = deposit(DataHelper.DEFAULT_DEPOSIT)
                .expectBody(DepositAccountMovement.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovement(response,
                account.getId(),
                AccountMovement.Status.SUCCESS,
                "AED",
                10.0,
                "2020-12-07",
                start,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(1);
        assertThat(response).isNotNull();
        AccountMovement registry = mongoTemplate.findById(response.getId(), AccountMovement.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void depositFailsIfAccountIsNotFound() {
        ModelErrorResponse<DepositMovementResource> response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/deposit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_DEPOSIT)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseDepositMovementResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.NOT_FOUND,
                "NotFoundException",
                String.format("Not found 'Account' ({accountName=DEFAULT, userId=%s})", normalUserId));
        assertThat(response).isNotNull();
        DepositMovementResource erroredObject = response.getErroredObject();
        com.rgapps.accounts.datahelper.ObjectValidations.validateDepositMovementResource(erroredObject, "AED", 10, "2020-12-07");

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void depositFailsIfNotAuthenticated() {
        mongoTemplate.save(ModelData.defaultAccount).block();

        webTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/deposit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_DEPOSIT)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @Test
    public void depositFailsIfAccountIsNotOwnedByCurrentUser() {
        mongoTemplate.save(ModelData.accountForOtherUser).block();

        ApiErrorResponse response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/deposit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_DEPOSIT)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.NOT_FOUND,
                "NotFoundException",
                String.format("Not found 'Account' ({accountName=DEFAULT, userId=%s})", normalUserId));

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void depositFailsIfCurrencyDoesntMatchWithAccountCurrency() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();

        ModelErrorResponse<DepositMovementResource> response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/deposit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEPOSIT_USD)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseDepositMovementResource.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Currency' (expected: AED, actual: USD)");
        assertThat(response).isNotNull();
        DepositMovementResource erroredObject = response.getErroredObject();
        com.rgapps.accounts.datahelper.ObjectValidations.validateDepositMovementResource(erroredObject, "USD", 10, "2020-12-07");

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @ParameterizedTest
    @MethodSource("com.rgapps.accounts.datahelper.DataHelper#withdrawFailBodyCases")
    public void withdrawFailWithValidationErrors(String resource, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/withdraw", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void withdrawCorrectly() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();

        deposit(DataHelper.DEFAULT_DEPOSIT);
        LocalDateTime start = LocalDateTime.now();
        WithdrawAccountMovement response = withdraw(DataHelper.DEFAULT_WITHDRAW)
                .expectBody(WithdrawAccountMovement.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovement(response,
                account.getId(),
                AccountMovement.Status.SUCCESS,
                "AED",
                10.0,
                "2020-12-07",
                start,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(2);
        assertThat(response).isNotNull();
        AccountMovement registry = mongoTemplate.findById(response.getId(), AccountMovement.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void withdrawFailsIfAccountIsNotFound() {
        ModelErrorResponse<WithdrawMovementResource> response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/withdraw", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_WITHDRAW)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseWithdrawMovementResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.NOT_FOUND,
                "NotFoundException",
                String.format("Not found 'Account' ({accountName=DEFAULT, userId=%s})", normalUserId));
        assertThat(response).isNotNull();
        WithdrawMovementResource erroredObject = response.getErroredObject();
        com.rgapps.accounts.datahelper.ObjectValidations.validateWithdrawMovementResource(erroredObject, "AED", 10, "2020-12-07");

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void withdrawFailsIfNotAuthenticated() {
        createAccount(DataHelper.DEFAULT_ACCOUNT);
        mongoTemplate.save(ModelData.defaultAccount).block();

        webTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/withdraw", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_WITHDRAW)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @Test
    public void withdrawFailsIfAccountIsNotOwnedByCurrentUser() {
        mongoTemplate.save(ModelData.accountForOtherUser).block();

        ApiErrorResponse response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/withdraw", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_WITHDRAW)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.NOT_FOUND,
                "NotFoundException",
                String.format("Not found 'Account' ({accountName=DEFAULT, userId=%s})", normalUserId));

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void withdrawFailsIfCurrencyDoesntMatchWithAccounts() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();

        ModelErrorResponse<WithdrawMovementResource> response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/withdraw", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.WITHDRAW_USD)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseWithdrawMovementResource.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Currency' (expected: AED, actual: USD)");
        assertThat(response).isNotNull();
        WithdrawMovementResource erroredObject = response.getErroredObject();
        com.rgapps.accounts.datahelper.ObjectValidations.validateWithdrawMovementResource(erroredObject, "USD", 10, "2020-12-07");

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void withdrawFailsIfNotEnoughMoneyToWithdraw() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();

        deposit(DataHelper.DEPOSIT_5);
        ModelErrorResponse<WithdrawMovementResource> response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/withdraw", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_WITHDRAW)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseWithdrawMovementResource.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Money to Withdraw' (expected: <= 5.000000, actual: 10.0)");
        assertThat(response).isNotNull();
        WithdrawMovementResource erroredObject = response.getErroredObject();
        com.rgapps.accounts.datahelper.ObjectValidations.validateWithdrawMovementResource(erroredObject, "AED", 10, "2020-12-07");

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(1);
    }

    @ParameterizedTest
    @MethodSource("com.rgapps.accounts.datahelper.DataHelper#creditFailBodyCases")
    public void creditFailWithValidationErrors(String resource, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/credit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void creditCorrectly() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();

        LocalDateTime start = LocalDateTime.now();
        CreditAccountMovement response = credit(DataHelper.DEFAULT_CREDIT)
                .expectBody(CreditAccountMovement.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovement(response,
                account.getId(),
                CreditAccountMovement.CreditType.STOCKS_REVENUE,
                "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
                AccountMovement.Status.SUCCESS,
                "AED",
                10.0,
                "2020-12-07",
                start,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(1);
        assertThat(response).isNotNull();
        AccountMovement registry = mongoTemplate.findById(response.getId(), AccountMovement.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void creditFailsIfAccountIsNotFound() {
        ModelErrorResponse<CreditMovementResource> response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/credit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_CREDIT)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseCreditMovementResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.NOT_FOUND,
                "NotFoundException",
                String.format("Not found 'Account' ({accountName=DEFAULT, userId=%s})", normalUserId));
        assertThat(response).isNotNull();
        CreditMovementResource erroredObject = response.getErroredObject();
        validateCreditMovementResource(erroredObject, "STOCKS_REVENUE", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa", "AED", 10, "2020-12-07");

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void creditFailsIfNotAuthenticated() {
        createAccount(DataHelper.DEFAULT_ACCOUNT);
        mongoTemplate.save(ModelData.defaultAccount).block();

        webTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/credit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_CREDIT)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @Test
    public void creditFailsIfAccountIsNotOwnedByCurrentUser() {
        mongoTemplate.save(ModelData.accountForOtherUser).block();

        ApiErrorResponse response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/credit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_CREDIT)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.NOT_FOUND,
                "NotFoundException",
                String.format("Not found 'Account' ({accountName=DEFAULT, userId=%s})", normalUserId));

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void creditFailsIfCurrencyDoesntMatchWithAccountCurrency() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();

        ModelErrorResponse<CreditMovementResource> response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/credit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.CREDIT_USD)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseCreditMovementResource.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Currency' (expected: AED, actual: USD)");
        assertThat(response).isNotNull();
        CreditMovementResource erroredObject = response.getErroredObject();
        validateCreditMovementResource(erroredObject, "STOCKS_REVENUE", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa", "USD", 10, "2020-12-07");

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @ParameterizedTest
    @MethodSource("com.rgapps.accounts.datahelper.DataHelper#debitFailBodyCases")
    public void debitFailWithValidationErrors(String resource, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/debit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void debitCorrectly() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();

        deposit(DataHelper.DEFAULT_DEPOSIT);
        LocalDateTime start = LocalDateTime.now();
        DebitAccountMovement response = debit(DataHelper.DEFAULT_DEBIT)
                .expectBody(DebitAccountMovement.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovement(response,
                account.getId(),
                DebitAccountMovement.DebitType.STOCKS_INVESTMENT,
                "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
                AccountMovement.Status.SUCCESS,
                "AED",
                10.0,
                "2020-12-07",
                start,
                LocalDateTime.now());

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(2);
        assertThat(response).isNotNull();
        AccountMovement registry = mongoTemplate.findById(response.getId(), AccountMovement.class).block();
        assertThat(registry).isEqualTo(response);
    }

    @Test
    public void debitFailsIfAccountIsNotFound() {
        ModelErrorResponse<DebitMovementResource> response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/debit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_DEBIT)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseDebitMovementResource.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.NOT_FOUND,
                "NotFoundException",
                String.format("Not found 'Account' ({accountName=DEFAULT, userId=%s})", normalUserId));
        assertThat(response).isNotNull();
        DebitMovementResource erroredObject = response.getErroredObject();
        validateDebitMovementResource(erroredObject, "STOCKS_INVESTMENT", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa", "AED", 10, "2020-12-07");

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void debitFailsIfNotAuthenticated() {
        createAccount(DataHelper.DEFAULT_ACCOUNT);
        mongoTemplate.save(ModelData.defaultAccount).block();

        webTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/debit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_DEBIT)
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @Test
    public void debitFailsIfAccountIsNotOwnedByCurrentUser() {
        mongoTemplate.save(ModelData.accountForOtherUser).block();

        ApiErrorResponse response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/debit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_DEBIT)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.NOT_FOUND,
                "NotFoundException",
                String.format("Not found 'Account' ({accountName=DEFAULT, userId=%s})", normalUserId));


        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void debitFailsIfCurrencyDoesntMatchWithAccounts() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();

        ModelErrorResponse<DebitMovementResource> response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/debit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEBIT_USD)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseDebitMovementResource.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Currency' (expected: AED, actual: USD)");
        assertThat(response).isNotNull();
        DebitMovementResource erroredObject = response.getErroredObject();
        validateDebitMovementResource(erroredObject, "STOCKS_INVESTMENT", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa", "USD", 10, "2020-12-07");

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void debitFailsIfNotEnoughMoneyToExpend() {
        Account account = mongoTemplate.save(ModelData.defaultAccount).block();

        deposit(DataHelper.DEPOSIT_5);
        ModelErrorResponse<DebitMovementResource> response = authenticatedWebTestClient.post()
                .uri(String.format("/api/accounts/%s/movements/debit", ModelData.defaultAccount.getId()))
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_DEBIT)
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(DataHelper.ModelErrorResponseDebitMovementResource.class)
                .returnResult()
                .getResponseBody();

        assertThat(account).isNotNull();
        ObjectValidations.validateError(response, CONFLICT,
                "UnexpectedValueException",
                "Unexpected value for 'Money to Debit' (expected: <= 5.000000, actual: 10.0)");
        assertThat(response).isNotNull();
        DebitMovementResource erroredObject = response.getErroredObject();
        validateDebitMovementResource(erroredObject, "STOCKS_INVESTMENT", "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa", "AED", 10, "2020-12-07");

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(1);
    }

    @ParameterizedTest
    @MethodSource("com.rgapps.accounts.datahelper.DataHelper#getMovementsReturnsNotFoundForInvalidPathParametersCases")
    public void getMovementsReturnsNotFoundForInvalidPathParameters(String requestParams, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = authenticatedWebTestClient.get()
                .uri(String.format(String.format("/api/accounts/%s/movements?%%s", ModelData.defaultAccount.getId()), requestParams))
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);
    }

    @Test
    public void getMovementsReturnsEmptyListIfNoData() {
        mongoTemplate.save(ModelData.defaultAccount).block();

        List<AccountMovementView> summaryList = authenticatedWebTestClient.get()
                .uri(String.format("/api/accounts/%s/movements", ModelData.defaultAccount.getId()))
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(AccountMovementView.class)
                .returnResult()
                .getResponseBody();

        assertThat(summaryList).isNotNull();
        assertThat(summaryList).hasSize(0);
    }

    @Test
    public void getMovementsReturnsCorrectly() {
        mongoTemplate.save(ModelData.defaultAccount).block();
        deposit(DataHelper.DEFAULT_DEPOSIT);
        debit(DataHelper.DEBIT_DEC_8);
        credit(DataHelper.CREDIT_DEC_9);
        withdraw(DataHelper.WITHDRAW_DEC_10);
        List<AccountMovementView> movementsList = authenticatedWebTestClient.get()
                .uri(String.format("/api/accounts/%s/movements", ModelData.defaultAccount.getId()))
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(AccountMovementView.class)
                .returnResult()
                .getResponseBody();

        assertThat(movementsList).isNotNull();
        assertThat(movementsList).hasSize(4);
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovementView(movementsList.get(0),
                AccountMovement.Type.WITHDRAW,
                "AED",
                10.00,
                "2020-12-10");
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovementView(movementsList.get(1),
                AccountMovement.Type.CREDIT,
                "AED",
                10.00,
                "2020-12-09");
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovementView(movementsList.get(2),
                AccountMovement.Type.DEBIT,
                "AED",
                10.00,
                "2020-12-08");
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovementView(movementsList.get(3),
                AccountMovement.Type.DEPOSIT,
                "AED",
                10.00,
                "2020-12-07");
    }

    @Test
    public void getMovementsReturnsCorrectlyWhileFilteringByTypes() {
        mongoTemplate.save(ModelData.defaultAccount).block();
        deposit(DataHelper.DEFAULT_DEPOSIT);
        debit(DataHelper.DEBIT_DEC_8);
        credit(DataHelper.CREDIT_DEC_9);
        withdraw(DataHelper.WITHDRAW_DEC_10);
        List<AccountMovementView> movementsList = authenticatedWebTestClient.get()
                .uri(String.format("/api/accounts/%s/movements?types=DEPOSIT", ModelData.defaultAccount.getId()))
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(AccountMovementView.class)
                .returnResult()
                .getResponseBody();
        assertThat(movementsList).isNotNull();
        assertThat(movementsList).hasSize(1);
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovementView(movementsList.get(0),
                AccountMovement.Type.DEPOSIT,
                "AED",
                10,
                "2020-12-07");

        movementsList = authenticatedWebTestClient.get()
                .uri(String.format("/api/accounts/%s/movements?types=CREDIT,DEBIT", ModelData.defaultAccount.getId()))
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(AccountMovementView.class)
                .returnResult()
                .getResponseBody();
        assertThat(movementsList).isNotNull();
        assertThat(movementsList).hasSize(2);
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovementView(movementsList.get(0),
                AccountMovement.Type.CREDIT,
                "AED",
                10.00,
                "2020-12-09");
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovementView(movementsList.get(1),
                AccountMovement.Type.DEBIT,
                "AED",
                10.00,
                "2020-12-08");

        movementsList = authenticatedWebTestClient.get()
                .uri(String.format("/api/accounts/%s/movements?types=DEPOSIT,WITHDRAW", ModelData.defaultAccount.getId()))
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(AccountMovementView.class)
                .returnResult()
                .getResponseBody();
        assertThat(movementsList).isNotNull();
        assertThat(movementsList).hasSize(2);
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovementView(movementsList.get(0),
                AccountMovement.Type.WITHDRAW,
                "AED",
                10.00,
                "2020-12-10");
        com.rgapps.accounts.datahelper.ObjectValidations.validateAccountMovementView(movementsList.get(1),
                AccountMovement.Type.DEPOSIT,
                "AED",
                10.00,
                "2020-12-07");
    }

    @Test
    public void getMovementsFailsIfNotAuthenticated() {
        mongoTemplate.save(ModelData.defaultAccount).block();
        deposit(DataHelper.DEFAULT_DEPOSIT);
        debit(DataHelper.DEBIT_DEC_8);
        credit(DataHelper.CREDIT_DEC_9);
        withdraw(DataHelper.WITHDRAW_DEC_10);

        webTestClient.get()
                .uri(String.format("/api/accounts/%s/movements", ModelData.defaultAccount.getId()))
                .exchange()
                .expectStatus().isEqualTo(HttpStatus.UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @Test
    public void getMovementsFailsIfAccountIsNotOwnedByCurrentUser() {
        mongoTemplate.save(ModelData.defaultAccount).block();
        deposit(DataHelper.DEFAULT_DEPOSIT);
        debit(DataHelper.DEBIT_DEC_8);
        credit(DataHelper.CREDIT_DEC_9);
        withdraw(DataHelper.WITHDRAW_DEC_10);

        ApiErrorResponse response = webTestClient.get()
                .uri(String.format("/api/accounts/%s/movements", ModelData.defaultAccount.getId()))
                .header(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", authenticatedUsers.userNormalToken2()))
                .exchange()
                .expectStatus().isEqualTo(CONFLICT)
                .expectBody(ApiErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.NOT_FOUND,
                "NotFoundException",
                String.format("Not found 'Account' ({accountName=DEFAULT, userId=%s})", normalUserId2));
    }
}
