package com.rgapps.accounts.controller;

import com.rgapps.accounts.IntegrationTest;
import com.rgapps.accounts.controller.resource.AccountView;
import com.rgapps.accounts.datahelper.DataHelper;
import com.rgapps.accounts.datahelper.ObjectValidations;
import com.rgapps.accounts.model.Account;
import com.rgapps.accounts.model.AccountMovement;
import com.rgapps.common.exception.apiexceptions.ValidationErrorResponse;
import com.rgapps.stocks.controller.resource.SummaryForMonthView;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Disabled("Embedded mongo not working inside docker")
class AccountTest extends IntegrationTest {
    @ParameterizedTest
    @MethodSource("com.rgapps.accounts.datahelper.DataHelper#createAccountFailBodyCases")
    public void createAccountFailWithValidationErrors(String resource, Map<String, List<String>> expectedErrors) {
        ValidationErrorResponse response = authenticatedWebTestClient.put()
                .uri("/api/accounts")
                .contentType(APPLICATION_JSON)
                .bodyValue(resource)
                .exchange()
                .expectStatus().isBadRequest()
                .expectBody(ValidationErrorResponse.class)
                .returnResult()
                .getResponseBody();

        ObjectValidations.validateError(response, HttpStatus.BAD_REQUEST, "ValidationException", "Validation Error", expectedErrors);

        assertThat(mongoTemplate.findAll(AccountMovement.class).collectList().block()).hasSize(0);
    }

    @Test
    public void createAccountCorrectly() {
        createAccount(DataHelper.DEFAULT_ACCOUNT)
                .expectBody().isEmpty();

        assertThat(mongoTemplate.findAll(Account.class).collectList().block()).hasSize(1);
    }

    @Test
    public void createAccountFailsIfNoUserAuthenticated() {
        webTestClient.put()
                .uri("/api/accounts")
                .contentType(APPLICATION_JSON)
                .bodyValue(DataHelper.DEFAULT_ACCOUNT)
                .exchange()
                .expectStatus().isEqualTo(UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @Test
    public void getAccountsReturnsEmptyListIfNoData() {
        List<SummaryForMonthView> summaryList = authenticatedWebTestClient.get()
                .uri("/api/accounts")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(SummaryForMonthView.class)
                .returnResult()
                .getResponseBody();

        assertThat(summaryList).isNotNull();
        assertThat(summaryList).hasSize(0);
    }

    @Test
    public void getAccountsReturnsCorrectly() {
        createAccount(DataHelper.DEFAULT_ACCOUNT);
        createAccount(DataHelper.ACCOUNT_USD);
        List<AccountView> accountList = authenticatedWebTestClient.get()
                .uri("/api/accounts")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(AccountView.class)
                .returnResult()
                .getResponseBody();

        assertThat(accountList).isNotNull();
        assertThat(accountList).hasSize(2);
        ObjectValidations.validateAccountView(accountList.get(0),
                accountList.get(0).getId(),
                "DEFAULT",
                "AED");
        ObjectValidations.validateAccountView(accountList.get(1),
                accountList.get(1).getId(),
                "DEFAULT_USD",
                "USD");
    }

    @Test
    public void getAccountsFailsIfUserNotAuthenticated() {
        createAccount(DataHelper.DEFAULT_ACCOUNT);
        createAccount(DataHelper.ACCOUNT_USD);
        webTestClient.get()
                .uri("/api/accounts")
                .exchange()
                .expectStatus().isEqualTo(UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @Test
    public void getAccountByIdReturnsCorrectly() {
        createAccount(DataHelper.DEFAULT_ACCOUNT);
        Account account = mongoTemplate.findOne(new Query(), Account.class).block();

        assertThat(account).isNotNull();
        AccountView accountRetrieved = authenticatedWebTestClient.get()
                .uri("/api/accounts/" + account.getId())
                .exchange()
                .expectStatus().isOk()
                .expectBody(AccountView.class)
                .returnResult()
                .getResponseBody();

        assertThat(accountRetrieved).isNotNull();
        ObjectValidations.validateAccountView(accountRetrieved,
                account.getId(),
                "DEFAULT",
                "AED");
    }

    @Test
    public void getAccountByIdFailsIfUserNotAuthenticated() {
        createAccount(DataHelper.DEFAULT_ACCOUNT);
        Account account = mongoTemplate.findOne(new Query(), Account.class).block();

        assertThat(account).isNotNull();
        webTestClient.get()
                .uri("/api/accounts/" + account.getId())
                .exchange()
                .expectStatus().isEqualTo(UNAUTHORIZED)
                .expectBody().isEmpty();
    }

    @Test
    public void getDefaultAccountReturnsCorrectly() {
        createAccount(DataHelper.DEFAULT_ACCOUNT);
        Account account = mongoTemplate.findOne(new Query(), Account.class).block();

        assertThat(account).isNotNull();
        AccountView accountRetrieved = authenticatedWebTestClient.get()
                .uri("/api/accounts/default")
                .exchange()
                .expectStatus().isOk()
                .expectBody(AccountView.class)
                .returnResult()
                .getResponseBody();

        assertThat(accountRetrieved).isNotNull();
        ObjectValidations.validateAccountView(accountRetrieved,
                account.getId(),
                "DEFAULT",
                "AED");
    }

    @Test
    public void getDefaultAccountFailsIfUserNotAuthenticated() {
        createAccount(DataHelper.DEFAULT_ACCOUNT);
        Account account = mongoTemplate.findOne(new Query(), Account.class).block();

        assertThat(account).isNotNull();
        webTestClient.get()
                .uri("/api/accounts/default")
                .exchange()
                .expectStatus().isEqualTo(UNAUTHORIZED)
                .expectBody().isEmpty();
    }
}
