package com.rgapps;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Disabled("Embedded mongo not working inside docker")
class ApplicationTests {
    @Test
    public void applicationStarts() {
        Application.main(new String[]{});
    }
}
