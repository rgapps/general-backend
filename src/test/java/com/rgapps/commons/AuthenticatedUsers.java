package com.rgapps.commons;

import com.rgapps.common.security.JwtUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class AuthenticatedUsers {
    private final JwtUtils jwtUtils;

    public static UUID adminUserId = UUID.randomUUID();
    public static UUID normalUserId = UUID.randomUUID();
    public static UUID normalUserId2 = UUID.randomUUID();
    public static UUID moderatorUserId = UUID.randomUUID();

    public String userAdminToken() {
        return jwtToken(adminUserId, "admin", List.of("ROLE_ADMIN"));
    }

    public String userNormalToken() {
        return jwtToken(normalUserId, "user", List.of("ROLE_USER"));
    }

    public String userNormalToken2() {
        return jwtToken(normalUserId2, "user", List.of("ROLE_USER"));
    }

    public String userModeratorToken() {
        return jwtToken(moderatorUserId, "moderator", List.of("ROLE_MODERATOR"));
    }

    public String jwtToken(UUID userId, String username, List<String> roles) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("role", roles);
        claims.put("id", userId);
        return jwtUtils.generateToken(claims, username);
    }
}
