package com.rgapps.data.changelog;

import com.rgapps.commons.AuthenticatedUsers;
import com.rgapps.data.model.RawData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;

import javax.annotation.PostConstruct;

@ExtendWith(SpringExtension.class)
@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class IntegrationTest {
    @Autowired
    protected ReactiveMongoTemplate mongoTemplate;

    @Autowired
    protected AuthenticatedUsers authenticatedUsers;

    @Autowired
    protected WebTestClient authenticatedWebTestClient;

    @Autowired
    protected WebTestClient webTestClient;

    @PostConstruct
    public void init() {
        authenticatedWebTestClient = authenticatedWebTestClient
                .mutate()
                .defaultHeader(HttpHeaders.AUTHORIZATION,
                        String.format("Bearer %s", authenticatedUsers.userNormalToken()))
                .build();
    }

    @BeforeEach
    public void clean() {
        mongoTemplate.findAllAndRemove(new Query(), RawData.class).collectList().block();
    }
}
