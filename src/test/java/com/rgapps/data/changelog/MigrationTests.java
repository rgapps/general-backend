package com.rgapps.data.changelog;

import com.rgapps.data.model.RawData;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

@Disabled("Embedded mongo not working inside docker")
class MigrationTests extends IntegrationTest {
    @Autowired
    private MongoTemplate mongoTemplate;

    Migration migration = new Migration();

    @Test
    public void migrateExistentStockTransactionsIntoDefaultAccount() {
        RawData rawData1 = new RawData("test", Map.of("field", "value", "tagText", "should be kept"));
        rawData1.setCreatedDate(null);
        RawData rawData2 = new RawData("test", Map.of("field", "dasdasds"));
        RawData rawData3 = new RawData("products", Map.of("field1", "value2", "tagText", "valueToRemove"));
        rawData3.setCreatedDate(null);
        RawData raw1 = mongoTemplate.save(rawData1);
        RawData raw2 = mongoTemplate.save(rawData2);
        RawData raw3 = mongoTemplate.save(rawData3);

        migration.fixCreatedDateAndProductsTag(() -> mongoTemplate);

        List<RawData> saved =
                mongoTemplate.find(new Query(), RawData.class);

        assertThat(saved).hasSize(3);
        assertThat(saved.get(0).getCreatedDate()).isCloseTo(raw1.getUpdatedDate(), within(1, ChronoUnit.SECONDS));
        assertThat(saved.get(0).getUpdatedDate()).isCloseTo(raw1.getUpdatedDate(), within(1, ChronoUnit.SECONDS));
        assertThat(saved.get(0).getProps()).containsEntry("tagText", "should be kept");
        assertThat(saved.get(0).getProps()).doesNotContainKey("tagId");
        assertThat(saved.get(0).getProps()).doesNotContainKey("currency");

        assertThat(saved.get(1).getCreatedDate()).isCloseTo(raw2.getCreatedDate(), within(1, ChronoUnit.SECONDS));
        assertThat(saved.get(1).getUpdatedDate()).isCloseTo(raw2.getUpdatedDate(), within(1, ChronoUnit.SECONDS));
        assertThat(saved.get(1).getProps()).doesNotContainKey("tagText");
        assertThat(saved.get(1).getProps()).doesNotContainKey("tagId");
        assertThat(saved.get(1).getProps()).doesNotContainKey("currency");

        assertThat(saved.get(2).getCreatedDate()).isCloseTo(raw3.getUpdatedDate(), within(1, ChronoUnit.SECONDS));
        assertThat(saved.get(2).getUpdatedDate()).isCloseTo(raw3.getUpdatedDate(), within(1, ChronoUnit.SECONDS));
        assertThat(saved.get(2).getProps()).doesNotContainKey("tagText");
        assertThat(saved.get(2).getProps()).containsEntry("tagId", "");
        assertThat(saved.get(2).getProps()).containsEntry("currency", "AED");
    }
}
