package com.rgapps.components.service;

import com.rgapps.common.exception.apiexceptions.NotFoundException;
import com.rgapps.components.controller.resource.ComponentResource;
import com.rgapps.components.model.Component;
import com.rgapps.components.repository.ComponentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

import static com.rgapps.components.model.Component.Status.DELETED;
import static com.rgapps.components.model.Component.Status.SUCCESS;

@Service
@RequiredArgsConstructor
public class ComponentService {
    private final ComponentRepository componentRepository;

    public Mono<Component> create(ComponentResource resource) {
        return componentRepository.save(resource.toComponent()).flatMap(this::onCreated);
    }

    private Mono<Component> onCreated(Component component) {
        return Mono.just(component);
    }

    public Mono<Component> delete(UUID id) {
        return componentRepository.findByIdAndStatus(id, SUCCESS)
                .switchIfEmpty(Mono.error(new NotFoundException("Success Component", "ComponentId", id.toString())))
                .flatMap(it -> componentRepository.save(it.updateStatus(DELETED)))
                .flatMap(this::onDeleted);
    }

    private Mono<Component> onDeleted(Component component) {
        return Mono.just(component);
    }

    public Flux<Component> getComponents() {
        return componentRepository.findAllByStatusOrderByCreatedDateDesc(SUCCESS);
    }
}
