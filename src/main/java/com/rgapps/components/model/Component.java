package com.rgapps.components.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Document(collection = "component")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = BusinessCardComponent.class, name = "BUSINESS_CARD") })
@NoArgsConstructor
public abstract class Component {
  public enum Type {
    BUSINESS_CARD
  }

  public enum Status {
    SUCCESS, DELETED
  }

  @Id
  protected UUID id;
  @EqualsAndHashCode.Exclude
  protected LocalDateTime createdDate;
  @EqualsAndHashCode.Exclude
  protected LocalDateTime updatedDate;

  protected Type type;
  protected String componentName;
  protected Status status;

  public Component(Type type, String componentName, Status status) {
    this.type = type;
    this.status = status;
    this.componentName = componentName;

    id = UUID.randomUUID();
    createdDate = LocalDateTime.now();
    updatedDate = createdDate;
  }

  public Component updateStatus(Status status) {
    this.setStatus(status);

    return this;
  }
}
