package com.rgapps.components.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class BusinessCardComponent extends Component {
  public String urlImageLogo;
  public String name;
  public String profession;

  public BusinessCardComponent(String urlImageLogo, String name, String profession, String componentName) {
    super(Type.BUSINESS_CARD, componentName, Status.SUCCESS);

    this.urlImageLogo = urlImageLogo;
    this.name = name;
    this.profession = profession;
  }
}
