package com.rgapps.components.repository;

import com.rgapps.components.model.Component;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface ComponentRepository extends ReactiveMongoRepository<Component, UUID> {
  Flux<Component> findAllByStatusOrderByCreatedDateDesc(Component.Status success);

  Mono<Component> findByIdAndStatus(UUID id, Component.Status success);
}
