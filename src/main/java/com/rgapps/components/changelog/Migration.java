package com.rgapps.components.changelog;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.rgapps.common.config.SimpleMongoTemplateWrapper;
import com.rgapps.components.model.Component;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;


@ChangeLog(order = "003")
@Slf4j
public class Migration {
  @ChangeSet(order = "001", id = "Add Status and Id to components", author = "Juan")
  public void addStatusAndIdToComponents(SimpleMongoTemplateWrapper templateSimpleWrapper) {
    log.info("Adding Id and Status to the components");
    MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();

    mongoTemplate.updateMulti(new Query(),
      new Update().set("status", Component.Status.SUCCESS), "component");
  }

  @ChangeSet(order = "002", id = "change module names components", author = "Wilson")
  public void changeModuleNamesComponents(SimpleMongoTemplateWrapper templateSimpleWrapper) {
    log.info("change database and module names components");
    MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();

    mongoTemplate.updateMulti(new Query(),
            new Update().set("_class", "com.rgapps.components.model.BusinessCardComponent"), "component");
  }
}
