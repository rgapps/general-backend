package com.rgapps.components.controller.resource;

import com.rgapps.common.exception.apiexceptions.UnexpectedValueException;
import com.rgapps.components.model.BusinessCardComponent;
import com.rgapps.components.model.Component;
import lombok.Value;

import java.util.UUID;

@Value
public class ComponentView {
  Component.Type componentType;
  String componentName;
  UUID componentId;
  Component.Status componentStatus;

  public static ComponentView fromComponent(Component component) {
    switch (component.getType()) {
      case BUSINESS_CARD:
        return fromComponent((BusinessCardComponent) component);
      default:
        throw new UnexpectedValueException("component.type", Component.Type.values(), component.getType());
    }
  }

  public static ComponentView fromComponent(BusinessCardComponent component) {
    return new ComponentView(
        component.getType(),
        component.getComponentName(),
        component.getId(),
        component.getStatus());
  }
}
