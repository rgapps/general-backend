package com.rgapps.components.controller.resource;

import com.rgapps.components.model.BusinessCardComponent;
import com.rgapps.components.model.Component;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotEmpty;

@EqualsAndHashCode(callSuper = true)
@Data
public class BusinessCardResource extends ComponentResource {
    @NotEmpty
    private String urlImageLogo;
    @NotEmpty
    private String name;
    @NotEmpty
    private String profession;

    public Component toComponent() {
        return new BusinessCardComponent(urlImageLogo, name, profession, componentName);
    }
}
