package com.rgapps.stocks.repository;

import com.rgapps.stocks.model.MonthlyProfitSummary;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.time.LocalDate;
import java.util.UUID;

@Repository
public interface MonthlyProfitSummaryRepository extends ReactiveMongoRepository<MonthlyProfitSummary, String> {
    Flux<MonthlyProfitSummary> findAllByMonthAndAccountIdOrderByUpdatedDateDesc(LocalDate yearMonth, UUID accountId);
}
