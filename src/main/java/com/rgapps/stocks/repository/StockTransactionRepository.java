package com.rgapps.stocks.repository;

import com.rgapps.stocks.model.StockTransaction;
import com.rgapps.stocks.model.StockTransaction.Status;
import com.rgapps.stocks.model.values.Stock;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface StockTransactionRepository extends ReactiveMongoRepository<StockTransaction, UUID> {
    Flux<StockTransaction> findAllByStatusOrderByCreatedDateDesc(Status success);

    Mono<StockTransaction> findFirstByStatusOrderByCreatedDateDesc(Status success);

    Mono<StockTransaction> findFirstByStatusAndStockCodeOrderByCreatedDateDesc(Status status, String stockCode);

    Mono<StockTransaction> findFirstByStatusAndTypeAndStockOrderByTransactionDateDesc(Status status, StockTransaction.Type type, Stock stock);
}
