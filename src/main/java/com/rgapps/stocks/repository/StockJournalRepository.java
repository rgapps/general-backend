package com.rgapps.stocks.repository;

import com.rgapps.stocks.model.StockJournal;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Repository
public interface StockJournalRepository extends ReactiveMongoRepository<StockJournal, String> {
    Flux<StockJournal> findAllByAccountIdAndNumberOfSharesGreaterThanOrderByLastBoughtDateDesc(UUID accountId, int numberOfShares);
}
