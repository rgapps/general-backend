package com.rgapps.stocks.repository;

import com.rgapps.stocks.model.values.Stock;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockValueRepository extends ReactiveMongoRepository<Stock, String> {
}
