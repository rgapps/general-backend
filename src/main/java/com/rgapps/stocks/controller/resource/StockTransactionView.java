package com.rgapps.stocks.controller.resource;

import com.rgapps.common.exception.apiexceptions.UnexpectedValueException;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.SellStockTransaction;
import com.rgapps.stocks.model.StockTransaction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockTransactionView {
    StockTransaction.Type transactionType;
    LocalDate date;
    String stockCode;
    int numberOfShares;
    double price;
    double fee;
    double totalMovement;
    double adjustedPrice;
    String currency;

    public static StockTransactionView fromTransaction(StockTransaction transaction) {
        switch (transaction.getType()) {
            case SELL:
                return fromTransaction((SellStockTransaction) transaction);
            case BUY:
                return fromTransaction((BuyStockTransaction) transaction);
            case DIVIDEND:
                return fromTransaction((GetDividendTransaction) transaction);
            default:
                throw new UnexpectedValueException("transaction.type", StockTransaction.Type.values(), transaction.getType());
        }
    }

    public static StockTransactionView fromTransaction(BuyStockTransaction transaction) {
        return new StockTransactionView(
                transaction.getType(),
                transaction.getTransactionDate(),
                transaction.getStock().getCode(),
                transaction.getNumberOfShares(),
                transaction.getPricePerShare().getValue(),
                transaction.getBrokerageFee().getValue(),
                transaction.getTotalSpent().getValue(),
                transaction.getAdjustedPrice().getValue(),
                transaction.getPricePerShare().getCurrency().getCode()
        );
    }

    public static StockTransactionView fromTransaction(SellStockTransaction transaction) {
        return new StockTransactionView(
                transaction.getType(),
                transaction.getTransactionDate(),
                transaction.getStock().getCode(),
                transaction.getNumberOfShares(),
                transaction.getPricePerShare().getValue(),
                transaction.getBrokerageFee().getValue(),
                transaction.getTotalReceived().getValue(),
                transaction.getAdjustedPrice().getValue(),
                transaction.getPricePerShare().getCurrency().getCode()
        );
    }

    public static StockTransactionView fromTransaction(GetDividendTransaction transaction) {
        return new StockTransactionView(
                transaction.getType(),
                transaction.getTransactionDate(),
                transaction.getStock().getCode(),
                transaction.getNumberOfShares(),
                transaction.getEarningsPerShare().getValue(),
                transaction.getDividendFee().getValue(),
                transaction.getTotalEarnings().getValue(),
                transaction.getAdjustedPrice().getValue(),
                transaction.getEarningsPerShare().getCurrency().getCode()
        );
    }
}
