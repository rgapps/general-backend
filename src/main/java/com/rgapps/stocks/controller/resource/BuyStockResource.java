package com.rgapps.stocks.controller.resource;

import com.rgapps.common.exception.apiexceptions.UnexpectedValueException;
import com.rgapps.common.exception.modelexceptions.PolicyValidationException;
import com.rgapps.common.validator.InThePastOrPresent;
import com.rgapps.common.validator.IsLocalDate;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.StockTransaction.Status;
import com.rgapps.stocks.model.values.Currency;
import com.rgapps.stocks.model.values.MoneyAmount;
import com.rgapps.stocks.model.values.Stock;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class BuyStockResource {
    @NotEmpty
    private String stockCode;
    @NotNull
    @Min(1)
    private Integer numberOfShares;
    @NotNull
    @Min(0)
    private Double buyPrice;
    @NotEmpty
    private String currencyCode;
    @NotEmpty
    @IsLocalDate
    @InThePastOrPresent
    private String dateBought;
    @NotNull
    @Min(0)
    private Double brokerageFee;

    public BuyStockTransaction toStockTransaction(Status status, UUID accountId) {
        return new BuyStockTransaction(
                accountId,
                new Stock(stockCode), numberOfShares,
                new MoneyAmount(buyPrice, new Currency(currencyCode)),
                LocalDate.parse(dateBought),
                new MoneyAmount(brokerageFee, new Currency(currencyCode)),
                status);
    }

    public boolean validateIsCorrectCurrency(String currencyCode) {
        Currency currency = new Currency(currencyCode);
        if (!currency.equals(Currency.ANY) && !this.currencyCode.equals(currency.getCode())) {
            throw new PolicyValidationException(
                    new UnexpectedValueException("Currency", currency.getCode(), this.currencyCode), this);
        }
        return true;
    }

    public boolean validateIsOlderOrEqualThan(LocalDate date) {
        if (LocalDate.parse(dateBought).isBefore(date)) {
            throw new PolicyValidationException(
                    new UnexpectedValueException("Date Sold", String.format("after %s", date), dateBought), this);
        }
        return true;
    }

    public boolean validateHasEnoughMoneyToBuy(double amount, UUID accountId) {
        double value = this.toStockTransaction(Status.SUCCESS, accountId).getTotalSpent().getValue();
        if (value > amount) {
            throw new PolicyValidationException(
                    new UnexpectedValueException("Money To Buy Stock", "<= " + amount, value), this);
        }
        return true;
    }
}
