package com.rgapps.stocks.controller.resource;

import com.rgapps.stocks.model.MonthlyProfitSummary;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SummaryForMonthView {
    String stockCode;
    double averageBoughtPrice;
    double averageSoldPrice;
    int numberOfShares;
    double profit;
    double profitPercentage;
    String currency;

    public static SummaryForMonthView fromMonthlyProfitSummary(MonthlyProfitSummary monthlyProfitSummary) {
        return new SummaryForMonthView(
                monthlyProfitSummary.getStock().getCode(),
                monthlyProfitSummary.getAverageBoughtPrice().getValue(),
                monthlyProfitSummary.getAverageSoldPrice().getValue(),
                monthlyProfitSummary.getNumberOfSharesSold(),
                monthlyProfitSummary.getTotalProfit().getValue(),
                monthlyProfitSummary.getProfitPercentage(),
                monthlyProfitSummary.getCurrency().getCode()
        );
    }
}
