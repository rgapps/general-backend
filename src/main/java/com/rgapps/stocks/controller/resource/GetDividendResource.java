package com.rgapps.stocks.controller.resource;

import com.rgapps.common.exception.apiexceptions.UnexpectedValueException;
import com.rgapps.common.exception.modelexceptions.PolicyValidationException;
import com.rgapps.common.validator.InThePastOrPresent;
import com.rgapps.common.validator.IsLocalDate;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.StockTransaction.Status;
import com.rgapps.stocks.model.values.Currency;
import com.rgapps.stocks.model.values.MoneyAmount;
import com.rgapps.stocks.model.values.Stock;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import static com.rgapps.stocks.model.values.Currency.ANY;

@Data
public class GetDividendResource {
    @NotEmpty
    private String stockCode;
    @NotNull
    @Min(1)
    private Integer numberOfShares;
    @NotNull
    @Min(0)
    private Double earningsPerShare;
    @NotEmpty
    private String currencyCode;
    @NotEmpty
    @IsLocalDate
    @InThePastOrPresent
    private String dateEarned;
    @NotNull
    @Min(0)
    private Double dividendFee;

    public GetDividendTransaction toStockTransaction(Status status, UUID accountId) {
        return new GetDividendTransaction(
                accountId,
                new Stock(stockCode), numberOfShares,
                new MoneyAmount(earningsPerShare, new Currency(currencyCode)),
                LocalDate.parse(dateEarned),
                new MoneyAmount(dividendFee, new Currency(currencyCode)),
                status);
    }

    public boolean validateIsCorrectCurrency(String currencyCode) {
        Currency currency = new Currency(currencyCode);
        if (!currency.equals(ANY) && !this.currencyCode.equals(currency.getCode())) {
            throw new PolicyValidationException(
                    new UnexpectedValueException("Currency", currency.getCode(), this.currencyCode), this);
        }
        return true;
    }
}
