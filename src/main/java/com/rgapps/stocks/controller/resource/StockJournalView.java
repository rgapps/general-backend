package com.rgapps.stocks.controller.resource;

import com.rgapps.stocks.model.StockJournal;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockJournalView {
    String stockCode;
    double averagePrice;
    int numberOfShares;
    LocalDate boughtDate;
    double marketValue;
    String currency;

    public static StockJournalView fromStockJournal(StockJournal stockJournal) {
        return new StockJournalView(
                stockJournal.getStockCode(),
                stockJournal.getAveragePricePerShare().getValue(),
                stockJournal.getNumberOfShares(),
                stockJournal.getLastBoughtDate(),
                stockJournal.getMarketValue().getValue(),
                stockJournal.getCurrency().getCode()
        );
    }
}
