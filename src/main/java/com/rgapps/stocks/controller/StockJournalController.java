package com.rgapps.stocks.controller;

import com.rgapps.common.validator.IsUUID;
import com.rgapps.stocks.controller.resource.StockJournalView;
import com.rgapps.stocks.service.StocksJournalService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping("/api/stocks/transactions/journal")
@CrossOrigin
@RequiredArgsConstructor
public class StockJournalController {
    private final StocksJournalService stocksJournalService;

    @GetMapping("")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Flux<StockJournalView> getJournal(@Valid @RequestParam @IsUUID @NotBlank String accountId) {
        return stocksJournalService.getStocksJournalView(UUID.fromString(accountId));
    }
}
