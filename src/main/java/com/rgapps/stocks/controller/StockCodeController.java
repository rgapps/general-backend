package com.rgapps.stocks.controller;

import com.rgapps.stocks.service.StockCodeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/api/stocks/code")
@CrossOrigin
@RequiredArgsConstructor
public class StockCodeController {
    private final StockCodeService stockCodeService;

    @GetMapping("")
    public Mono<List<String>> getCodes() {
        return stockCodeService.findCodes();
    }
}
