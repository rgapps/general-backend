package com.rgapps.stocks.service.client.resources;

import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.SellStockTransaction;
import com.rgapps.stocks.model.StockTransaction;
import lombok.Data;

import java.util.UUID;

import static com.rgapps.stocks.service.client.resources.CreditMovementResource.CreditType.STOCKS_DELETE_TRANSACTION;
import static com.rgapps.stocks.service.client.resources.CreditMovementResource.CreditType.STOCKS_DIVIDEND;
import static com.rgapps.stocks.service.client.resources.CreditMovementResource.CreditType.STOCKS_REVENUE;

@Data
public class CreditMovementResource {
    public enum CreditType {
        STOCKS_REVENUE,
        STOCKS_DIVIDEND,
        STOCKS_DELETE_TRANSACTION
    }

    private final Double creditAmount;
    private final String currencyCode;
    private final String dateCredited;
    private final UUID externalId;
    private final CreditType creditType;

    public static CreditMovementResource fromSellTransaction(SellStockTransaction transaction) {
        return new CreditMovementResource(
                transaction.getTotalReceived().getValue(),
                transaction.getTotalReceived().getCurrency().getCode(),
                transaction.getTransactionDate().toString(),
                transaction.getId(),
                STOCKS_REVENUE
        );
    }

    public static CreditMovementResource fromBuyDeletedTransaction(BuyStockTransaction transaction) {
        assert transaction.getStatus() == StockTransaction.Status.DELETED;

        return new CreditMovementResource(
                transaction.getTotalSpent().getValue(),
                transaction.getTotalSpent().getCurrency().getCode(),
                transaction.getTransactionDate().toString(),
                transaction.getId(),
                STOCKS_DELETE_TRANSACTION
        );
    }

    public static CreditMovementResource fromGetDividendTransaction(GetDividendTransaction transaction) {
        return new CreditMovementResource(
                transaction.getTotalEarnings().getValue(),
                transaction.getTotalEarnings().getCurrency().getCode(),
                transaction.getTransactionDate().toString(),
                transaction.getId(),
                STOCKS_DIVIDEND
        );
    }
}
