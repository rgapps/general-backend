package com.rgapps.stocks.service.client.resources;

import lombok.Data;

import java.util.UUID;

@Data
public class AccountSummaryView {
    private UUID accountId;
    private String accountName;
    private String currency;
    private double amount;
}
