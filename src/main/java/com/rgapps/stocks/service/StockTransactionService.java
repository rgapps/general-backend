package com.rgapps.stocks.service;

import com.rgapps.common.exception.apiexceptions.NotFoundException;
import com.rgapps.common.exception.apiexceptions.UnexpectedValueException;
import com.rgapps.stocks.controller.resource.BuyStockResource;
import com.rgapps.stocks.controller.resource.GetDividendResource;
import com.rgapps.stocks.controller.resource.SellStockResource;
import com.rgapps.stocks.controller.resource.StockTransactionView;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.SellStockTransaction;
import com.rgapps.stocks.model.StockJournal;
import com.rgapps.stocks.model.StockTransaction;
import com.rgapps.stocks.model.values.Stock;
import com.rgapps.stocks.repository.StockTransactionRepository;
import com.rgapps.stocks.service.client.AccountsClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.Map;
import java.util.UUID;

import static com.rgapps.stocks.model.StockTransaction.Status.DELETED;
import static com.rgapps.stocks.model.StockTransaction.Status.SUCCESS;
import static com.rgapps.stocks.model.StockTransaction.Type.BUY;
import static com.rgapps.stocks.model.StockTransaction.Type.SELL;

@Service
@RequiredArgsConstructor
public class StockTransactionService {
    private final StockTransactionRepository stockTransactionRepository;
    private final StocksJournalService stocksJournalService;
    private final StockCodeService stockCodeService;
    private final MonthlyProfitSummaryService monthlyProfitSummaryService;
    private final AccountsClient accountsClient;

    public Mono<BuyStockTransaction> buyStock(UUID accountId, BuyStockResource resource) {
        BuyStockTransaction transaction = resource.toStockTransaction(SUCCESS, accountId);
        return accountsClient.accountSummary(accountId)
                .filter(account -> resource.validateIsCorrectCurrency(account.getCurrency()) &&
                        resource.validateHasEnoughMoneyToBuy(account.getAmount(), accountId))
                .flatMap(it -> stocksJournalService.getStockJournal(transaction))
                .filter(stockJournal -> resource.validateIsOlderOrEqualThan(stockJournal.getLastBoughtDate()) &&
                        resource.validateIsOlderOrEqualThan(stockJournal.getLastSoldDate()))
                .zipWhen(stockJournal -> stockTransactionRepository.save(transaction), this::onStockBought)
                .flatMap(it -> it)
                .onErrorResume(e -> onTransactionBuyFailed(resource).flatMap(it -> Mono.error(e)));
    }

    private Mono<BuyStockTransaction> onStockBought(StockJournal stockJournal, BuyStockTransaction transaction) {
        return Flux.concat(
                stocksJournalService.updateWhenBuy(stockJournal, transaction),
                stockCodeService.saveStock(transaction.getStock()),
                accountsClient.debit(transaction)
        ).then(Mono.just(transaction));
    }

    private Mono<BuyStockResource> onTransactionBuyFailed(BuyStockResource resource) {
        return Mono.just(resource);
    }

    public Mono<SellStockTransaction> sellStock(UUID accountId, SellStockResource resource) {
        SellStockTransaction transaction = resource.toStockTransaction(SUCCESS, accountId);

        return accountsClient.accountSummary(accountId)
                .filter(account -> resource.validateIsCorrectCurrency(account.getCurrency()))
                .flatMap(it -> stocksJournalService.getStockJournal(transaction))
                .filter(stockJournal -> resource.validateHasEnoughtSharesToSell(stockJournal.getNumberOfShares()) &&
                        resource.validateIsOlderOrEqualThan(stockJournal.getLastBoughtDate()) &&
                        resource.validateIsOlderOrEqualThan(stockJournal.getLastSoldDate()))
                .zipWhen(stockJournal -> stockTransactionRepository.save(transaction))
                .flatMap(tuple -> onStockSold(tuple.getT1(), tuple.getT2()))
                .onErrorResume(e -> onTransactionSoldFailed(resource).flatMap(it -> Mono.error(e)));
    }

    private Mono<SellStockTransaction> onStockSold(StockJournal stockJournal, SellStockTransaction transaction) {
        return Flux.concat(
                stocksJournalService.updateWhenSell(stockJournal, transaction),
                monthlyProfitSummaryService.updateWhenSell(transaction),
                accountsClient.creditOnSell(transaction)
        ).then(Mono.just(transaction));
    }

    private Mono<SellStockResource> onTransactionSoldFailed(SellStockResource resource) {
        return Mono.just(resource);
    }

    public Mono<GetDividendTransaction> getDividend(GetDividendResource resource, UUID accountId) {
        GetDividendTransaction transaction = resource.toStockTransaction(SUCCESS, accountId);

        return accountsClient.accountSummary(accountId)
                .filter(account -> resource.validateIsCorrectCurrency(account.getCurrency()))
                .flatMap(it -> stocksJournalService.getStockJournal(transaction))
                .zipWhen(stockJournal -> stockTransactionRepository.save(transaction))
                .flatMap(tuple -> onDividendGotten(tuple.getT1(), tuple.getT2()))
                .onErrorResume(e -> onTransactionDividendFailed(resource).flatMap(it -> Mono.error(e)));
    }

    private Mono<GetDividendTransaction> onDividendGotten(StockJournal stockJournal, GetDividendTransaction transaction) {
        return Flux.concat(
                monthlyProfitSummaryService.updateWhenDividend(transaction),
                accountsClient.creditOnDividend(transaction)
        ).then(Mono.just(transaction));
    }

    private Mono<GetDividendResource> onTransactionDividendFailed(GetDividendResource resource) {
        return Mono.just(resource);
    }

    public Mono<StockTransaction> deleteLast() {
        return stockTransactionRepository.findFirstByStatusOrderByCreatedDateDesc(SUCCESS)
                .switchIfEmpty(Mono.error(new NotFoundException("Success Transaction", "CreatedDate", "oldest")))
                .flatMap(it -> stockTransactionRepository.save(it.updateStatus(DELETED)))
                .flatMap(this::onTransactionDeleted);
    }

    public Mono<StockTransaction> deleteLastForStock(String stockCode) {
        return stockTransactionRepository.findFirstByStatusAndStockCodeOrderByCreatedDateDesc(SUCCESS, stockCode)
                .switchIfEmpty(Mono.error(new NotFoundException("Success Transaction",
                        Map.of("createdDate", "oldest",
                                "stockCode", stockCode))))
                .flatMap(it -> stockTransactionRepository.save(it.updateStatus(DELETED)))
                .flatMap(this::onTransactionDeleted);
    }

    private Mono<StockTransaction> onTransactionDeleted(StockTransaction transaction) {
        switch (transaction.getType()) {
            case BUY:
                return getLastBoughtDate(transaction.getStock())
                        .flatMap(date -> onBuyTransactionDeleted((BuyStockTransaction) transaction, date));
            case SELL:
                return getLastSoldDate(transaction.getStock())
                        .flatMap(date -> onSellTransactionDeleted((SellStockTransaction) transaction, date));
            case DIVIDEND:
                return getLastSoldDate(transaction.getStock())
                        .flatMap(date -> onDividendTransactionDeleted((GetDividendTransaction) transaction));
            default:
                return Mono.error(new UnexpectedValueException("transaction.type", StockTransaction.Type.values(), transaction.getType()));
        }
    }

    private Mono<BuyStockTransaction> onBuyTransactionDeleted(BuyStockTransaction transaction, LocalDate date) {
        return Flux.concat(
                stocksJournalService.updateWhenDeleteSellTransaction(transaction, date).thenReturn(transaction),
                accountsClient.creditOnDelete(transaction)
        ).then(Mono.just(transaction));
    }

    private Mono<StockTransaction> onSellTransactionDeleted(SellStockTransaction transaction, LocalDate date) {
        return Flux.concat(
                stocksJournalService.updateWhenDeleteSellTransaction(transaction, date),
                monthlyProfitSummaryService.updateWhenDeleteSellTransaction(transaction),
                accountsClient.debitOnDeleteSellTransaction(transaction)
        ).then(Mono.just(transaction));
    }

    private Mono<StockTransaction> onDividendTransactionDeleted(GetDividendTransaction transaction) {
        return Flux.concat(
                monthlyProfitSummaryService.updateWhenDeleteDividendTransaction(transaction),
                accountsClient.debitOnDeleteDividendTransaction(transaction)
        ).then(Mono.just(transaction));
    }

    private Mono<LocalDate> getLastBoughtDate(Stock stock) {
        return stockTransactionRepository.findFirstByStatusAndTypeAndStockOrderByTransactionDateDesc(SUCCESS, BUY, stock)
                .map(StockTransaction::getTransactionDate)
                .defaultIfEmpty(LocalDate.EPOCH);
    }

    private Mono<LocalDate> getLastSoldDate(Stock stock) {
        return stockTransactionRepository.findFirstByStatusAndTypeAndStockOrderByTransactionDateDesc(SUCCESS, SELL, stock)
                .map(StockTransaction::getTransactionDate)
                .defaultIfEmpty(LocalDate.EPOCH);
    }

    public Flux<StockTransactionView> getStockTransactionsView() {
        return stockTransactionRepository.findAllByStatusOrderByCreatedDateDesc(StockTransaction.Status.SUCCESS)
                .map(StockTransactionView::fromTransaction);
    }
}
