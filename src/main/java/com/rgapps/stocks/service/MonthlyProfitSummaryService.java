package com.rgapps.stocks.service;

import com.rgapps.stocks.controller.resource.SummaryForMonthView;
import com.rgapps.stocks.model.GetDividendTransaction;
import com.rgapps.stocks.model.MonthlyProfitSummary;
import com.rgapps.stocks.model.SellStockTransaction;
import com.rgapps.stocks.model.StockJournal;
import com.rgapps.stocks.repository.MonthlyProfitSummaryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class MonthlyProfitSummaryService {
    private final MonthlyProfitSummaryRepository monthlyProfitSummaryRepository;
    private final StocksJournalService stocksJournalService;

    public Mono<MonthlyProfitSummary> updateWhenSell(SellStockTransaction transaction) {
        LocalDate date = transaction.getTransactionDate();

        return monthlyProfitSummaryRepository.findById(MonthlyProfitSummary.calculateId(transaction))
                .defaultIfEmpty(MonthlyProfitSummary.empty(date, transaction.getStock(), transaction.getAccountId()))
                .zipWith(stocksJournalService.getStockJournal(transaction))
                .map(it -> {
                    MonthlyProfitSummary monthlyProfitSummary = it.getT1();
                    StockJournal stockJournal = it.getT2();
                    return monthlyProfitSummary.updateSummaryWhenSell(stockJournal, transaction);
                })
                .flatMap(monthlyProfitSummaryRepository::save)
                .flatMap(this::onSummaryUpdated);
    }

    public Mono<MonthlyProfitSummary> updateWhenDividend(GetDividendTransaction transaction) {
        LocalDate date = transaction.getTransactionDate();

        return monthlyProfitSummaryRepository.findById(MonthlyProfitSummary.calculateId(transaction))
                .defaultIfEmpty(MonthlyProfitSummary.empty(date, transaction.getStock(), transaction.getAccountId()))
                .zipWith(stocksJournalService.getStockJournal(transaction))
                .map(it -> {
                    MonthlyProfitSummary monthlyProfitSummary = it.getT1();
                    StockJournal stockJournal = it.getT2();
                    return monthlyProfitSummary.updateSummaryWhenDividend(stockJournal, transaction);
                })
                .flatMap(monthlyProfitSummaryRepository::save)
                .flatMap(this::onSummaryUpdated);
    }

    public Mono<MonthlyProfitSummary> updateWhenDeleteSellTransaction(SellStockTransaction transaction) {
        return Mono.just(transaction)
                .flatMap(it -> monthlyProfitSummaryRepository.findById(MonthlyProfitSummary.calculateId(transaction)))
                .zipWith(stocksJournalService.getStockJournal(transaction))
                .map(it -> {
                    MonthlyProfitSummary monthlyProfitSummary = it.getT1();
                    StockJournal stockJournal = it.getT2();
                    return monthlyProfitSummary.updateWhenDeleteSellTransaction(stockJournal, transaction);
                })
                .flatMap(monthlyProfitSummaryRepository::save)
                .flatMap(this::onSummaryUpdated);
    }

    public Mono<MonthlyProfitSummary> updateWhenDeleteDividendTransaction(GetDividendTransaction transaction) {
        return Mono.just(transaction)
                .flatMap(it -> monthlyProfitSummaryRepository.findById(MonthlyProfitSummary.calculateId(transaction)))
                .zipWith(stocksJournalService.getStockJournal(transaction))
                .map(it -> {
                    MonthlyProfitSummary monthlyProfitSummary = it.getT1();
                    StockJournal stockJournal = it.getT2();
                    return monthlyProfitSummary.updateWhenDeleteDividendTransaction(stockJournal, transaction);
                })
                .flatMap(monthlyProfitSummaryRepository::save)
                .flatMap(this::onSummaryUpdated);
    }

    private Mono<MonthlyProfitSummary> onSummaryUpdated(MonthlyProfitSummary gains) {
        return Mono.just(gains);
    }

    public Flux<SummaryForMonthView> getSummaryViewForMonth(LocalDate yearMonth, UUID accountId) {
        return monthlyProfitSummaryRepository.findAllByMonthAndAccountIdOrderByUpdatedDateDesc(yearMonth, accountId)
                .filter(profitSummary -> profitSummary.getNumberOfSharesSold() > 0)
                .map(SummaryForMonthView::fromMonthlyProfitSummary);
    }
}
