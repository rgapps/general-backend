package com.rgapps.stocks.service;

import com.rgapps.stocks.controller.resource.StockJournalView;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.SellStockTransaction;
import com.rgapps.stocks.model.StockJournal;
import com.rgapps.stocks.model.StockTransaction;
import com.rgapps.stocks.repository.StockJournalRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class StocksJournalService {
    private final StockJournalRepository stockJournalRepository;

    public Mono<StockJournal> updateWhenBuy(StockJournal stockJournal, BuyStockTransaction transaction) {
        return stockJournalRepository.save(stockJournal.updateWhenBuy(transaction))
                .flatMap(this::onStocksSummaryUpdated);
    }

    public Mono<StockJournal> updateWhenSell(StockJournal stockJournal, SellStockTransaction transaction) {
        return stockJournalRepository.save(stockJournal.updateWhenSell(transaction))
                .flatMap(this::onStocksSummaryUpdated);
    }

    public Mono<StockJournal> updateWhenDeleteSellTransaction(BuyStockTransaction transaction, LocalDate updatedLastBoughtDate) {
        return getStockJournal(transaction)
                .flatMap(stockJournal -> stockJournalRepository.save(stockJournal.updateWhenDeleteBuyTransaction(transaction, updatedLastBoughtDate)))
                .flatMap(this::onStocksSummaryUpdated);
    }

    public Mono<StockJournal> updateWhenDeleteSellTransaction(SellStockTransaction transaction, LocalDate updatedLastSoldDate) {
        return getStockJournal(transaction)
                .flatMap(stockJournal -> stockJournalRepository.save(stockJournal.updateWhenDeleteSellTransaction(transaction, updatedLastSoldDate)))
                .flatMap(this::onStocksSummaryUpdated);
    }

    private Mono<StockJournal> onStocksSummaryUpdated(StockJournal journal) {
        return Mono.just(journal);
    }

    public Mono<StockJournal> getStockJournal(StockTransaction transaction) {
        return stockJournalRepository.findById(StockJournal.calculateId(transaction))
                .defaultIfEmpty(StockJournal.empty(transaction));
    }

    public Flux<StockJournalView> getStocksJournalView(UUID accountId) {
        return stockJournalRepository.findAllByAccountIdAndNumberOfSharesGreaterThanOrderByLastBoughtDateDesc(accountId, 0)
                .map(StockJournalView::fromStockJournal);
    }
}
