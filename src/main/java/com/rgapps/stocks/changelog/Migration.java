package com.rgapps.stocks.changelog;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.mongodb.MongoNamespace;
import com.mongodb.client.MongoDatabase;
import com.rgapps.accounts.model.Account;
import com.rgapps.common.config.SimpleMongoTemplateWrapper;
import com.rgapps.stocks.model.MonthlyProfitSummary;
import com.rgapps.stocks.model.StockJournal;
import com.rgapps.stocks.model.StockTransaction;
import com.rgapps.stocks.model.values.Currency;
import com.rgapps.stocks.model.values.MoneyAmount;
import com.rgapps.stocks.model.values.Stock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import static com.rgapps.stocks.model.MonthlyProfitSummary.calculateId;

@ChangeLog(order = "004")
@Slf4j
public class Migration {
    @ChangeSet(order = "001", id = "renameCollections", author = "Wilson")
    public void renameCollections(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        MongoDatabase db = templateSimpleWrapper.getMongoTemplate().getDb();
        try {
            db.getCollection("stock-summary")
                    .renameCollection(new MongoNamespace(db.getName(), "stock-journal"));
        } catch (Exception e) {
            log.info(String.format("renameCollections: collection stock-summary was not found (%s)", e.getMessage()));
        }

        try {
            db.getCollection("monthly-gains")
                    .renameCollection(new MongoNamespace(db.getName(), "monthly-profit-summary"));
        } catch (Exception e) {
            log.info(String.format("renameCollections: collection monthly-gains was not found (%s)", e.getMessage()));
        }
    }

    @ChangeSet(order = "002", id = "add brokerage fee on buy transaction and lastSoldDate", author = "Wilson")
    public void addBrokerageFeeAndLastSoldDate(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();

        mongoTemplate.updateMulti(
                new Query().addCriteria(new Criteria("type").is(StockTransaction.Type.BUY)),
                new Update().set("brokerageFee", MoneyAmount.zeroAmount), StockTransaction.class);

        mongoTemplate.updateMulti(
                new Query(),
                new Update().set("lastSoldDate", LocalDate.EPOCH), StockJournal.class);
    }

    @ChangeSet(order = "003", id = "update Classes due to refactor", author = "Wilson")
    public void updateClassesDueToRefactor(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();

        mongoTemplate.updateMulti(
                new Query().addCriteria(new Criteria("_class").is("com.wilsonr990.stocks.model.BuyStockTransaction")),
                new Update().set("_class", "com.wilsonr990.page.stocks.model.BuyStockTransaction"), StockTransaction.class);

        mongoTemplate.updateMulti(
                new Query().addCriteria(new Criteria("_class").is("com.wilsonr990.stocks.model.SellStockTransaction")),
                new Update().set("_class", "com.wilsonr990.page.stocks.model.SellStockTransaction"), StockTransaction.class);

        mongoTemplate.updateMulti(
                new Query().addCriteria(new Criteria("_class").is("com.wilsonr990.stocks.model.StockJournal")),
                new Update().set("_class", "com.wilsonr990.page.stocks.model.StockJournal"), StockJournal.class);

        mongoTemplate.updateMulti(
                new Query().addCriteria(new Criteria("_class").is("com.wilsonr990.stocks.model.MonthlyProfitSummary")),
                new Update().set("_class", "com.wilsonr990.page.stocks.model.MonthlyProfitSummary"), MonthlyProfitSummary.class);

        mongoTemplate.updateMulti(
                new Query().addCriteria(new Criteria("_class").is("com.wilsonr990.stocks.model.values.Stock")),
                new Update().set("_class", "com.wilsonr990.page.stocks.model.values.Stock"), Stock.class);

        mongoTemplate.updateMulti(
                new Query().addCriteria(new Criteria("_class").is("com.wilsonr990.stocks.model.values.Currency")),
                new Update().set("_class", "com.wilsonr990.page.stocks.model.values.Currency"), Currency.class);
    }

    @ChangeSet(order = "004", id = "remove failed transactions", author = "Wilson")
    public void removeFailedTransactionsFix(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();

        mongoTemplate.remove(
                new Query().addCriteria(new Criteria("status").is("FAILED")), StockTransaction.class);
    }

    @ChangeSet(order = "005", id = "remove currencies collection", author = "Wilson")
    public void removeCurrenciesCollection(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();

        mongoTemplate.dropCollection("currencies");
    }

    @ChangeSet(order = "006", id = "recalculate journey and summary ids", author = "Wilson")
    public void recalculateJourneyAndSummaryIds(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();

        Account account = Objects.requireNonNull(mongoTemplate
                .findOne(new Query().addCriteria(new Criteria("accountName").is("DEFAULT")), Account.class));

        List<StockJournal> stockJournals = mongoTemplate.findAll(StockJournal.class);
        stockJournals.forEach(journal -> {
            mongoTemplate.remove(journal);
            journal.setStockCode(journal.getId());
            journal.setId(StockJournal.calculateId(journal.getId(), account.getId()));
            journal.setAccountId(account.getId());
            mongoTemplate.save(journal);
        });
        List<MonthlyProfitSummary> summaries = mongoTemplate.findAll(MonthlyProfitSummary.class);
        summaries.forEach(summary -> {
                    mongoTemplate.remove(summary);
                    summary.setId(calculateId(summary.getMonth(), summary.getStock(), account.getId()));
                    summary.setAccountId(account.getId());
                    mongoTemplate.save(summary);
                }
        );
    }

    @ChangeSet(order = "007", id = "change module names stocks", author = "Wilson")
    public void changeModuleNamesStocks(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        log.info("change database and module names stocks");
        MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();


        mongoTemplate.updateMulti(new Query(),
                new Update().set("_class", "com.rgapps.stocks.model.values.Stock"), "stock");

        mongoTemplate.updateMulti(new Query(),
                new Update().set("_class", "com.rgapps.stocks.model.StockJournal"), "stock-journal");

        mongoTemplate.updateMulti(new Query(),
                new Update().set("_class", "com.rgapps.stocks.model.MonthlyProfitSummary"), "monthly-profit-summary");

        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("type").is("BUY")),
                new Update().set("_class", "com.rgapps.stocks.model.BuyStockTransaction"), "stock-transaction");

        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("type").is("SELL")),
                new Update().set("_class", "com.rgapps.stocks.model.SellStockTransaction"), "stock-transaction");

        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("type").is("DIVIDEND")),
                new Update().set("_class", "com.rgapps.stocks.model.GetDividendStockTransaction"), "stock-transaction");
    }
}
