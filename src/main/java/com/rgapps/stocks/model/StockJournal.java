package com.rgapps.stocks.model;

import com.rgapps.stocks.model.values.Currency;
import com.rgapps.stocks.model.values.MoneyAmount;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Document(collection = "stock-journal")
public class StockJournal {
    @EqualsAndHashCode.Exclude
    private LocalDateTime updatedDate;

    @Id
    private String id;
    private String stockCode;
    private int numberOfShares;
    private MoneyAmount averagePricePerShare;
    private LocalDate lastBoughtDate;
    private LocalDate lastSoldDate;
    private UUID accountId;

    public StockJournal(String stockCode,
                        int numberOfShares,
                        MoneyAmount averagePricePerShare,
                        LocalDate lastBoughtDate,
                        LocalDate lastSoldDate,
                        UUID accountId) {
        this.stockCode = stockCode;
        this.numberOfShares = numberOfShares;
        this.averagePricePerShare = averagePricePerShare;
        this.lastBoughtDate = lastBoughtDate;
        this.lastSoldDate = lastSoldDate;
        this.accountId = accountId;

        this.updatedDate = LocalDateTime.now();
        id = calculateId(stockCode, accountId);
    }

    public static String calculateId(String stockCode, UUID accountId) {
        return String.format("%s-%s", stockCode, accountId);
    }

    public static String calculateId(StockTransaction transaction) {
        return calculateId(transaction.stock.getCode(), transaction.getAccountId());
    }

    public static StockJournal empty(String stockCode, UUID accountId) {
        return new StockJournal(stockCode, 0, MoneyAmount.zeroAmount, LocalDate.EPOCH, LocalDate.EPOCH, accountId);
    }

    public static StockJournal empty(StockTransaction transaction) {
        return empty(transaction.stock.getCode(), transaction.getAccountId());
    }

    public StockJournal updateWhenBuy(BuyStockTransaction transaction) {
        assert stockCode.equals(transaction.getStock().getCode());
        assert transaction.getStatus().equals(StockTransaction.Status.SUCCESS);
        assert transaction.getType().equals(StockTransaction.Type.BUY);
        assert transaction.getAccountId().equals(accountId);

        int totalShares = this.numberOfShares + transaction.getNumberOfShares();
        this.averagePricePerShare = this.getMarketValue().plus(transaction.getTotalSpent()).dividedBy(totalShares);
        this.numberOfShares = totalShares;

        LocalDate boughtDate = transaction.transactionDate;
        if (boughtDate.isAfter(lastBoughtDate)) {
            lastBoughtDate = boughtDate;
        }

        updatedDate = LocalDateTime.now();
        return this;
    }

    public StockJournal updateWhenSell(SellStockTransaction transaction) {
        assert stockCode.equals(transaction.getStock().getCode());
        assert transaction.getStatus().equals(StockTransaction.Status.SUCCESS);
        assert transaction.getNumberOfShares() <= numberOfShares;
        assert transaction.getAccountId().equals(accountId);

        LocalDate soldDate = transaction.transactionDate;
        if (soldDate.isAfter(lastSoldDate)) {
            lastSoldDate = soldDate;
        }

        this.numberOfShares = this.numberOfShares - transaction.getNumberOfShares();
        updatedDate = LocalDateTime.now();
        return this;
    }

    public StockJournal updateWhenDeleteBuyTransaction(BuyStockTransaction transaction, LocalDate correctedBoughtDate) {
        assert stockCode.equals(transaction.getStock().getCode());
        assert transaction.getType().equals(StockTransaction.Type.BUY);
        assert transaction.getStatus().equals(StockTransaction.Status.DELETED);
        assert transaction.getAccountId().equals(accountId);

        int totalShares = this.numberOfShares - transaction.getNumberOfShares();
        this.averagePricePerShare = this.getMarketValue().minus(transaction.getTotalSpent()).dividedBy(totalShares);
        this.numberOfShares = this.numberOfShares - transaction.getNumberOfShares();
        this.lastBoughtDate = correctedBoughtDate;
        this.updatedDate = LocalDateTime.now();
        return this;
    }

    public StockJournal updateWhenDeleteSellTransaction(SellStockTransaction transaction, LocalDate correctedSoldDate) {
        assert stockCode.equals(transaction.getStock().getCode());
        assert transaction.getType().equals(StockTransaction.Type.SELL);
        assert transaction.getStatus().equals(StockTransaction.Status.DELETED);
        assert transaction.getAccountId().equals(accountId);

        this.numberOfShares = this.numberOfShares + transaction.getNumberOfShares();
        this.lastSoldDate = correctedSoldDate;
        this.updatedDate = LocalDateTime.now();
        return this;
    }

    public MoneyAmount getMarketValue() {
        return averagePricePerShare.multipliedBy(this.numberOfShares);
    }

    public Currency getCurrency() {
        return averagePricePerShare.getCurrency();
    }
}
