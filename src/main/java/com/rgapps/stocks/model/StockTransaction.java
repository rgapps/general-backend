package com.rgapps.stocks.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.rgapps.stocks.model.values.MoneyAmount;
import com.rgapps.stocks.model.values.Stock;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Document(collection = "stock-transaction")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BuyStockTransaction.class, name = "BUY"),
        @JsonSubTypes.Type(value = SellStockTransaction.class, name = "SELL"),
        @JsonSubTypes.Type(value = GetDividendTransaction.class, name = "DIVIDEND")
})
public abstract class StockTransaction {
    public enum Type {
        SELL, BUY, DIVIDEND
    }

    public enum Status {
        SUCCESS, DELETED
    }

    @Id
    protected UUID id = UUID.randomUUID();
    @EqualsAndHashCode.Exclude
    protected LocalDateTime createdDate = LocalDateTime.now();
    @EqualsAndHashCode.Exclude
    protected LocalDateTime updatedDate = createdDate;

    @NonNull
    private UUID accountId;
    @NonNull
    private Type type;
    @NonNull
    protected Stock stock;
    @NonNull
    protected Integer numberOfShares;
    @NonNull
    protected LocalDate transactionDate;
    @NonNull
    protected Status status;

    public StockTransaction updateStatus(Status status) {
        this.setStatus(status);
        updatedDate = LocalDateTime.now();

        return this;
    }

    public abstract MoneyAmount getAdjustedPrice();
}
