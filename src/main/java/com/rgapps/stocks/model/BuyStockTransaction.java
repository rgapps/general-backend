package com.rgapps.stocks.model;

import com.rgapps.stocks.model.values.MoneyAmount;
import com.rgapps.stocks.model.values.Stock;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class BuyStockTransaction extends StockTransaction {
    protected MoneyAmount brokerageFee;
    protected MoneyAmount pricePerShare;

    public BuyStockTransaction(UUID accountId,
                               Stock stock,
                               int numberOfShares,
                               MoneyAmount pricePerShare,
                               LocalDate transactionDate,
                               MoneyAmount brokerageFee,
                               Status status) {
        super(accountId, Type.BUY, stock, numberOfShares, transactionDate, status);
        this.brokerageFee = brokerageFee;
        this.pricePerShare = pricePerShare;
    }

    public MoneyAmount getTotalSpent() {
        return pricePerShare.multipliedBy(this.numberOfShares).plus(brokerageFee);
    }

    @Override
    public MoneyAmount getAdjustedPrice() {
        return getTotalSpent().dividedBy(numberOfShares);
    }
}
