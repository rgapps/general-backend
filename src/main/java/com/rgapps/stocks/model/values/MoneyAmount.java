package com.rgapps.stocks.model.values;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MoneyAmount {
    public static MoneyAmount zeroAmount = new MoneyAmount(0.0, Currency.ANY);

    double value;
    Currency currency;

    public MoneyAmount multipliedBy(int multiplier) {
        return new MoneyAmount(value * multiplier, currency);
    }

    public MoneyAmount dividedBy(double divisor) {
        return divisor == 0 ? zeroAmount : new MoneyAmount(value / divisor, currency);
    }

    public MoneyAmount dividedBy(MoneyAmount other) {
        assert currency == Currency.ANY || currency.equals(other.currency);

        return this.dividedBy(other.getValue());
    }

    public MoneyAmount plus(MoneyAmount other) {
        assert currency == Currency.ANY || currency.equals(other.currency);

        return new MoneyAmount(value + other.getValue(), other.getCurrency());
    }

    public MoneyAmount minus(MoneyAmount other) {
        assert currency == Currency.ANY || currency.equals(other.currency);

        return new MoneyAmount(value - other.getValue(), other.getCurrency());
    }
}
