package com.rgapps.auth.service;

import com.rgapps.auth.controller.resource.AuthResponse;
import com.rgapps.auth.controller.resource.LoginResource;
import com.rgapps.auth.controller.resource.SignupResource;
import com.rgapps.auth.model.Role;
import com.rgapps.auth.model.User;
import com.rgapps.auth.repository.RoleRepository;
import com.rgapps.auth.repository.UserRepository;
import com.rgapps.common.exception.apiexceptions.DuplicatedValueException;
import com.rgapps.common.exception.apiexceptions.NotFoundException;
import com.rgapps.common.exception.modelexceptions.PolicyValidationException;
import com.rgapps.common.security.AuthenticationUtils;
import com.rgapps.common.security.JwtUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.security.sasl.AuthenticationException;
import javax.validation.Valid;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final JwtUtils jwtUtils;

    public Mono<AuthResponse> authenticateUser(@Valid LoginResource loginRequest) {
        return userRepository.findByUsername(loginRequest.getUsername())
                .switchIfEmpty(Mono.error(new AuthenticationException("User Not found")))
                .filter(userDetails -> passwordEncoder.matches(loginRequest.getPassword(), userDetails.getPassword()))
                .switchIfEmpty(Mono.error(new AuthenticationException("Invalid Password")))
                .map(userDetails -> {
                    Map<String, Object> claims = new HashMap<>();
                    claims.put("role", userDetails.getRoles().stream().map(role -> role.getType().name()).collect(Collectors.toList()));
                    claims.put("id", userDetails.getId());
                    return new AuthResponse(jwtUtils.generateToken(claims, userDetails.getUsername()));
                });
    }

    public Mono<User> registerUser(@Valid SignupResource signupResource) {
        return userRepository.existsByUsername(signupResource.getUsername())
                .filter(exists -> !exists)
                .switchIfEmpty(Mono.error(
                        new PolicyValidationException(new DuplicatedValueException("User", signupResource.getUsername()), signupResource)))
                .then(createNewUser(signupResource))
                .flatMap(userRepository::save);
    }

    private Mono<User> createNewUser(SignupResource signupResource) {
        return Flux.fromStream(signupResource.getRoles().stream())
                .flatMap(roleType ->
                        roleRepository.findByType(Role.RoleType.valueOf(roleType))
                                .switchIfEmpty(
                                        Mono.error(new PolicyValidationException(
                                                new NotFoundException("role", "type", roleType), signupResource))))
                .collectList()
                .map(roles -> new User(
                        signupResource.getUsername(),
                        passwordEncoder.encode(signupResource.getPassword()), Set.copyOf(roles)
                ));
    }

    public Flux<User> getUsers() {
        return userRepository.findAll();
    }

    public Mono<User> getCurrentUser() {
        return AuthenticationUtils.getCurrentUserId()
                .flatMap(userRepository::findById);
    }
}
