package com.rgapps.auth.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Data
@Document(collection = "user")
public class User {
    @Id
    protected UUID id;
    @EqualsAndHashCode.Exclude
    protected LocalDateTime createdDate;
    @EqualsAndHashCode.Exclude
    protected LocalDateTime updatedDate;

    protected String username;
    protected String password;
    protected Set<Role> roles;

    public User(String username, String password, Set<Role> roles) {
        this.username = username;
        this.password = password;
        this.roles = roles;

        id = UUID.randomUUID();
        createdDate = LocalDateTime.now();
        updatedDate = createdDate;
    }
}
