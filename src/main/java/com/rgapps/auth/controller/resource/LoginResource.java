package com.rgapps.auth.controller.resource;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class LoginResource {
    @NotEmpty
    private String username;
    @NotEmpty
    private String password;
}
