package com.rgapps.auth.changelog;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.rgapps.auth.model.Role;
import com.rgapps.auth.model.User;
import com.rgapps.common.config.SimpleMongoTemplateWrapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Set;

@ChangeLog(order = "001")
@Slf4j
public class Migration {
    @ChangeSet(order = "001", id = "insert Admin user and roles", author = "Wilson")
    public void insertAdminUserAndRoles(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        log.info("insert Admin user and roles");
        String encodedPassword = "$2a$10$m/zh8bNkXF547FYwGnJinOH4o74HSEZaZQMp6nMhJyMvpcNbcit5m";

        templateSimpleWrapper.getMongoTemplate().insert(new Role(Role.RoleType.ROLE_ADMIN));
        templateSimpleWrapper.getMongoTemplate().insert(new Role(Role.RoleType.ROLE_MODERATOR));
        templateSimpleWrapper.getMongoTemplate().insert(new Role(Role.RoleType.ROLE_USER));
        templateSimpleWrapper.getMongoTemplate()
                .insert(new User("admin", encodedPassword, Set.of(new Role(Role.RoleType.ROLE_ADMIN))));
    }

    @ChangeSet(order = "002", id = "change module names auth", author = "Wilson")
    public void changeModuleNamesAuth(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        log.info("change database and module names auth");
        MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();

        mongoTemplate.updateMulti(new Query(),
                new Update().set("_class", "com.rgapps.auth.model.Role"), "role");

        mongoTemplate.updateMulti(new Query(),
                new Update().set("_class", "com.rgapps.auth.model.User"), "user");
    }
}
