package com.rgapps.data.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.UUID;

@Data
@Document(collection = "raw-data")
@NoArgsConstructor
public class RawData {
    @Id
    protected UUID id;
    @CreatedDate
    protected LocalDateTime createdDate;
    @LastModifiedDate
    protected LocalDateTime updatedDate;

    @Indexed
    protected String resourceName;
    protected Map<String, Object> props;

    private RawData(UUID id, String resourceName, Map<String, Object> props) {
        this.resourceName = resourceName;
        this.props = props;
        this.id = id;

        updatedDate = LocalDateTime.now();
    }

    public RawData(String resourceName, Map<String, Object> props) {
        this(UUID.randomUUID(), resourceName, props);
        createdDate = updatedDate;
    }

    public RawData(RawData existingData, Map<String, Object> props) {
        this(existingData.getId(), existingData.getResourceName(), props);
        this.createdDate = existingData.getCreatedDate();
    }
}
