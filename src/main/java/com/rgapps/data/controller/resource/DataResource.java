package com.rgapps.data.controller.resource;

import com.rgapps.data.model.RawData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;

@EqualsAndHashCode(callSuper = true)
@Data
public class DataResource extends HashMap<String, Object> {

    public RawData toRawData(RawData existingData) {
        return new RawData(existingData, this);
    }

    public RawData toRawData(String resourceName) {
        return new RawData(resourceName, this);
    }
}
