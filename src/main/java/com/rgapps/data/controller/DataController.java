package com.rgapps.data.controller;

import com.rgapps.common.validator.IsUUID;
import com.rgapps.data.controller.resource.DataResource;
import com.rgapps.data.controller.resource.DataView;
import com.rgapps.data.service.DataService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/data")
@CrossOrigin
@RequiredArgsConstructor
public class DataController {
    private final DataService dataService;

    @GetMapping("{resourceName}")
    public Flux<DataView> getAll(@PathVariable String resourceName) {
        return dataService.getAll(resourceName);
    }

    @GetMapping("{resourceName}/{id}")
    public Mono<DataView> get(@PathVariable String resourceName, @Valid @IsUUID @PathVariable String id) {
        return dataService.get(resourceName, UUID.fromString(id));
    }

    @PostMapping("{resourceName}")
    public Mono<DataView> create(@PathVariable String resourceName, @Valid @RequestBody DataResource resource) {
        return dataService.create(resourceName, resource);
    }

    @PutMapping("{resourceName}/{id}")
    public Mono<DataView> update(
            @PathVariable String resourceName,
            @Valid @IsUUID @PathVariable String id,
            @Valid @RequestBody DataResource resource) {
        return dataService.update(resourceName, UUID.fromString(id), resource);
    }

    @DeleteMapping("{resourceName}/{id}")
    public Mono<DataView> delete(@PathVariable String resourceName, @Valid @IsUUID @PathVariable String id) {
        return dataService.delete(resourceName, UUID.fromString(id));
    }
}
