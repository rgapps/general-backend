package com.rgapps.data.service;

import com.rgapps.common.exception.apiexceptions.NotFoundException;
import com.rgapps.data.controller.resource.DataResource;
import com.rgapps.data.controller.resource.DataView;
import com.rgapps.data.repository.DataRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class DataService {
    private final DataRepository dataRepository;

    public Flux<DataView> getAll(String resourceName) {
        return dataRepository.findAllByResourceName(resourceName).map(DataView::fromRawData);
    }

    public Mono<DataView> get(String resourceName, UUID id) {
        return dataRepository.findByIdAndResourceName(id, resourceName).map(DataView::fromRawData)
                .switchIfEmpty(Mono.error(new NotFoundException(resourceName, "id", id.toString())));
    }

    public Mono<DataView> create(String resourceName, DataResource resource) {
        return dataRepository.save(resource.toRawData(resourceName)).map(DataView::fromRawData);
    }

    public Mono<DataView> update(String resourceName, UUID id, DataResource resource) {
        return dataRepository.findByIdAndResourceName(id, resourceName)
                .switchIfEmpty(Mono.error(new NotFoundException(resourceName, "id", id.toString())))
                .flatMap(it -> dataRepository.save(resource.toRawData(it)))
                .map(DataView::fromRawData);
    }

    public Mono<DataView> delete(String resourceName, UUID id) {
        return dataRepository.findByIdAndResourceName(id, resourceName)
                .switchIfEmpty(Mono.error(new NotFoundException(resourceName, "id", id.toString())))
                .flatMap(it -> dataRepository.delete(it).thenReturn(it))
                .map(DataView::fromRawData);
    }
}
