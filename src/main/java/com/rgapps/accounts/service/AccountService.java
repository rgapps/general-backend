package com.rgapps.accounts.service;

import com.rgapps.accounts.controller.resource.AccountResource;
import com.rgapps.accounts.model.Account;
import com.rgapps.accounts.repository.AccountRepository;
import com.rgapps.common.exception.apiexceptions.NotFoundException;
import com.rgapps.common.security.AuthenticationUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;

    public Mono<Void> createAccount(AccountResource resource) {
        return AuthenticationUtils.getCurrentUserId()
                .flatMap(userId -> accountRepository.save(resource.toAccount(userId)).then());
    }

    public Flux<Account> getAccounts() {
        return AuthenticationUtils.getCurrentUserId()
                .flatMapMany(accountRepository::findAllByUserId);
    }

    public Mono<Account> getAccount(UUID accountId) {
        Mono<UUID> userId = AuthenticationUtils.getCurrentUserId();
        return userId.flatMap(currentUserId -> accountRepository.findFirstByIdAndUserId(accountId, currentUserId)
                .filterWhen(account -> userId.filter(it -> it.equals(account.getUserId())).hasElement())
                .switchIfEmpty(Mono.error(new NotFoundException("Account", Map.of("accountName", "DEFAULT", "userId", currentUserId.toString())))));
    }

    public Mono<Account> getDefaultAccountForCurrentUser() {
        Mono<UUID> userId = AuthenticationUtils.getCurrentUserId();
        return userId.flatMap(currentUserId -> accountRepository.findFirstByAccountNameAndUserId("DEFAULT", currentUserId)
                .filterWhen(account -> userId.filter(it -> it.equals(account.getUserId())).hasElement())
                .switchIfEmpty(Mono.error(new NotFoundException("Account", Map.of("accountName", "DEFAULT", "userId", currentUserId.toString())))));
    }
}
