package com.rgapps.accounts.service;

import com.rgapps.accounts.model.Account;
import com.rgapps.accounts.model.AccountSummary;
import com.rgapps.accounts.model.CreditAccountMovement;
import com.rgapps.accounts.model.DebitAccountMovement;
import com.rgapps.accounts.model.DepositAccountMovement;
import com.rgapps.accounts.model.WithdrawAccountMovement;
import com.rgapps.accounts.repository.AccountSummaryRepository;
import com.rgapps.common.exception.ApiException;
import com.rgapps.common.exception.apiexceptions.NotFoundException;
import com.rgapps.common.exception.modelexceptions.PolicyValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AccountSummaryService {
    private final AccountSummaryRepository accountSummaryRepository;
    private final AccountService accountService;

    public Mono<AccountSummary> updateSummaryOnDeposit(Account account, DepositAccountMovement movement) {
        return getAccountSummary(account)
                .map(summary -> summary.updateOnDeposit(movement))
                .flatMap(accountSummaryRepository::save)
                .flatMap(this::onSummaryUpdated);
    }

    public Mono<AccountSummary> updateSummaryOnCredit(Account account, CreditAccountMovement movement) {
        return getAccountSummary(account)
                .map(summary -> summary.updateOnCredit(movement))
                .flatMap(accountSummaryRepository::save)
                .flatMap(this::onSummaryUpdated);
    }

    public Mono<AccountSummary> updateSummaryOnWithdraw(AccountSummary summary, WithdrawAccountMovement movement) {
        return Mono.fromCallable(() -> summary.updateOnWithdraw(movement))
                .flatMap(accountSummaryRepository::save)
                .flatMap(this::onSummaryUpdated);
    }

    public Mono<AccountSummary> updateSummaryOnDebit(AccountSummary summary, DebitAccountMovement movement) {
        return Mono.fromCallable(() -> summary.updateOnDebit(movement))
                .flatMap(accountSummaryRepository::save)
                .flatMap(this::onSummaryUpdated);
    }

    private Mono<AccountSummary> onSummaryUpdated(AccountSummary movement) {
        return Mono.just(movement);
    }

    public Mono<AccountSummary> getAccountSummary(UUID accountId) {
        return accountService.getAccount(accountId)
                .onErrorMap(ApiException.class, e -> new PolicyValidationException(e, ""))
                .flatMap(this::getAccountSummary)
                .switchIfEmpty(Mono.error(new NotFoundException("Account", "accountName", "DEFAULT")));
    }

    public Mono<AccountSummary> getAccountSummary(Account account) {
        return accountSummaryRepository.findById(account.getId())
                .defaultIfEmpty(AccountSummary.empty(account));
    }
}
