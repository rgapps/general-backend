package com.rgapps.accounts.service;

import com.rgapps.accounts.controller.resource.CreditMovementResource;
import com.rgapps.accounts.controller.resource.DebitMovementResource;
import com.rgapps.accounts.controller.resource.DepositMovementResource;
import com.rgapps.accounts.controller.resource.WithdrawMovementResource;
import com.rgapps.accounts.model.Account;
import com.rgapps.accounts.model.AccountMovement;
import com.rgapps.accounts.model.AccountSummary;
import com.rgapps.accounts.model.CreditAccountMovement;
import com.rgapps.accounts.model.DebitAccountMovement;
import com.rgapps.accounts.model.DepositAccountMovement;
import com.rgapps.accounts.model.WithdrawAccountMovement;
import com.rgapps.accounts.repository.AccountMovementRepository;
import com.rgapps.common.exception.ApiException;
import com.rgapps.common.exception.modelexceptions.PolicyValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AccountMovementService {
    private final AccountMovementRepository accountMovementRepository;
    private final AccountSummaryService accountSummaryService;
    private final AccountService accountService;

    public Mono<DepositAccountMovement> deposit(DepositMovementResource resource, UUID accountId) {
        return accountService.getAccount(accountId)
                .filter(account -> resource.validateIsCorrectCurrency(account.getCurrency()))
                .onErrorMap(ApiException.class, e -> new PolicyValidationException(e, resource))
                .zipWhen(account ->
                        accountMovementRepository.save(resource.toAccountMovement(AccountMovement.Status.SUCCESS, account.getId()))
                ).flatMap((Tuple2<Account, DepositAccountMovement> accountAndDeposit) ->
                        onDeposit(accountAndDeposit.getT1(), accountAndDeposit.getT2()))
                .onErrorResume(e -> onDepositFailed(resource).flatMap(it -> Mono.error(e)));
    }

    private Mono<DepositAccountMovement> onDeposit(Account account, DepositAccountMovement movement) {
        return accountSummaryService.updateSummaryOnDeposit(account, movement)
                .thenReturn(movement);
    }

    private Mono<DepositMovementResource> onDepositFailed(DepositMovementResource response) {
        return Mono.just(response);
    }

    public Mono<WithdrawAccountMovement> withdraw(WithdrawMovementResource resource, UUID accountId) {
        return accountService.getAccount(accountId)
                .filter(accountSummary -> resource.validateIsCorrectCurrency(accountSummary.getCurrency()))
                .flatMap(accountSummaryService::getAccountSummary)
                .filter(accountSummary -> resource.validateHasEnoughMoneyToWithDraw(accountSummary.getCurrentAmount().getValue()))
                .onErrorMap(ApiException.class, e -> new PolicyValidationException(e, resource))
                .zipWhen(accountSummary ->
                        accountMovementRepository.save(resource.toAccountMovement(AccountMovement.Status.SUCCESS, accountSummary.getAccountId()))
                ).flatMap((Tuple2<AccountSummary, WithdrawAccountMovement> accountAndWithdraw) ->
                        onWithdraw(accountAndWithdraw.getT1(), accountAndWithdraw.getT2())
                )
                .onErrorResume(e -> onWithDrawFailed(resource).flatMap(it -> Mono.error(e)));
    }

    private Mono<WithdrawAccountMovement> onWithdraw(AccountSummary summary, WithdrawAccountMovement movement) {
        return accountSummaryService.updateSummaryOnWithdraw(summary, movement)
                .thenReturn(movement);
    }

    private Mono<WithdrawMovementResource> onWithDrawFailed(WithdrawMovementResource response) {
        return Mono.just(response);
    }

    public Mono<CreditAccountMovement> credit(CreditMovementResource resource, UUID accountId) {
        return accountService.getAccount(accountId)
                .filter(accountSummary -> resource.validateIsCorrectCurrency(accountSummary.getCurrency()))
                .onErrorMap(ApiException.class, e -> new PolicyValidationException(e, resource))
                .zipWhen(defaultAccount ->
                        accountMovementRepository.save(resource.toAccountMovement(AccountMovement.Status.SUCCESS, defaultAccount.getId()))
                ).flatMap((Tuple2<Account, CreditAccountMovement> accountAndDeposit) ->
                        onCredit(accountAndDeposit.getT1(), accountAndDeposit.getT2()))
                .onErrorResume(e -> onCreditFailed(resource).flatMap(it -> Mono.error(e)));
    }

    private Mono<CreditAccountMovement> onCredit(Account account, CreditAccountMovement movement) {
        return accountSummaryService.updateSummaryOnCredit(account, movement)
                .thenReturn(movement);
    }

    private Mono<CreditMovementResource> onCreditFailed(CreditMovementResource response) {
        return Mono.just(response);
    }

    public Mono<DebitAccountMovement> debit(DebitMovementResource resource, UUID accountId) {
        return accountService.getAccount(accountId)
                .filter(accountSummary -> resource.validateIsCorrectCurrency(accountSummary.getCurrency()))
                .flatMap(accountSummaryService::getAccountSummary)
                .filter(accountSummary -> resource.validateHasEnoughMoneyToDebit(accountSummary.getCurrentAmount().getValue()))
                .onErrorMap(ApiException.class, e -> new PolicyValidationException(e, resource))
                .zipWhen(accountSummary ->
                        accountMovementRepository.save(resource.toAccountMovement(AccountMovement.Status.SUCCESS, accountSummary.getAccountId()))
                ).flatMap((Tuple2<AccountSummary, DebitAccountMovement> accountAndWithdraw) ->
                        onDebit(accountAndWithdraw.getT1(), accountAndWithdraw.getT2())
                )
                .onErrorResume(e -> onDebitFailed(resource).flatMap(it -> Mono.error(e)));
    }

    private Mono<DebitAccountMovement> onDebit(AccountSummary summary, DebitAccountMovement movement) {
        return accountSummaryService.updateSummaryOnDebit(summary, movement)
                .thenReturn(movement);
    }

    private Mono<DebitMovementResource> onDebitFailed(DebitMovementResource response) {
        return Mono.just(response);
    }

    public Flux<AccountMovement> getMovementsByTypes(String rawTypes, UUID accountId) {
        return accountService.getAccount(accountId)
                .onErrorMap(ApiException.class, e -> new PolicyValidationException(e, ""))
                .flatMapMany(account ->
                        Mono.fromCallable(() -> Arrays.asList(rawTypes.split(",")))
                                .flatMapMany(types ->
                                        accountMovementRepository.findByStatusAndTypeInAndAccountInOrderByMovementDateDesc(
                                                AccountMovement.Status.SUCCESS,
                                                types,
                                                List.of(account.getId())))
                                .onErrorResume(NullPointerException.class, e -> getAllMovements(account)));
    }

    public Flux<AccountMovement> getAllMovements(Account account) {
        return accountMovementRepository.findByStatusAndAccountInOrderByMovementDateDesc(AccountMovement.Status.SUCCESS, List.of(account.getId()));
    }
}
