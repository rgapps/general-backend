package com.rgapps.accounts.repository;

import com.rgapps.accounts.model.Account;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface AccountRepository extends ReactiveMongoRepository<Account, UUID> {
    Mono<Account> findFirstByAccountNameAndUserId(String accountName, UUID userId);

    Flux<Account> findAllByUserId(UUID userId);

    Mono<Account> findFirstByIdAndUserId(UUID accountId, UUID userId);
}
