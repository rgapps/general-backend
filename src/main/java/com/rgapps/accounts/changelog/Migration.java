package com.rgapps.accounts.changelog;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.rgapps.accounts.model.Account;
import com.rgapps.accounts.model.AccountMovement;
import com.rgapps.accounts.model.AccountSummary;
import com.rgapps.accounts.model.CreditAccountMovement;
import com.rgapps.accounts.model.DebitAccountMovement;
import com.rgapps.accounts.model.values.Currency;
import com.rgapps.accounts.model.values.MoneyAmount;
import com.rgapps.auth.model.Role;
import com.rgapps.auth.model.User;
import com.rgapps.common.config.SimpleMongoTemplateWrapper;
import com.rgapps.stocks.model.BuyStockTransaction;
import com.rgapps.stocks.model.SellStockTransaction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@ChangeLog(order = "002")
@Slf4j
public class Migration {
    @ChangeSet(order = "001", id = "createDefaultAccount", author = "Wilson")
    public void createDefaultAccount(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        log.info("create new Account");
        templateSimpleWrapper.getMongoTemplate().insert(new Account("DEFAULT", new Currency("PHP"), UUID.randomUUID()));
    }

    @ChangeSet(order = "002", id = "migrateExistentStockTransactionsIntoDefaultAccount", author = "Wilson")
    public void migrateExistentStockTransactionsIntoDefaultAccount(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        log.info("migrating existent stock transactions into default account");
        MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();

        Account account = mongoTemplate.findOne(new Query(new Criteria("accountName").is("DEFAULT")), Account.class);
        assert account != null;

        AccountSummary summary = AccountSummary.empty(account);
        List<BuyStockTransaction> buyTransactions = mongoTemplate.find(
                new Query().addCriteria(new Criteria("type").is("BUY").and("status").is("SUCCESS")),
                BuyStockTransaction.class);

        Flux.fromStream(buyTransactions.stream())
                .map(transaction -> summary.updateOnDebit(
                        mongoTemplate.save(new DebitAccountMovement(
                                account.getId(),
                                new MoneyAmount(transaction.getTotalSpent().getValue(),
                                        new Currency(transaction.getTotalSpent().getCurrency().getCode())),
                                transaction.getTransactionDate(),
                                AccountMovement.Status.SUCCESS,
                                DebitAccountMovement.DebitType.STOCKS_INVESTMENT,
                                transaction.getId())))
                ).collectList().block();

        List<SellStockTransaction> sellTransactions = mongoTemplate.find(
                new Query().addCriteria(new Criteria("type").is("SELL").and("status").is("SUCCESS")),
                SellStockTransaction.class);

        Flux.fromStream(sellTransactions.stream())
                .map(transaction ->
                        summary.updateOnCredit(mongoTemplate.save(new CreditAccountMovement(
                                account.getId(),
                                new MoneyAmount(transaction.getTotalReceived().getValue(),
                                        new Currency(transaction.getTotalReceived().getCurrency().getCode())),
                                transaction.getTransactionDate(),
                                AccountMovement.Status.SUCCESS,
                                CreditAccountMovement.CreditType.STOCKS_REVENUE,
                                transaction.getId())))
                ).collectList().block();
        mongoTemplate.save(summary);
    }

    @ChangeSet(order = "003", id = "separate DEPOSIT and WITHDRAW from Credit and Debit transactions", author = "Wilson")
    public void separateDepositAndWithDrawTransactions(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        log.info("separating DEPOSIT and WITHDRAW from Credit and Debit transactions");
        MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();

        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("creditType").is("DEPOSIT")),
                new Update().set("type", "DEPOSIT").unset("creditType"), "account-movement");

        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("debitType").is("WITHDRAW")),
                new Update().set("type", "WITHDRAW").unset("debitType"), "account-movement");

        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("type").is("DEBIT")),
                new Update().set("_class", "com.wilsonr990.page.accounts.model.DebitAccountMovement"), "account-movement");

        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("type").is("CREDIT")),
                new Update().set("_class", "com.wilsonr990.page.accounts.model.CreditAccountMovement"), "account-movement");
    }

    @ChangeSet(order = "004", id = "assign defaultAccount to new user", author = "Wilson")
    public void assignDefaultAccountToNewUser(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        log.info("insert user for current data");
        String encodedPassword = "$2a$10$FuOAslADu8Mz2A.f02wzcOeQ/zJVbCxeZxMjYstZTuGYuGXl4n72.";

        templateSimpleWrapper.getMongoTemplate()
                .insert(new User("Joshel", encodedPassword, Set.of(new Role(Role.RoleType.ROLE_USER))));

        log.info("create new Account");
        UUID userId = Objects.requireNonNull(templateSimpleWrapper.getMongoTemplate()
                .findOne(new Query().addCriteria(new Criteria("username").is("Joshel")), User.class)).getId();
        templateSimpleWrapper.getMongoTemplate().updateFirst(
                new Query().addCriteria(new Criteria("accountName").is("DEFAULT")),
                new Update().set("userId", userId), Account.class);
    }

    @ChangeSet(order = "005", id = "change module names accounts", author = "Wilson")
    public void changeModuleNamesAccounts(SimpleMongoTemplateWrapper templateSimpleWrapper) {
        log.info("change database and module names accounts");
        MongoTemplate mongoTemplate = templateSimpleWrapper.getMongoTemplate();

        mongoTemplate.updateMulti(new Query(),
                new Update().set("_class", "com.rgapps.accounts.model.Account"), "account");

        mongoTemplate.updateMulti(new Query(),
                new Update().set("_class", "com.rgapps.accounts.model.AccountSummary"), "account-summary");

        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("type").is("DEBIT")),
                new Update().set("_class", "com.rgapps.accounts.model.DebitAccountMovement"), "account-movement");

        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("type").is("CREDIT")),
                new Update().set("_class", "com.rgapps.accounts.model.CreditAccountMovement"), "account-movement");

        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("type").is("WITHDRAW")),
                new Update().set("_class", "com.rgapps.accounts.model.WithdrawAccountMovement"), "account-movement");

        mongoTemplate.updateMulti(new Query().addCriteria(new Criteria("type").is("DEPOSIT")),
                new Update().set("_class", "com.rgapps.accounts.model.WithdrawAccountMovement"), "account-movement");
    }
}
