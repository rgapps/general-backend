package com.rgapps.accounts.controller;

import com.rgapps.accounts.controller.resource.AccountSummaryView;
import com.rgapps.accounts.service.AccountSummaryService;
import com.rgapps.common.validator.IsUUID;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.UUID;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/accounts/{accountId}/summary")
@CrossOrigin
@RequiredArgsConstructor
public class AccountSummaryController {
    private final AccountSummaryService accountSummaryService;

    @GetMapping("")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Mono<AccountSummaryView> getAccountSummary(@Valid @PathVariable @IsUUID String accountId) {
        return accountSummaryService.getAccountSummary(UUID.fromString(accountId))
                .map(AccountSummaryView::fromAccountSummary);
    }
}
