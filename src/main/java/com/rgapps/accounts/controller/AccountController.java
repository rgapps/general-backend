package com.rgapps.accounts.controller;

import com.rgapps.accounts.controller.resource.AccountResource;
import com.rgapps.accounts.controller.resource.AccountView;
import com.rgapps.accounts.service.AccountService;
import com.rgapps.common.validator.IsUUID;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/accounts")
@CrossOrigin
@RequiredArgsConstructor
public class AccountController {
    private final AccountService accountService;

    @PutMapping("")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Mono<Void> createAccount(@Valid @RequestBody AccountResource resource) {
        return accountService.createAccount(resource);
    }

    @GetMapping("")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Flux<AccountView> getAccounts() {
        return accountService.getAccounts()
                .map(AccountView::fromAccount);
    }

    @GetMapping("/{accountId}")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Mono<AccountView> getAccount(@Valid @PathVariable @IsUUID String accountId) {
        return accountService.getAccount(UUID.fromString(accountId)).map(AccountView::fromAccount);
    }

    @GetMapping("/default")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Mono<AccountView> getDefaultAccount() {
        return accountService.getDefaultAccountForCurrentUser().map(AccountView::fromAccount);
    }
}
