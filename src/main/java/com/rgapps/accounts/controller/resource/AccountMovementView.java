package com.rgapps.accounts.controller.resource;

import com.rgapps.accounts.model.AccountMovement;
import lombok.Data;

@Data
public class AccountMovementView {
    private final AccountMovement.Type movementType;
    private final String currency;
    private final double amount;
    private final String date;

    public static AccountMovementView fromAccountSummary(AccountMovement movement) {
        return new AccountMovementView(movement.getType(),
                movement.getAmount().getCurrency().getCode(),
                movement.getAmount().getValue(),
                movement.getMovementDate().toString());
    }
}
