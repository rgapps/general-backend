package com.rgapps.accounts.controller.resource;

import com.rgapps.accounts.model.AccountSummary;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;

@Data
public class AccountSummaryView {
    private final UUID accountId;
    private final String accountName;
    private final String currency;
    private final double amount;
    private final String lastDepositOrWithdrawalDate;

    public static AccountSummaryView fromAccountSummary(AccountSummary summary) {
        LocalDate lastDepositOrWithdrawalDate = summary.getLastDepositDate().isAfter(summary.getLastWithDrawDate()) ?
                summary.getLastDepositDate() : summary.getLastWithDrawDate();
        return new AccountSummaryView(summary.getAccountId(),
                summary.getAccountName(),
                summary.getCurrency().getCode(),
                summary.getCurrentAmount().getValue(),
                lastDepositOrWithdrawalDate == LocalDate.EPOCH ? "" : lastDepositOrWithdrawalDate.toString());
    }
}
