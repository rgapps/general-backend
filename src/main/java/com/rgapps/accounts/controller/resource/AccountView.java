package com.rgapps.accounts.controller.resource;

import com.rgapps.accounts.model.Account;
import lombok.Data;

import java.util.UUID;

@Data
public class AccountView {
    private final UUID id;
    private final String accountName;
    private final String currency;

    public static AccountView fromAccount(Account account) {
        return new AccountView(account.getId(),
                account.getAccountName(),
                account.getCurrency().getCode());
    }
}
