package com.rgapps.accounts.controller.resource;

import com.rgapps.accounts.model.AccountMovement;
import com.rgapps.accounts.model.WithdrawAccountMovement;
import com.rgapps.accounts.model.values.Currency;
import com.rgapps.accounts.model.values.MoneyAmount;
import com.rgapps.common.exception.apiexceptions.UnexpectedValueException;
import com.rgapps.common.validator.InThePastOrPresent;
import com.rgapps.common.validator.IsLocalDate;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import static com.rgapps.accounts.model.values.Currency.ANY;

@Data
public class WithdrawMovementResource {
    @NotNull
    @Min(0)
    private Double withdrawAmount;
    @NotEmpty
    private String currencyCode;
    @NotEmpty
    @IsLocalDate
    @InThePastOrPresent
    private String dateWithdrawn;

    public WithdrawAccountMovement toAccountMovement(AccountMovement.Status status, UUID account) {
        return new WithdrawAccountMovement(
                account,
                new MoneyAmount(withdrawAmount, new Currency(currencyCode)),
                LocalDate.parse(dateWithdrawn),
                status);
    }

    public boolean validateIsCorrectCurrency(Currency currency) {
        if (!currency.equals(ANY) && !this.currencyCode.equals(currency.getCode())) {
            throw new UnexpectedValueException("Currency", currency.getCode(), this.currencyCode);
        }
        return true;
    }

    public boolean validateHasEnoughMoneyToWithDraw(double currentAmount) {
        if (withdrawAmount > currentAmount) {
            throw new UnexpectedValueException("Money to Withdraw", String.format("<= %f", currentAmount), withdrawAmount);
        }
        return true;
    }
}
