package com.rgapps.accounts.controller;

import com.rgapps.accounts.controller.resource.AccountMovementView;
import com.rgapps.accounts.controller.resource.CreditMovementResource;
import com.rgapps.accounts.controller.resource.DebitMovementResource;
import com.rgapps.accounts.controller.resource.DepositMovementResource;
import com.rgapps.accounts.controller.resource.WithdrawMovementResource;
import com.rgapps.accounts.model.AccountMovement;
import com.rgapps.accounts.model.CreditAccountMovement;
import com.rgapps.accounts.model.DebitAccountMovement;
import com.rgapps.accounts.model.DepositAccountMovement;
import com.rgapps.accounts.model.WithdrawAccountMovement;
import com.rgapps.accounts.service.AccountMovementService;
import com.rgapps.common.validator.HasEnums;
import com.rgapps.common.validator.IsUUID;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/accounts/{accountId}/movements")
@CrossOrigin
@Validated
@RequiredArgsConstructor
public class AccountMovementController {
    private final AccountMovementService accountMovementService;

    @GetMapping("")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Flux<AccountMovementView> getMovementsByType(@Valid @RequestParam(required = false) @HasEnums(AccountMovement.Type.class) String types,
                                                        @Valid @PathVariable @IsUUID String accountId) {
        return accountMovementService.getMovementsByTypes(types, UUID.fromString(accountId))
                .map(AccountMovementView::fromAccountSummary);
    }

    @PostMapping("/deposit")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Mono<DepositAccountMovement> deposit(@Valid @RequestBody DepositMovementResource resource,
                                                @Valid @PathVariable @IsUUID String accountId) {
        return accountMovementService.deposit(resource, UUID.fromString(accountId));
    }

    @PostMapping("/withdraw")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Mono<WithdrawAccountMovement> withdraw(@Valid @RequestBody WithdrawMovementResource resource,
                                                  @Valid @PathVariable @IsUUID String accountId) {
        return accountMovementService.withdraw(resource, UUID.fromString(accountId));
    }

    @PostMapping("/credit")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Mono<CreditAccountMovement> creditTransaction(@Valid @RequestBody CreditMovementResource resource,
                                                         @Valid @PathVariable @IsUUID String accountId) {
        return accountMovementService.credit(resource, UUID.fromString(accountId));
    }

    @PostMapping("/debit")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    @PreAuthorize("hasRole('USER')")
    public Mono<DebitAccountMovement> debitTransaction(@Valid @RequestBody DebitMovementResource resource,
                                                       @Valid @PathVariable @IsUUID String accountId) {
        return accountMovementService.debit(resource, UUID.fromString(accountId));
    }
}
