package com.rgapps.accounts.model;

import com.rgapps.accounts.model.values.MoneyAmount;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class DebitAccountMovement extends AccountMovement {
    public enum DebitType {
        STOCKS_INVESTMENT,
        STOCKS_DELETE_TRANSACTION,
    }

    protected DebitType debitType;
    protected UUID externalId;

    public DebitAccountMovement(UUID account,
                                MoneyAmount amount,
                                LocalDate movementDate,
                                Status status,
                                DebitType debitType,
                                UUID externalId) {
        super(account, Type.DEBIT, amount, movementDate, status);
        this.debitType = debitType;
        this.externalId = externalId;
    }
}
