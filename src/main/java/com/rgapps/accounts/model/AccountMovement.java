package com.rgapps.accounts.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.rgapps.accounts.model.values.MoneyAmount;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Document(collection = "account-movement")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = DepositAccountMovement.class, name = "DEPOSIT"),
        @JsonSubTypes.Type(value = WithdrawAccountMovement.class, name = "WITHDRAW"),
        @JsonSubTypes.Type(value = CreditAccountMovement.class, name = "CREDIT"),
        @JsonSubTypes.Type(value = DebitAccountMovement.class, name = "DEBIT")
})
public abstract class AccountMovement {
    public enum Type {
        DEPOSIT, WITHDRAW, CREDIT, DEBIT
    }

    public enum Status {
        SUCCESS, DELETED
    }

    @Id
    protected UUID id = UUID.randomUUID();
    @EqualsAndHashCode.Exclude
    protected LocalDateTime createdDate = LocalDateTime.now();
    @EqualsAndHashCode.Exclude
    protected LocalDateTime updatedDate = createdDate;

    @NonNull
    private UUID account;
    @NonNull
    private Type type;
    @NonNull
    protected MoneyAmount amount;
    @NonNull
    protected LocalDate movementDate;
    @NonNull
    protected Status status;
}
