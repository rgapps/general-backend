package com.rgapps.accounts.model;

import com.rgapps.accounts.model.values.Currency;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Document(collection = "account")
public class Account {
    @EqualsAndHashCode.Exclude
    private LocalDateTime createdDate = LocalDateTime.now();
    @EqualsAndHashCode.Exclude
    private LocalDateTime updatedDate = createdDate;

    @Id
    private UUID id = UUID.randomUUID();
    private final String accountName;
    private final Currency currency;
    private final UUID userId;
}
