package com.rgapps.accounts.model;

import com.rgapps.accounts.model.values.Currency;
import com.rgapps.accounts.model.values.MoneyAmount;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Document(collection = "account-summary")
public class AccountSummary {
    @EqualsAndHashCode.Exclude
    private LocalDateTime updatedDate = LocalDateTime.now();

    @Id
    @NonNull
    private UUID accountId;
    @NonNull
    private String accountName;
    @NonNull
    private Currency currency;
    private MoneyAmount currentAmount = MoneyAmount.zeroAmount;
    private LocalDate lastDepositDate = LocalDate.EPOCH;
    private LocalDate lastWithDrawDate = LocalDate.EPOCH;

    public static AccountSummary empty(Account account) {
        return new AccountSummary(account.getId(), account.getAccountName(), account.getCurrency());
    }

    public AccountSummary updateOnDeposit(DepositAccountMovement movement) {
        this.currentAmount = this.currentAmount.plus(movement.amount);
        this.lastDepositDate = movement.getMovementDate();
        return this;
    }

    public AccountSummary updateOnCredit(CreditAccountMovement movement) {
        this.currentAmount = this.currentAmount.plus(movement.amount);
        return this;
    }

    public AccountSummary updateOnWithdraw(WithdrawAccountMovement movement) {
        this.currentAmount = this.currentAmount.minus(movement.amount);
        this.lastWithDrawDate = movement.getMovementDate();
        return this;
    }

    public AccountSummary updateOnDebit(DebitAccountMovement movement) {
        this.currentAmount = this.currentAmount.minus(movement.amount);
        return this;
    }
}
