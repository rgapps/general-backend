package com.rgapps.accounts.model;

import com.rgapps.accounts.model.values.MoneyAmount;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
public class DepositAccountMovement extends AccountMovement {
    public DepositAccountMovement(UUID account, MoneyAmount amount, LocalDate movementDate, Status status) {
        super(account, Type.DEPOSIT, amount, movementDate, status);
    }
}
