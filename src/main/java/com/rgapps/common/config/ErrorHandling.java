package com.rgapps.common.config;

import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.ApiException;
import com.rgapps.common.exception.apiexceptions.ValidationErrorResponse;
import com.rgapps.common.exception.modelexceptions.ModelErrorResponse;
import com.rgapps.common.exception.modelexceptions.ModelException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ServerWebInputException;

import javax.security.sasl.AuthenticationException;
import javax.validation.ConstraintViolationException;

@ControllerAdvice
@Slf4j
public class ErrorHandling {
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler({WebExchangeBindException.class, ConstraintViolationException.class,
            ServerWebInputException.class})
    ValidationErrorResponse handleConstraintViolationException(Exception e) {
        ValidationErrorResponse validationErrorResponse = new ValidationErrorResponse(HttpStatus.BAD_REQUEST,
                "ValidationException", "Validation Error", e);
        log.warn(String.format("Validation Exception: %s", validationErrorResponse.getErrors()), e);
        return validationErrorResponse;
    }

    @ResponseStatus(HttpStatus.CONFLICT)
    @ResponseBody
    @ExceptionHandler(ModelException.class)
    ModelErrorResponse<Object> handleModelException(ModelException e) {
        log.warn(String.format("Model Exception: %s", e.errorResponse), e);
        return e.errorResponse;
    }

    @ResponseBody
    @ExceptionHandler(ApiException.class)
    ResponseEntity<ApiErrorResponse> handleRawException(ApiException e) {
        log.warn(String.format("Api Exception: %s", e.errorResponse), e);
        return new ResponseEntity<>(e.errorResponse, e.errorResponse.getStatus());
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ResponseBody
    @ExceptionHandler(AuthenticationException.class)
    ApiErrorResponse handleAuthenticationException(AuthenticationException e) {
        log.warn(String.format("Authentication required: %s", e), e);
        return new ApiErrorResponse(HttpStatus.UNAUTHORIZED, e.getClass().getSimpleName(),
                e.getLocalizedMessage());
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    @ExceptionHandler(AccessDeniedException.class)
    ApiErrorResponse handleAuthenticationException(AccessDeniedException e) {
        log.warn(String.format("Access Denied: %s", e), e);
        return new ApiErrorResponse(HttpStatus.FORBIDDEN, e.getClass().getSimpleName(),
                e.getLocalizedMessage());
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler(Exception.class)
    ApiErrorResponse handleRawException(Exception e) {
        log.error(String.format("Unknown error: %s", e.getMessage()), e);
        return new ApiErrorResponse(
                HttpStatus.INTERNAL_SERVER_ERROR,
                e.getClass().getSimpleName(),
                e.getLocalizedMessage());
    }
}
