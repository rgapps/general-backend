package com.rgapps.common.config;

import com.github.cloudyrock.spring.v5.EnableMongock;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.bson.UuidRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@EnableMongock
@Profile("!test")
public class MongockConf {
    @Value("${spring.data.mongodb.uri}")
    String uri;

    @Value("${spring.data.mongodb.database}")
    String database;

    @Bean
    public MongoClient mongo() {
        ConnectionString connectionString = new ConnectionString(uri);
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .uuidRepresentation(UuidRepresentation.JAVA_LEGACY)
                .build();
        return MongoClients.create(mongoClientSettings);
    }

    @Bean
    @Primary
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongo(), database);
    }

    @Bean
    @Primary
    public SimpleMongoTemplateWrapper mongoTemplateSimpleWrapper() {
        return () -> new MongoTemplate(mongo(), database);
    }
}
