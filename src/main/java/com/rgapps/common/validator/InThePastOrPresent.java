package com.rgapps.common.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Target({FIELD, PARAMETER})
@Constraint(validatedBy = InThePastOrPresentValidator.class)
public @interface InThePastOrPresent {
    String message() default "must be a date in the past or in the present";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * Defines several {@link InThePastOrPresent} annotations on the same element.
     *
     * @see InThePastOrPresent
     */
    @Target({FIELD, PARAMETER})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        InThePastOrPresent[] value();
    }
}
