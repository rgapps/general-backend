package com.rgapps.common.validator;

import java.time.LocalDate;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class InThePastOrPresentValidator implements ConstraintValidator<InThePastOrPresent, String> {
    @Override
    public void initialize(InThePastOrPresent constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.isEmpty()) {
            return true;
        }
        try {
            LocalDate date = LocalDate.parse(value);
            return date.isBefore(LocalDate.now().plusDays(1));
        } catch (Exception e) {
            return false;
        }
    }
}
