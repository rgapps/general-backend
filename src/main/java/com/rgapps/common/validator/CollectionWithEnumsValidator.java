package com.rgapps.common.validator;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class CollectionWithEnumsValidator implements ConstraintValidator<CollectionWithEnums, Collection<String>> {
    private List<String> acceptedValues;
    private String message;

    @Override
    public void initialize(CollectionWithEnums annotation) {
        acceptedValues = Stream.of(annotation.value().getEnumConstants()).map(Enum::name).collect(Collectors.toList());
        message = MessageFormat.format("must have a valid among: {0}", acceptedValues);
    }

    @Override
    public boolean isValid(Collection<String> value, ConstraintValidatorContext context) {
        if (value == null || value.isEmpty()) {
            return true;
        }
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(message).addConstraintViolation();

        return new HashSet<>(acceptedValues).containsAll(value);
    }
}
