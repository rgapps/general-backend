package com.rgapps.common.validator;

import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

class IsEnumValidator implements ConstraintValidator<IsEnum, String> {
    private List<String> acceptedValues;
    private String message;

    @Override
    public void initialize(IsEnum annotation) {
        acceptedValues = Stream.of(annotation.value().getEnumConstants())
                .map(Enum::name)
                .collect(Collectors.toList());
        message = MessageFormat.format("must have a valid among: {0}", acceptedValues);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.isEmpty()) {
            return true;
        }
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(message).addConstraintViolation();

        return acceptedValues.contains(value);
    }
}
