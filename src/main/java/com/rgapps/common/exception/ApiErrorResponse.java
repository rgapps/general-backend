package com.rgapps.common.exception;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class ApiErrorResponse {
    LocalDateTime timestamp = LocalDateTime.now();

    @NonNull
    private HttpStatus status;
    @NonNull
    private String error;
    @NonNull
    private String message;
}
