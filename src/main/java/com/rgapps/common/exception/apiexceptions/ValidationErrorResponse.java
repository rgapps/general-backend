package com.rgapps.common.exception.apiexceptions;

import com.rgapps.common.exception.ApiErrorResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.ServerWebInputException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class ValidationErrorResponse extends ApiErrorResponse {
    private Map<String, List<String>> errors;

    public ValidationErrorResponse(HttpStatus status, String error, String message, Exception exception) {
        super(status, error, message);
        if (exception instanceof WebExchangeBindException) {
            this.errors = getErrorsFromBindingResult(((WebExchangeBindException) exception).getBindingResult());
        } else if (exception instanceof ConstraintViolationException) {
            this.errors = getErrorsFromConstraintViolations(((ConstraintViolationException) exception).getConstraintViolations());
        } else if (exception instanceof ServerWebInputException) {
            this.errors = getErrorsFromServerWebInputException((ServerWebInputException) exception);
        }
    }

    private Map<String, List<String>> getErrorsFromConstraintViolations(Collection<ConstraintViolation<?>> constraintViolations) {
        return constraintViolations.stream().collect(
                Collectors.toMap(c -> {
                            String[] pathFragments = c.getPropertyPath().toString().split("\\.");
                            return List.of(pathFragments).get(pathFragments.length - 1);
                        },
                        constrain -> List.of(Objects.requireNonNull(constrain.getMessage())),
                        (List<String> x1, List<String> x2) -> List.of(x1.get(0), x2.get(0)))
        );
    }

    private Map<String, List<String>> getErrorsFromBindingResult(BindingResult bindingResult) {
        return bindingResult.getFieldErrors().stream().collect(
                Collectors.toMap(FieldError::getField, error -> List.of(Objects.requireNonNull(error.getDefaultMessage())),
                        (List<String> x1, List<String> x2) -> List.of(x1.get(0), x2.get(0))));
    }

    private Map<String, List<String>> getErrorsFromServerWebInputException(ServerWebInputException serverInputException) {
        return Map.of(Objects.requireNonNull(Objects.requireNonNull(serverInputException.getMethodParameter()).getParameterName()),
                List.of(Objects.requireNonNull(serverInputException.getReason())));
    }
}
