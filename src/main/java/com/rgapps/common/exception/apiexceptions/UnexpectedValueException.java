package com.rgapps.common.exception.apiexceptions;

import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.ApiException;
import org.springframework.http.HttpStatus;

public class UnexpectedValueException extends ApiException {
    public UnexpectedValueException(String value, Object expected, Object actual) {
        super(new ApiErrorResponse(HttpStatus.CONFLICT,
                UnexpectedValueException.class.getSimpleName(),
                buildMessage(value, expected, actual)));
    }

    private static String buildMessage(String value, Object expected, Object actual) {
        return String.format("Unexpected value for '%s' (expected: %s, actual: %s)", value, expected, actual);
    }
}
