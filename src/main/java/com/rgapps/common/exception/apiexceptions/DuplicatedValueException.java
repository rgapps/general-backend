package com.rgapps.common.exception.apiexceptions;

import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.ApiException;
import org.springframework.http.HttpStatus;

public class DuplicatedValueException extends ApiException {
    public DuplicatedValueException(String field, Object value) {
        super(new ApiErrorResponse(HttpStatus.CONFLICT, DuplicatedValueException.class.getSimpleName(),
                buildMessage(field, value)));
    }

    private static String buildMessage(String field, Object value) {
        return String.format("%s '%s' already exists", field, value);
    }
}
