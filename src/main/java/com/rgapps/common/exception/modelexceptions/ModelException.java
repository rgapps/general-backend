package com.rgapps.common.exception.modelexceptions;

import com.rgapps.common.exception.ApiException;

public class ModelException extends ApiException {
    public ModelErrorResponse<Object> errorResponse;

    public ModelException(ApiException e, Object erroredObject) {
        super(e.errorResponse, e);
        this.errorResponse = new ModelErrorResponse<>(e, erroredObject);
    }
}

