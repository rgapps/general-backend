package com.rgapps.common.exception.modelexceptions;

import com.rgapps.common.exception.ApiException;

public class PolicyValidationException extends ModelException {
    public PolicyValidationException(ApiException e, Object erroredInformation) {
        super(e, erroredInformation);
    }
}
