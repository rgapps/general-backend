package com.rgapps.common.exception.modelexceptions;

import com.rgapps.common.exception.ApiErrorResponse;
import com.rgapps.common.exception.ApiException;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ModelErrorResponse<T> extends ApiErrorResponse {
    public T erroredObject;

    public ModelErrorResponse(ApiException errorResponse, T erroredObject) {
        super(errorResponse.errorResponse.getStatus(), errorResponse.errorResponse.getError(), errorResponse.errorResponse.getMessage());
        this.erroredObject = erroredObject;
    }
}
