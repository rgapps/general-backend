package com.rgapps.common.security;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class AuthenticationManager implements ReactiveAuthenticationManager {
    private final JwtUtils jwtUtils;

    @Override
    @SuppressWarnings("unchecked")
    public Mono<Authentication> authenticate(Authentication authentication) {
        return Mono.fromCallable(() -> authentication.getCredentials().toString())
                .filter(jwtUtils::validateToken)
                .map(authToken -> {
                    Claims claims = jwtUtils.getAllClaimsFromToken(authToken);
                    List<String> rolesMap = claims.get("role", List.class);
                    UUID id = UUID.fromString(claims.get("id", String.class));
                    return new UsernamePasswordAuthenticationToken(id, authToken,
                            rolesMap.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
                });
    }
}
